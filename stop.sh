#!/usr/bin/env bash

echo "Stop netty and processor"
kill -n 9 $(ps aux | grep '[g]radlew' | awk '{print $2}')

echo "Stop redis"
kill -n 9 $(ps aux | grep '[r]edis-server' | awk '{print $2}')

echo "Stop kafka"
kill -n 9 $(ps aux | grep '[k]afka' | awk '{print $2}')

echo "Stop zookeeper"
kill -n 9 $(ps aux | grep '[z]ookeeper' | awk '{print $2}')

rm -rf /tmp/zookeeper
rm -rf /tmp/kafka-logs
