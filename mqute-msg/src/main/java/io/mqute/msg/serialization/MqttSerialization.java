package io.mqute.msg.serialization;

import io.mqute.msg.MqttMessage;
import io.mqute.msg.enums.MessageType;
import io.mqute.msg.serialization.impl.*;
import io.mqute.msg.serialization.util.ProtocolUtils;

import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;


public class MqttSerialization {

  private static MqttSerialization instance;

  private Map<MessageType, IMqttSerializer<? extends MqttMessage>> msgTypeToSerializers;

  private MqttSerialization() {
    final Map<MessageType, IMqttSerializer<? extends MqttMessage>> building = new HashMap<>(16);
    building.put(MessageType.CONNECT, new ConnectSerializer());
    building.put(MessageType.CONNACK, new ConnAckSerializer());
    building.put(MessageType.PUBLISH, new PublishSerializer());
    building.put(MessageType.PUBACK, new PubAckSerializer());
    building.put(MessageType.PUBREC, new PubRecSerializer());
    building.put(MessageType.PUBREL, new PubRelSerializer());
    building.put(MessageType.PUBCOMP, new PubCompSerializer());
    building.put(MessageType.SUBSCRIBE, new SubscribeSerializer());
    building.put(MessageType.SUBACK, new SubAckSerializer());
    building.put(MessageType.UNSUBSCRIBE, new UnsubscribeSerializer());
    building.put(MessageType.UNSUBACK, new UnsubAckSerializer());
    building.put(MessageType.PINGREQ, new PingReqSerializer());
    building.put(MessageType.PINGRESP, new PingRespSerializer());
    building.put(MessageType.DISCONNECT, new DisconnectSerializer());
    //because the mapping is immutable, there's no need to construct a concurrent hashmap
    msgTypeToSerializers = Collections.unmodifiableMap(building);
  }

  public static MqttSerialization getInstance() {
    return Optional.ofNullable(instance).orElseGet(() -> {
      synchronized (MqttSerialization.class) {
        if (!Optional.ofNullable(instance).isPresent()) {
          instance = new MqttSerialization();
        }
        return instance;
      }
    });
  }

  @SuppressWarnings("unchecked")
  public <Message extends MqttMessage> ByteBuffer serialize(final Message message) {
    final MessageType messageType = message.getMessageType();
    //no need to check key existence, as it's halfway retrieving the serializer already
    final IMqttSerializer<Message> serializer = (IMqttSerializer<Message>) msgTypeToSerializers.get(messageType);
    if (serializer == null) {
      throw new RuntimeException("Cannot find Serializer for " + messageType);
    }
    return serializer.serialize(message);
  }

  @SuppressWarnings("unchecked")
  public <Message extends MqttMessage> Message deserialize(final ByteBuffer byteBuffer) {
    // mark the current position
//    byteBuffer.mark();
//    byte firstByte = byteBuffer.get();
//    byteBuffer.reset();
    //could compare a bit, whether it's worthy mark/reset vs. get byte at position, assuming the buffer size is small (mqtt message), absolute position
    //could be faster which avoids concurrent issues, and avoids 2 extra calls to set/reset the marks
    final byte firstByte = byteBuffer.get(byteBuffer.position());
    final MessageType messageType = ProtocolUtils.getMessageType(firstByte);
    final IMqttSerializer<Message> serializer = (IMqttSerializer<Message>) msgTypeToSerializers.get(messageType);
    if (serializer == null) {
      throw new RuntimeException("Cannot find Serializer for " + messageType);
    }
    return (Message) msgTypeToSerializers.get(messageType).deserialize(byteBuffer);
  }

}
