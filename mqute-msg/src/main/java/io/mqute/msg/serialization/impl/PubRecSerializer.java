package io.mqute.msg.serialization.impl;

import io.mqute.msg.PubRecMessage;
import io.mqute.msg.error.CorruptedMqttPacketException;
import io.mqute.msg.serialization.MqttFixedHeader;
import io.mqute.msg.serialization.util.ProtocolUtils;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;


public class PubRecSerializer extends AbstractMqttSerializer<PubRecMessage> {

  @Override
  public ByteBuffer serialize(PubRecMessage message) {
    int remainingLength = message.getRemainingLength();
    int numOfBytes = ProtocolUtils.numOfBytesToEncode(message.getRemainingLength());
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(1 + numOfBytes + remainingLength);
    byteBuffer.mark();
    serializeHeader(message, byteBuffer);
    byteBuffer.putShort(message.getPacketId());
    byteBuffer.reset();
    return byteBuffer;
  }

  @Override
  public PubRecMessage deserialize(ByteBuffer byteBuffer) {
    MqttFixedHeader header = deserializeHeader(byteBuffer);
    PubRecMessage message;
    try {
      short packetId = byteBuffer.getShort();
      message = new PubRecMessage(packetId);
      if (!validateHeader(message, header)) {
        throw new CorruptedMqttPacketException(
            "Fixed header " + header + " from the buffer doesn't match this message type"
        );
      }
    } catch (BufferUnderflowException e) {
      throw new CorruptedMqttPacketException(e);
    }
    return message;
  }
}
