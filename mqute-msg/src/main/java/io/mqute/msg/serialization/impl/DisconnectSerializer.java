package io.mqute.msg.serialization.impl;

import io.mqute.msg.DisconnectMessage;
import io.mqute.msg.error.CorruptedMqttPacketException;
import io.mqute.msg.serialization.MqttFixedHeader;
import io.mqute.msg.serialization.util.ProtocolUtils;

import java.nio.ByteBuffer;


public class DisconnectSerializer extends AbstractMqttSerializer<DisconnectMessage> {

  @Override
  public ByteBuffer serialize(DisconnectMessage message) {
    int remainingLength = message.getRemainingLength();
    int numOfBytes = ProtocolUtils.numOfBytesToEncode(message.getRemainingLength());
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(1 + numOfBytes + remainingLength);
    byteBuffer.mark();
    serializeHeader(message, byteBuffer);
    byteBuffer.reset();
    return byteBuffer;
  }

  @Override
  public DisconnectMessage deserialize(ByteBuffer byteBuffer) {
    MqttFixedHeader header = deserializeHeader(byteBuffer);
    DisconnectMessage message = new DisconnectMessage();
    if (!validateHeader(message, header)) {
      throw new CorruptedMqttPacketException(
          "Fixed header " + header + " from the buffer doesn't match this message type"
      );
    }
    return message;
  }
}
