package io.mqute.msg.serialization.util;


import io.mqute.msg.enums.MessageType;
import io.mqute.msg.error.CorruptedMqttPacketException;

import java.io.UnsupportedEncodingException;
import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public class ProtocolUtils {

  public static final int MAX_LENGTH_LIMIT = 268435455;

  public static final Charset UTF_8 = Charset.forName("UTF-8");

  /**
   * Decode the remaining length in fixed header
   * MQTT 3.1.1 (Section 2.2.3)
   *
   * @param byteBuffer the ByteBuffer of the current MQTT message with position at second byte
   * @return the decoded length
   * @throws CorruptedMqttPacketException if the following bytes don't follow the spec
   * @throws IllegalArgumentException     if the current position in byteBuffer is not at second byte
   */
  public static int decodeRemainingLength(ByteBuffer byteBuffer) {
    if (byteBuffer.position() != 1) {
      throw new IllegalArgumentException("Current position in ByteBuffer is not 1: " + byteBuffer.position());
    }
    byte b;
    int value = 0;
    int multiplier = 1;
    do {
      if (multiplier > 128 * 128 * 128) {
        throw new CorruptedMqttPacketException();
      }
      try {
        b = byteBuffer.get();
        value += (b & 0x7F) * multiplier;
        multiplier *= 128;
      } catch (BufferUnderflowException e) {
        throw new CorruptedMqttPacketException(e);
      }
    } while ((b & 0x80) != 0);
    return value;
  }

  /**
   * Encode the remaining length in fixed header
   * MQTT 3.1.1 (Section 2.2.3)
   *
   * @param length     the length need to be encoded
   * @param byteBuffer the ByteBuffer to which the encoded length is written with position at second byte
   * @throws IllegalArgumentException     if the length is not in the specified range
   * @throws IllegalArgumentException     if the current position in byteBuffer is not at second byte
   * @throws CorruptedMqttPacketException if byteBuffer overflow
   */
  public static void encodeRemainingLength(int length, ByteBuffer byteBuffer) {
    if (length > MAX_LENGTH_LIMIT || length < 0) {
      throw new IllegalArgumentException("Value should in range 0.." + MAX_LENGTH_LIMIT + " found " + length);
    }
    if (byteBuffer.position() != 1) {
      throw new IllegalArgumentException("Current position in ByteBuffer is not 1: " + byteBuffer.position());
    }
    do {
      byte digit = (byte) (length % 128);
      length = length / 128;
      // if there are more digits to encode, set the top bit of this digit
      if (length > 0) {
        digit = (byte) (digit | 0x80);
      }
      try {
        byteBuffer.put(digit);
      } catch (BufferOverflowException e) {
        throw new CorruptedMqttPacketException(e);
      }
    } while (length > 0);
  }

  /**
   * Return the number of bytes to encode the given remaining length value
   * MQTT 3.1.1 (Section 2.2.3)
   *
   * @param length the length need to be encoded
   * @return the number of bytes that need to be used to encode the remaining length
   * @throws IllegalArgumentException if the length is not in the specified range
   */
  public static int numOfBytesToEncode(int length) {
    if (0 <= length && length <= 127) return 1;
    if (128 <= length && length <= 16383) return 2;
    if (16384 <= length && length <= 2097151) return 3;
    if (2097152 <= length && length <= MAX_LENGTH_LIMIT) return 4;
    throw new IllegalArgumentException("value should be in the range [0.." + MAX_LENGTH_LIMIT + "]");
  }

  /**
   * Decode a UTF-8 encoded string from the ByteBuffer
   * MQTT 3.1.1 (Section 1.5.3)
   *
   * @param byteBuffer the ByteBuffer from which string is decoded
   * @return decoded UTF-8 encoded string
   * @throws UnsupportedEncodingException if the string is not encoded in utf-8
   * @throws CorruptedMqttPacketException if the byteBuffer underflow
   */
  public static String decodeString(ByteBuffer byteBuffer) throws UnsupportedEncodingException {
    try {
      int length = byteBuffer.getShort();
      byte[] bytes = new byte[length];
      byteBuffer.get(bytes);
      return new String(bytes, UTF_8);
    } catch (BufferUnderflowException e) {
      throw new CorruptedMqttPacketException(e);
    }
  }

  /**
   * Encode a UTF-8 encoded string from the ByteBuffer
   * MQTT 3.1.1 (Section 1.5.3)
   *
   * @param string     UTF-8 encoded string needs to be encoded
   * @param byteBuffer the ByteBuffer to which encoded string is written
   * @throws CorruptedMqttPacketException if bytBuffer overflow
   */
  public static void encodeString(String string, ByteBuffer byteBuffer) {
    try {
      byte[] bytes = string.getBytes(UTF_8);
      byteBuffer.putShort((short) bytes.length);
      byteBuffer.put(bytes);
    } catch (BufferOverflowException e) {
      throw new CorruptedMqttPacketException(e);
    }
  }

  /**
   * Construct the first byte in the MQTT packet
   *
   * @param messageType type of the MQTT message
   * @param flags       header flags
   * @return the first byte in the MQTT packet
   */
  public static byte constructFirstByte(MessageType messageType, byte flags) {
    return (byte) (messageType.getByteValue() << 4 & 0xf0 | flags & 0x0f);
  }

  /**
   * Parse the message type from the MQTT packet
   *
   * @param firstByte the first byte in the MQTT packet
   * @return the message type
   * @throws CorruptedMqttPacketException if the message type is unknown
   */
  public static MessageType getMessageType(byte firstByte) {
    try {
      return MessageType.fromByte((byte) (firstByte >> 4 & 0x0f));
    } catch (IllegalArgumentException e) {
      throw new CorruptedMqttPacketException(e);
    }
  }

  /**
   * Parse the header flags from the MQTT packet
   *
   * @param firstByte first byte in the MQTT packet
   * @return the header flags
   */
  public static byte getFlags(byte firstByte) {
    return (byte) (firstByte & 0x0f);
  }

  /**
   * A topic filter in UN/SUBSCRIBE message can contain wildcards.
   * Wildcard "#" is only allowed in the last section
   *
   * @param topicFilter topic filter in UN/SUBSCRIBE message
   * @return if the topic filter is valid
   */
  public static boolean validateTopicFilter(String topicFilter) {
    if (topicFilter == null || topicFilter.length() == 0) {
      return false;
    }
    if (topicFilter.startsWith("/") || topicFilter.endsWith("/")) {
      return false;
    }
    String[] sections = topicFilter.split("/");
    for (int i = 0; i < sections.length - 1; i++) {
      if (sections[i].equals("#")) {
        return false;
      }
    }
    return true;
  }

  /**
   * A topic name in PUBLISH message must not contain wildcards
   *
   * @param topicName topic name in PUBLISH message
   * @return if the topic name is valid
   */
  public static boolean validateTopicName(String topicName) {
    if (topicName == null || topicName.length() == 0) {
      return false;
    }
    if (topicName.startsWith("/") || topicName.endsWith("/")) {
      return false;
    }
    for (String section : topicName.split("/")) {
      if (section.equals("#") || section.equals("+")) {
        return false;
      }
    }
    return true;
  }

}
