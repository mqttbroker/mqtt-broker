package io.mqute.msg.serialization.impl;


import io.mqute.msg.ConnAckMessage;
import io.mqute.msg.enums.ConnectReturnCode;
import io.mqute.msg.error.CorruptedMqttPacketException;
import io.mqute.msg.serialization.MqttFixedHeader;
import io.mqute.msg.serialization.util.ProtocolUtils;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;

public class ConnAckSerializer extends AbstractMqttSerializer<ConnAckMessage> {

  @Override
  public ByteBuffer serialize(ConnAckMessage message) {
    int remainingLength = message.getRemainingLength();
    int numOfBytes = ProtocolUtils.numOfBytesToEncode(message.getRemainingLength());
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(1 + numOfBytes + remainingLength);
    byteBuffer.mark();
    serializeHeader(message, byteBuffer);
    byteBuffer.put(message.isSessionPresent() ? (byte) 0x01 : (byte) 0x00);
    byteBuffer.put(message.getReturnCode().getByteValue());
    byteBuffer.reset();
    return byteBuffer;
  }

  @Override
  public ConnAckMessage deserialize(ByteBuffer byteBuffer) {
    MqttFixedHeader header = deserializeHeader(byteBuffer);
    ConnAckMessage message;
    try {
      byte sp = byteBuffer.get();
      boolean sessionPresent;
      if (sp == (byte) 0x00) {
        sessionPresent = false;
      } else if (sp == (byte) 0x01) {
        sessionPresent = true;
      } else {
        throw new CorruptedMqttPacketException("Violate MQTT section 3.2.2.1");
      }
      ConnectReturnCode code = ConnectReturnCode.fromByte(byteBuffer.get());
      message = new ConnAckMessage(code, sessionPresent);
      if (!validateHeader(message, header)) {
        throw new CorruptedMqttPacketException(
            "Fixed header " + header + " from the buffer doesn't match this message type"
        );
      }
    } catch (BufferUnderflowException e) {
      throw new CorruptedMqttPacketException(e);
    }
    return message;
  }
}
