package io.mqute.msg.serialization.impl;

import io.mqute.msg.ConnectMessage;
import io.mqute.msg.Credential;
import io.mqute.msg.PublishMessage;
import io.mqute.msg.enums.QoSType;
import io.mqute.msg.error.CorruptedMqttPacketException;
import io.mqute.msg.serialization.MqttFixedHeader;
import io.mqute.msg.serialization.util.ProtocolUtils;

import java.io.UnsupportedEncodingException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;


public class ConnectSerializer extends AbstractMqttSerializer<ConnectMessage> {

  public static final byte USERNAME_MASK = (byte) 0x80;
  public static final byte PASSWORD_MASK = (byte) 0x40;
  public static final byte WILL_RETAIN_MASK = (byte) 0x20;
  public static final byte WILL_QOS_MASK = (byte) 0x18;
  public static final byte WILL_MASK = (byte) 0x04;
  public static final byte CLEAN_SESSION_MASK = (byte) 0x02;

  @Override
  public ByteBuffer serialize(ConnectMessage message) {
    int remainingLength = message.getRemainingLength();
    int numOfBytes = ProtocolUtils.numOfBytesToEncode(message.getRemainingLength());
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(1 + numOfBytes + remainingLength);
    byteBuffer.mark();
    serializeHeader(message, byteBuffer);
    ProtocolUtils.encodeString(message.getProtocolName(), byteBuffer);
    byteBuffer.put(message.getProtocolLevel());
    byteBuffer.put(computeConnectFlags(message));
    byteBuffer.putShort(message.getKeepAlive());
    ProtocolUtils.encodeString(message.getClientId(), byteBuffer);
    message.getWillMessageOpt().ifPresent(willMessage -> {
      ProtocolUtils.encodeString(willMessage.getTopicName(), byteBuffer);
      byteBuffer.putShort((short) (willMessage.getPayload().length));
      byteBuffer.put(willMessage.getPayload());
    });
    message.getCredentialOpt().ifPresent(credential -> {
      ProtocolUtils.encodeString(credential.getUsername(), byteBuffer);
      byte[] password = credential.getPassword();
      byteBuffer.putShort((short) password.length);
      byteBuffer.put(password);
    });
    byteBuffer.reset();
    return byteBuffer;
  }

  private byte computeConnectFlags(ConnectMessage message) {
    byte connectFlags = 0;
    if (message.getCredentialOpt().isPresent()) {
      connectFlags |= USERNAME_MASK | PASSWORD_MASK;
    }
    if (message.getWillMessageOpt().isPresent()) {
      connectFlags |= WILL_MASK;
      PublishMessage willMessage = message.getWillMessageOpt().get();
      connectFlags |= willMessage.getQos().getByteValue() << 3 & WILL_QOS_MASK;
      if (willMessage.isRetain()) {
        connectFlags |= WILL_RETAIN_MASK;
      }
    }
    if (message.isCleanSession()) {
      connectFlags |= CLEAN_SESSION_MASK;
    }
    return connectFlags;
  }

  @Override
  public ConnectMessage deserialize(ByteBuffer byteBuffer) {
    MqttFixedHeader header = deserializeHeader(byteBuffer);
    ConnectMessage message;
    try {
      String protocolName = ProtocolUtils.decodeString(byteBuffer);
      byte protocolLevel = byteBuffer.get();
      byte connectFlags = byteBuffer.get();
      boolean cleanSession = (connectFlags & CLEAN_SESSION_MASK) != 0;
      boolean hasWillMessage = (connectFlags & WILL_MASK) != 0;
      QoSType willQoS = QoSType.fromByte((byte) ((connectFlags & WILL_QOS_MASK) >> 3));
      boolean willRetain = (connectFlags & WILL_RETAIN_MASK) != 0;
      boolean hasUsername = (connectFlags & USERNAME_MASK) != 0;
      boolean hasPassword = (connectFlags & PASSWORD_MASK) != 0;
      short keepAlive = byteBuffer.getShort();
      String clientId = ProtocolUtils.decodeString(byteBuffer);
      PublishMessage willMessage = null;
      if (hasWillMessage) {
        String willTopic = ProtocolUtils.decodeString(byteBuffer);
        int payloadSize = byteBuffer.getShort();
        byte[] payload = new byte[payloadSize];
        byteBuffer.get(payload);
        // TODO generate the packet id
        willMessage = new PublishMessage(false, willQoS, willRetain, (short) 0, willTopic, payload);
      }
      Credential credential = null;
      if (hasUsername) {
        String username = ProtocolUtils.decodeString(byteBuffer);
        byte[] password = null;
        if (hasPassword) {
          int passwordSize = byteBuffer.getShort();
          password = new byte[passwordSize];
          byteBuffer.get(password);
        }
        credential = new Credential(username, password);
      }
      message = new ConnectMessage(protocolName, protocolLevel, keepAlive,
          cleanSession, clientId, credential, willMessage);
      if (!validateHeader(message, header)) {
        throw new CorruptedMqttPacketException(
            "Fixed header " + header + " from the buffer doesn't match this message type"
        );
      }
    } catch (UnsupportedEncodingException | BufferUnderflowException | IllegalArgumentException e) {
      throw new CorruptedMqttPacketException(e);
    }
    return message;
  }
}
