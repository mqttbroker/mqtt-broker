package io.mqute.msg.serialization.impl;


import io.mqute.msg.SubAckMessage;
import io.mqute.msg.enums.QoSType;
import io.mqute.msg.error.CorruptedMqttPacketException;
import io.mqute.msg.serialization.MqttFixedHeader;
import io.mqute.msg.serialization.util.ProtocolUtils;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;

public class SubAckSerializer extends AbstractMqttSerializer<SubAckMessage> {

  @Override
  public ByteBuffer serialize(SubAckMessage message) {
    int remainingLength = message.getRemainingLength();
    int numOfBytes = ProtocolUtils.numOfBytesToEncode(message.getRemainingLength());
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(1 + numOfBytes + remainingLength);
    byteBuffer.mark();
    serializeHeader(message, byteBuffer);
    byteBuffer.putShort(message.getPacketId());
    for (QoSType code : message.getReturnCodes()) {
      byteBuffer.put(code.getByteValue());
    }
    byteBuffer.reset();
    return byteBuffer;
  }

  @Override
  public SubAckMessage deserialize(ByteBuffer byteBuffer) {
    MqttFixedHeader header = deserializeHeader(byteBuffer);
    SubAckMessage message;
    try {
      short packetId = byteBuffer.getShort();
      QoSType[] returnCodes = new QoSType[header.getRemainingLength() - 2];
      for (int i = 0; i < returnCodes.length; i++) {
        byte code = byteBuffer.get();
        if (code == (byte) 0x80) {
          returnCodes[i] = QoSType.FAILURE;
        } else {
          returnCodes[i] = QoSType.fromByte(code);
        }
      }
      message = new SubAckMessage(packetId, returnCodes);
      if (!validateHeader(message, header)) {
        throw new CorruptedMqttPacketException(
            "Fixed header " + header + " from the buffer doesn't match this message type"
        );
      }
    } catch (BufferUnderflowException e) {
      throw new CorruptedMqttPacketException(e);
    }
    return message;
  }
}
