package io.mqute.msg.serialization.impl;

import io.mqute.msg.PingRespMessage;
import io.mqute.msg.error.CorruptedMqttPacketException;
import io.mqute.msg.serialization.MqttFixedHeader;
import io.mqute.msg.serialization.util.ProtocolUtils;

import java.nio.ByteBuffer;


public class PingRespSerializer extends AbstractMqttSerializer<PingRespMessage> {

  @Override
  public ByteBuffer serialize(PingRespMessage message) {
    int remainingLength = message.getRemainingLength();
    int numOfBytes = ProtocolUtils.numOfBytesToEncode(message.getRemainingLength());
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(1 + numOfBytes + remainingLength);
    byteBuffer.mark();
    serializeHeader(message, byteBuffer);
    byteBuffer.reset();
    return byteBuffer;
  }

  @Override
  public PingRespMessage deserialize(ByteBuffer byteBuffer) {
    MqttFixedHeader header = deserializeHeader(byteBuffer);
    PingRespMessage message = new PingRespMessage();
    if (!validateHeader(message, header)) {
      throw new CorruptedMqttPacketException(
          "Fixed header " + header + " from the buffer doesn't match this message type"
      );
    }
    return message;
  }
}
