package io.mqute.msg.serialization;


import io.mqute.msg.error.CorruptedMqttPacketException;

import java.nio.ByteBuffer;

/**
 * an IMqttSerializer is responsible for de/serializing MQTT messages
 *
 * @param <Message> type of the MQTT message
 */
public interface IMqttSerializer<Message> {

  /**
   * Serialize message to {@link ByteBuffer}
   *
   * @param message the message need to serialized
   * @return ByteBuffer contains serialized message
   */
  ByteBuffer serialize(Message message);

  /**
   * Deserialize message from {@link ByteBuffer}
   *
   * @param byteBuffer the ByteBuffer from which message is deserialized
   * @return deserialized message
   * @throws CorruptedMqttPacketException
   */
  Message deserialize(ByteBuffer byteBuffer);
}
