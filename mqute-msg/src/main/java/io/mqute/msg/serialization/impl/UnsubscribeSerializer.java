package io.mqute.msg.serialization.impl;

import io.mqute.msg.UnsubscribeMessage;
import io.mqute.msg.error.CorruptedMqttPacketException;
import io.mqute.msg.serialization.MqttFixedHeader;
import io.mqute.msg.serialization.util.ProtocolUtils;

import java.io.UnsupportedEncodingException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;


public class UnsubscribeSerializer extends AbstractMqttSerializer<UnsubscribeMessage> {

  @Override
  public ByteBuffer serialize(UnsubscribeMessage message) {
    int remainingLength = message.getRemainingLength();
    int numOfBytes = ProtocolUtils.numOfBytesToEncode(message.getRemainingLength());
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(1 + numOfBytes + remainingLength);
    byteBuffer.mark();
    serializeHeader(message, byteBuffer);
    byteBuffer.putShort(message.getPacketId());
    for (String topicFilter : message.getTopicFilters()) {
      ProtocolUtils.encodeString(topicFilter, byteBuffer);
    }
    byteBuffer.reset();
    return byteBuffer;
  }

  @Override
  public UnsubscribeMessage deserialize(ByteBuffer byteBuffer) {
    MqttFixedHeader header = deserializeHeader(byteBuffer);
    short packetId = byteBuffer.getShort();
    List<String> topicFilters = new ArrayList<>();
    try {
      while (byteBuffer.hasRemaining()) {
        topicFilters.add(ProtocolUtils.decodeString(byteBuffer));
      }
    } catch (UnsupportedEncodingException | BufferUnderflowException e) {
      throw new CorruptedMqttPacketException(e);
    }
    UnsubscribeMessage message = new UnsubscribeMessage(packetId, topicFilters);
    if (!validateHeader(message, header)) {
      throw new CorruptedMqttPacketException(
          "Fixed header " + header + " from the buffer doesn't match this message type"
      );
    }
    return message;
  }
}
