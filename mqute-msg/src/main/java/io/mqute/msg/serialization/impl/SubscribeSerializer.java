package io.mqute.msg.serialization.impl;


import io.mqute.msg.SubscribeMessage;
import io.mqute.msg.enums.QoSType;
import io.mqute.msg.error.CorruptedMqttPacketException;
import io.mqute.msg.serialization.MqttFixedHeader;
import io.mqute.msg.serialization.util.ProtocolUtils;

import java.io.UnsupportedEncodingException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.LinkedHashMap;

public class SubscribeSerializer extends AbstractMqttSerializer<SubscribeMessage> {

  @Override
  public ByteBuffer serialize(SubscribeMessage message) {
    int remainingLength = message.getRemainingLength();
    int numOfBytes = ProtocolUtils.numOfBytesToEncode(message.getRemainingLength());
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(1 + numOfBytes + remainingLength);
    byteBuffer.mark();
    serializeHeader(message, byteBuffer);
    byteBuffer.putShort(message.getPacketId());
    message.getSubscriptions().entrySet().stream().forEach(entry -> {
      ProtocolUtils.encodeString(entry.getKey(), byteBuffer);
      byteBuffer.put(entry.getValue().getByteValue());
    });
    byteBuffer.reset();
    return byteBuffer;
  }

  @Override
  public SubscribeMessage deserialize(ByteBuffer byteBuffer) {
    MqttFixedHeader header = deserializeHeader(byteBuffer);
    short packetId = byteBuffer.getShort();
    LinkedHashMap<String, QoSType> subscriptions = new LinkedHashMap<>();
    try {
      while (byteBuffer.hasRemaining()) {
        String topic = ProtocolUtils.decodeString(byteBuffer);
        QoSType qos = QoSType.fromByte(byteBuffer.get());
        subscriptions.computeIfPresent(topic, (k, v) -> v.getByteValue() > qos.getByteValue() ? v : qos);
        subscriptions.computeIfAbsent(topic, (t) -> qos);
      }
    } catch (UnsupportedEncodingException | BufferUnderflowException | IllegalArgumentException e) {
      throw new CorruptedMqttPacketException(e);
    }
    SubscribeMessage message = new SubscribeMessage(packetId, subscriptions);
    if (!validateHeader(message, header)) {
      throw new CorruptedMqttPacketException(
          "Fixed header " + header + " from the buffer doesn't match this message type"
      );
    }
    return message;
  }
}
