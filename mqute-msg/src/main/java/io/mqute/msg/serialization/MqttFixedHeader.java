package io.mqute.msg.serialization;


import io.mqute.msg.enums.MessageType;

public class MqttFixedHeader {

  private final MessageType messageType;
  private final byte flags;
  private final int remainingLength;

  public MqttFixedHeader(MessageType messageType, byte flags, int remainingLength) {
    this.messageType = messageType;
    this.flags = flags;
    this.remainingLength = remainingLength;
  }

  public MessageType getMessageType() {
    return messageType;
  }

  public byte getFlags() {
    return flags;
  }

  public int getRemainingLength() {
    return remainingLength;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    return sb.append("(").append(messageType).append(",").append(flags)
        .append(",").append(remainingLength).append(")").toString();
  }
}
