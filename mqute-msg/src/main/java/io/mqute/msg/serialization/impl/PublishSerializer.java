package io.mqute.msg.serialization.impl;


import io.mqute.msg.PublishMessage;
import io.mqute.msg.enums.QoSType;
import io.mqute.msg.error.CorruptedMqttPacketException;
import io.mqute.msg.serialization.MqttFixedHeader;
import io.mqute.msg.serialization.util.ProtocolUtils;

import java.io.UnsupportedEncodingException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;

public class PublishSerializer extends AbstractMqttSerializer<PublishMessage> {

  @Override
  public ByteBuffer serialize(PublishMessage message) {
    int remainingLength = message.getRemainingLength();
    int numOfBytes = ProtocolUtils.numOfBytesToEncode(message.getRemainingLength());
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(1 + numOfBytes + remainingLength);
    byteBuffer.mark();
    serializeHeader(message, byteBuffer);
    ProtocolUtils.encodeString(message.getTopicName(), byteBuffer);
    if (message.getQos() != QoSType.MOST_ONCE) {
      byteBuffer.putShort(message.getPacketId());
    }
    byteBuffer.put(message.getPayload());
    byteBuffer.reset();
    return byteBuffer;
  }

  @Override
  public PublishMessage deserialize(ByteBuffer byteBuffer) {
    MqttFixedHeader header = deserializeHeader(byteBuffer);
    PublishMessage message;
    try {
      String topicName = ProtocolUtils.decodeString(byteBuffer);
      short packetId = 0;
      int payloadSize = header.getRemainingLength() - 2 - topicName.length();
      if ((byte) (header.getFlags() >> 1 & 0x03) != QoSType.MOST_ONCE.getByteValue()) {
        packetId = byteBuffer.getShort();
        payloadSize -= 2;
      }
      byte[] payload = new byte[payloadSize];
      byteBuffer.get(payload);
      message = new PublishMessage(header.getFlags(), packetId, topicName, payload);
      if (!validateHeader(message, header)) {
        throw new CorruptedMqttPacketException(
            "Fixed header " + header + " from the buffer doesn't match this message type"
        );
      }
    } catch (BufferUnderflowException | UnsupportedEncodingException | IllegalArgumentException e) {
      throw new CorruptedMqttPacketException(e);
    }
    return message;
  }
}
