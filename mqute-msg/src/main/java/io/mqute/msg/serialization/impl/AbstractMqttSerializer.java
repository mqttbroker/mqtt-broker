package io.mqute.msg.serialization.impl;


import io.mqute.msg.MqttMessage;
import io.mqute.msg.serialization.IMqttSerializer;
import io.mqute.msg.serialization.MqttFixedHeader;
import io.mqute.msg.serialization.util.ProtocolUtils;

import java.nio.ByteBuffer;

public abstract class AbstractMqttSerializer<Message extends MqttMessage> implements IMqttSerializer<Message> {

  protected void serializeHeader(Message message, ByteBuffer byteBuffer) {
    byteBuffer.put(ProtocolUtils.constructFirstByte(message.getMessageType(), message.getFlags()));
    ProtocolUtils.encodeRemainingLength(message.getRemainingLength(), byteBuffer);
  }


  protected MqttFixedHeader deserializeHeader(ByteBuffer byteBuffer) {
    byte firstByte = byteBuffer.get();
    return new MqttFixedHeader(
        ProtocolUtils.getMessageType(firstByte),
        ProtocolUtils.getFlags(firstByte),
        ProtocolUtils.decodeRemainingLength(byteBuffer)
    );
  }

  protected boolean validateHeader(Message message, MqttFixedHeader header) {
    return message.getMessageType() == header.getMessageType()
        && message.getFlags() == header.getFlags()
        && message.getRemainingLength() == header.getRemainingLength();
  }
}
