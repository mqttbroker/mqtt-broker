package io.mqute.msg;

import io.mqute.msg.enums.MessageType;

/**
 * MQTT Section 3.6
 */
public class PubRelMessage extends IdentifiableMqttMessage {

  private final short packetId;

  public PubRelMessage(short packetId) {
    super(MessageType.PUBREL, (byte) 0x02);
    this.packetId = packetId;
  }

  @Override
  public short getPacketId() {
    return packetId;
  }

  @Override
  public int getRemainingLength() {
    return 2;
  }

}
