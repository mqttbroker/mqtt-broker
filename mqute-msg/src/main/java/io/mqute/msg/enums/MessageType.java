package io.mqute.msg.enums;


public enum MessageType {
  CONNECT((byte) 0x01),
  CONNACK((byte) 0x02),
  PUBLISH((byte) 0x03),
  PUBACK((byte) 0x04),
  PUBREC((byte) 0x05),
  PUBREL((byte) 0x06),
  PUBCOMP((byte) 0x07),
  SUBSCRIBE((byte) 0x08),
  SUBACK((byte) 0x09),
  UNSUBSCRIBE((byte) 0x0A),
  UNSUBACK((byte) 0x0B),
  PINGREQ((byte) 0x0C),
  PINGRESP((byte) 0x0D),
  DISCONNECT((byte) 0x0E);

  private final byte byteValue;

  MessageType(byte byteValue) {
    this.byteValue = byteValue;
  }

  public static MessageType fromByte(byte byteValue) {
    switch (byteValue) {
      case (0x01):
        return CONNECT;
      case (0x02):
        return CONNACK;
      case (0x03):
        return PUBLISH;
      case (0x04):
        return PUBACK;
      case (0x05):
        return PUBREC;
      case (0x06):
        return PUBREL;
      case (0x07):
        return PUBCOMP;
      case (0x08):
        return SUBSCRIBE;
      case (0x09):
        return SUBACK;
      case (0x0A):
        return UNSUBSCRIBE;
      case (0x0B):
        return UNSUBACK;
      case (0x0C):
        return PINGREQ;
      case (0x0D):
        return PINGRESP;
      case (0x0E):
        return DISCONNECT;
      default:
        throw new IllegalArgumentException("Invalid message type " + byteValue + " (expect 0x01 to 0x0E)");
    }
  }

  public byte getByteValue() {
    return byteValue;
  }
}
