package io.mqute.msg.enums;


public enum ConnectReturnCode {
  ACCEPTED((byte) 0x00),
  BAD_PROTOCOL_VERSION((byte) 0x01),
  ID_REJECTED((byte) 0x02),
  SERVER_UNAVAILABLE((byte) 0x03),
  BAD_USERNAME_OR_PASSWORD((byte) 0x04),
  NOT_AUTHORIZED((byte) 0x05);

  private final byte byteValue;

  ConnectReturnCode(byte byteValue) {
    this.byteValue = byteValue;
  }

  public static ConnectReturnCode fromByte(byte byteValue) {
    switch (byteValue) {
      case ((byte) 0x00):
        return ACCEPTED;
      case ((byte) 0x01):
        return BAD_PROTOCOL_VERSION;
      case ((byte) 0x02):
        return ID_REJECTED;
      case ((byte) 0x03):
        return SERVER_UNAVAILABLE;
      case ((byte) 0x04):
        return BAD_USERNAME_OR_PASSWORD;
      case ((byte) 0x05):
        return NOT_AUTHORIZED;
      default:
        throw new IllegalArgumentException("Connect Return Code must between 0x00 ~ 0x05: " + byteValue);
    }
  }

  public byte getByteValue() {
    return byteValue;
  }

}
