package io.mqute.msg.enums;


public enum QoSType {
  MOST_ONCE((byte) 0x00),
  LEAST_ONCE((byte) 0x01),
  EXACT_ONCE((byte) 0x02),
  FAILURE((byte) 0x80);

  private final byte byteValue;

  QoSType(byte byteValue) {
    this.byteValue = byteValue;
  }

  public static QoSType fromByte(byte byteValue) {
    switch (byteValue) {
      case 0x00:
        return MOST_ONCE;
      case 0x01:
        return LEAST_ONCE;
      case 0x02:
        return EXACT_ONCE;
      default:
        throw new IllegalArgumentException("Invalid QoS type " + byteValue + " (expect 0x00 to 0x02)");
    }
  }

  public byte getByteValue() {
    return byteValue;
  }
}
