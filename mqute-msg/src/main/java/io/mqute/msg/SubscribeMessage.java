package io.mqute.msg;

import io.mqute.msg.enums.MessageType;
import io.mqute.msg.enums.QoSType;
import io.mqute.msg.serialization.util.ProtocolUtils;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * MQTT Section 3.8
 */
public class SubscribeMessage extends IdentifiableMqttMessage {

  private final short packetId;
  private final LinkedHashMap<String, QoSType> subscriptions;
  private int remainingLength;

  public SubscribeMessage(short packetId, LinkedHashMap<String, QoSType> subscriptions) {
    super(MessageType.SUBSCRIBE, (byte) 0x02);
    this.packetId = packetId;
    this.subscriptions = subscriptions;
    remainingLength = 2;
    for (Map.Entry<String, QoSType> entry : subscriptions.entrySet()) {
      String topic = entry.getKey();
      remainingLength += 2 + topic.getBytes(ProtocolUtils.UTF_8).length + 1;
    }
  }

  @Override
  public short getPacketId() {
    return packetId;
  }

  public Map<String, QoSType> getSubscriptions() {
    return Collections.unmodifiableMap(subscriptions);
  }

  @Override
  public int getRemainingLength() {
    return remainingLength;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof SubscribeMessage) {
      SubscribeMessage message = (SubscribeMessage) obj;
      return subscriptions.equals(message.subscriptions) && super.equals(message);
    } else {
      return false;
    }
  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    result = result * 37 + subscriptions.hashCode();
    return result;
  }
}
