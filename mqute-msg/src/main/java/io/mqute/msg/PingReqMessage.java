package io.mqute.msg;

import io.mqute.msg.enums.MessageType;

/**
 * MQTT 3.12
 */
public class PingReqMessage extends MqttMessage {

  public PingReqMessage() {
    super(MessageType.PINGREQ, (byte) 0x00);
  }

  @Override
  public int getRemainingLength() {
    return 0;
  }

}
