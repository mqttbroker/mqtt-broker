package io.mqute.msg;

import io.mqute.msg.enums.MessageType;

/**
 * MQTT Section 3.5
 */
public class PubRecMessage extends IdentifiableMqttMessage {

  private final short packetId;

  public PubRecMessage(short packetId) {
    super(MessageType.PUBREC, (byte) 0x00);
    this.packetId = packetId;
  }

  @Override
  public short getPacketId() {
    return packetId;
  }

  @Override
  public int getRemainingLength() {
    return 2;
  }

}
