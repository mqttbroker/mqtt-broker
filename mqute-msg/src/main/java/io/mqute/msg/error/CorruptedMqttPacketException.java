package io.mqute.msg.error;


public class CorruptedMqttPacketException extends RuntimeException {

  private static final long serialVersionUID = -4274276298326136670L;

  private static final String ERROR_MESSAGE = "The MQTT message frame is corrupted";

  public CorruptedMqttPacketException() {
    super(ERROR_MESSAGE);
  }

  public CorruptedMqttPacketException(String errorMsg) {
    super(errorMsg);
  }

  public CorruptedMqttPacketException(Throwable cause) {
    super(ERROR_MESSAGE, cause);
  }

  public CorruptedMqttPacketException(String errorMsg, Throwable cause) {
    super(errorMsg, cause);
  }

}
