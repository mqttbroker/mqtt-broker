package io.mqute.msg;

import io.mqute.msg.enums.MessageType;


public abstract class IdentifiableMqttMessage extends MqttMessage {

  public IdentifiableMqttMessage(MessageType messageType, byte flags) {
    super(messageType, flags);
  }

  public abstract short getPacketId();

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof IdentifiableMqttMessage) {
      IdentifiableMqttMessage message = (IdentifiableMqttMessage) obj;
      return getPacketId() == message.getPacketId() && super.equals(message);
    } else {
      return false;
    }
  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    return result * 37 + (int) getPacketId();
  }
}
