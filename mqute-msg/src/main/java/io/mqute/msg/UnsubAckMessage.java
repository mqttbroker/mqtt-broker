package io.mqute.msg;

import io.mqute.msg.enums.MessageType;

/**
 * MQTT Section 3.11
 */
public class UnsubAckMessage extends IdentifiableMqttMessage {

  private final short packetId;

  public UnsubAckMessage(short packetId) {
    super(MessageType.UNSUBACK, (byte) 0x00);
    this.packetId = packetId;
  }

  @Override
  public short getPacketId() {
    return packetId;
  }

  @Override
  public int getRemainingLength() {
    return 2;
  }

}
