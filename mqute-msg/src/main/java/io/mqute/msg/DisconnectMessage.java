package io.mqute.msg;

import io.mqute.msg.enums.MessageType;

/**
 * MQTT 3.14
 */
public class DisconnectMessage extends MqttMessage {

  public DisconnectMessage() {
    super(MessageType.DISCONNECT, (byte) 0x00);
  }

  @Override
  public int getRemainingLength() {
    return 0;
  }

}
