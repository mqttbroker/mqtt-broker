package io.mqute.msg;

import io.mqute.msg.enums.MessageType;

/**
 * MQTT Section 3.7
 */
public class PubCompMessage extends IdentifiableMqttMessage {

  private final short packetId;

  public PubCompMessage(short packetId) {
    super(MessageType.PUBCOMP, (byte) 0x00);
    this.packetId = packetId;
  }

  @Override
  public short getPacketId() {
    return packetId;
  }

  @Override
  public int getRemainingLength() {
    return 2;
  }
}
