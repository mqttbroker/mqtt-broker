package io.mqute.msg;

import io.mqute.msg.enums.MessageType;
import io.mqute.msg.enums.QoSType;

import java.util.Arrays;
import java.util.Optional;

/**
 * MQTT Section 3.3
 */
public class PublishMessage extends IdentifiableMqttMessage {

  private final String topicName;
  private final byte[] payload;
  private final int remainingLength;
  private short packetId;
  private boolean dup;
  private QoSType qos;
  private boolean retain;

  public PublishMessage(boolean dup, QoSType qos, boolean retain, short packetId, String topicName, byte[] payload) {
    this((byte) ((dup ? 1 : 0) << 3 | qos.getByteValue() << 1 | (retain ? 1 : 0)), packetId, topicName, payload);
    this.dup = dup;
    this.qos = qos;
    this.retain = retain;
  }

  public PublishMessage(byte flags, short packetId, String topicName, byte[] payload) {
    super(MessageType.PUBLISH, flags);
    this.topicName = topicName;
    this.packetId = packetId;
    this.payload = payload;
    remainingLength = 2 + topicName.length() + (getQos() != QoSType.MOST_ONCE ? 2 : 0) + payload.length;
  }

  /**
   * MQTT Section 3.3.1.1
   *
   * @return if the packet is duplicate
   */
  public boolean isDup() {
    return Optional.ofNullable(dup).orElseGet(() -> (getFlags() & 0x08) != 0);
  }

  /**
   * MQTT Section 3.3.1.2
   *
   * @return QoS of the publish message
   */
  public QoSType getQos() {
    return Optional.ofNullable(qos).orElseGet(() -> QoSType.fromByte((byte) (getFlags() >> 1 & 0x03)));
  }

  /**
   * MQTT Section 3.3.1.3
   *
   * @return if the message needs to be retain
   */
  public boolean isRetain() {
    return Optional.ofNullable(retain).orElseGet(() -> (getFlags() & 0x01) != 0);
  }

  /**
   * MQTT Section 3.3.2.1
   *
   * @return the topic name
   */
  public String getTopicName() {
    return topicName;
  }

  /**
   * MQTT Section 3.3.2.2
   *
   * @return the packet id
   */
  @Override
  public short getPacketId() {
    return packetId;
  }

  /**
   * MQTT Section 3.3
   *
   * @return the payload of PUBLISH packet
   */
  public byte[] getPayload() {
    return payload;
  }

  @Override
  public int getRemainingLength() {
    return remainingLength;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof PublishMessage) {
      PublishMessage message = (PublishMessage) obj;
      return topicName.equals(message.topicName) && Arrays.equals(payload, message.payload) && super.equals(message);
    } else {
      return false;
    }
  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    result += result * 37 + topicName.hashCode();
    result += result * 37 + Arrays.hashCode(payload);
    return result;
  }

}
