package io.mqute.msg;

import io.mqute.msg.enums.MessageType;
import io.mqute.msg.serialization.util.ProtocolUtils;

import java.util.Objects;
import java.util.Optional;

/**
 * MQTT 3.1
 */
public class ConnectMessage extends MqttMessage {

  private final int remainingLength;
  // variable header part
  private final String protocolName;
  private final byte protocolLevel;
  private final short keepAlive;
  private final boolean cleanSession;

  // payload part
  private final String clientId;
  private final Credential credential;
  private final PublishMessage willMessage;


  public ConnectMessage(String protocolName, byte protocolLevel, short keepAlive,
                        boolean cleanSession, String clientId) {
    this(protocolName, protocolLevel, keepAlive, cleanSession, clientId, null);
  }

  public ConnectMessage(String protocolName, byte protocolLevel, short keepAlive,
                        boolean cleanSession, String clientId, Credential credential) {
    this(protocolName, protocolLevel, keepAlive, cleanSession, clientId, credential, null);
  }

  public ConnectMessage(String protocolName, byte protocolLevel, short keepAlive,
                        boolean cleanSession, String clientId, Credential credential, PublishMessage willMessage) {
    super(MessageType.CONNECT, (byte) 0x00);
    this.protocolName = protocolName;
    this.protocolLevel = protocolLevel;
    this.keepAlive = keepAlive;
    this.cleanSession = cleanSession;
    this.clientId = clientId;
    this.credential = credential;
    this.willMessage = willMessage;
    Optional<Credential> credentialOpt = Optional.ofNullable(credential);
    Optional<PublishMessage> willMessageOpt = Optional.ofNullable(willMessage);
    remainingLength = 2 + protocolName.getBytes(ProtocolUtils.UTF_8).length + 4
        + 2 + clientId.getBytes(ProtocolUtils.UTF_8).length
        + willMessageOpt.map(m -> 2 + m.getTopicName().getBytes(ProtocolUtils.UTF_8).length).orElse(0)
        + willMessageOpt.map(m -> 2 + m.getPayload().length).orElse(0)
        + credentialOpt.map(c -> 2 + c.getUsername().getBytes(ProtocolUtils.UTF_8).length).orElse(0)
        + credentialOpt.map(c -> 2 + c.getPassword().length).orElse(0);
  }

  /**
   * MQTT 3.1.2.1
   *
   * @return
   */
  public String getProtocolName() {
    return protocolName;
  }

  /**
   * MQTT 3.1.2.2
   *
   * @return
   */
  public byte getProtocolLevel() {
    return protocolLevel;
  }

  /**
   * MQTT 3.1.2.4
   *
   * @return
   */
  public boolean isCleanSession() {
    return cleanSession;
  }

  /**
   * MQTT 3.1.2.10
   *
   * @return
   */
  public short getKeepAlive() {
    return keepAlive;
  }

  /**
   * MQTT 3.1.3.1
   *
   * @return
   */
  public String getClientId() {
    return clientId;
  }

  /**
   * MQTT 3.1.2.5, 3.1.2.6, 3.1.2.7, 3.1.3.2, 3.1.3.3
   *
   * @return
   */
  public Optional<PublishMessage> getWillMessageOpt() {
    return Optional.ofNullable(willMessage);
  }

  /**
   * MQTT 3.1.2.8, 3.1.2.9, 3.1.3.4, 3.1.3.5
   *
   * @return
   */
  public Optional<Credential> getCredentialOpt() {
    return Optional.ofNullable(credential);
  }

  @Override
  public int getRemainingLength() {
    return remainingLength;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof ConnectMessage) {
      ConnectMessage message = (ConnectMessage) obj;
      return protocolName.equals(message.protocolName) && protocolLevel == message.protocolLevel
          && keepAlive == message.keepAlive && cleanSession == message.cleanSession
          && clientId.equals(message.clientId) && Objects.equals(credential, message.credential)
          && Objects.equals(willMessage, message.willMessage) && super.equals(message);
    } else {
      return false;
    }
  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    result = result * 37 + protocolName.hashCode();
    result = result * 37 + (int) protocolLevel;
    result = result * 37 + (int) keepAlive;
    result = result * 37 + (cleanSession ? 1 : 0);
    result = result * 37 + clientId.hashCode();
    if (credential != null) {
      result = result * 37 + credential.hashCode();
    }
    if (willMessage != null) {
      result = result * 37 + willMessage.hashCode();
    }
    return result;
  }
}
