package io.mqute.msg;

import io.mqute.msg.enums.MessageType;

/**
 * MQTT 3.13
 */
public class PingRespMessage extends MqttMessage {

  public PingRespMessage() {
    super(MessageType.PINGRESP, (byte) 0x00);
  }

  @Override
  public int getRemainingLength() {
    return 0;
  }

}
