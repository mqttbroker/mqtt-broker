package io.mqute.msg.internal;

import io.mqute.msg.MqttMessage;

/**
 * This class represents the wrapped message from processor node to the connection node.
 * It contains the following:
 * <ul>
 * <li>the original MQTT message needs to be sent to client</li>
 * <li>the client id to find out original connection</li>
 * <li>the partition to indicate from which partition it comes</li>
 * </ul>
 */
public class OutgoingMessageWrapper {

  private final MqttMessage originalMessage;
  private final String clientId;
  private final int partition;

  public OutgoingMessageWrapper(final MqttMessage originalMessage, final String clientId) {

    this(originalMessage, clientId, -1);
  }

  public OutgoingMessageWrapper(final MqttMessage originalMessage,
                                final String clientId,
                                final int partition) {
    this.originalMessage = originalMessage;
    this.clientId = clientId;
    this.partition = partition;
  }

  /**
   * Get the client id to whom this message will be sent.
   * @return client id
   */
  public String getClientId() {
    return clientId;
  }

  /**
   * Get the original outgoing MQTT message.
   * @return the original MQTT message needs to be sent
   */
  public MqttMessage getOriginalMessage() {
    return originalMessage;
  }

  /**
   * Get the partition which processor node belongs to.
   * @return the partition
   */
  public int getPartition() {
    return partition;
  }
}
