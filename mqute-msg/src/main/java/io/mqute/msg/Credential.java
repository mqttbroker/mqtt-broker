package io.mqute.msg;

import java.util.Arrays;


public class Credential {

  private final String username;
  private final byte[] password;

  public Credential(String username, byte[] password) {
    this.username = username;
    this.password = password;
  }

  public String getUsername() {
    return username;
  }

  public byte[] getPassword() {
    return password;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof Credential) {
      Credential credential = (Credential) obj;
      return username.equals(credential.username) && Arrays.equals(password, credential.password);
    } else {
      return false;
    }
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = result * 37 + username.hashCode();
    result = result * 37 + Arrays.hashCode(password);
    return result;
  }

}
