package io.mqute.msg;

import io.mqute.msg.enums.MessageType;
import io.mqute.msg.serialization.util.ProtocolUtils;

import java.util.List;

/**
 * MQTT Section 3.10
 */
public class UnsubscribeMessage extends IdentifiableMqttMessage {

  private final short packetId;
  private final List<String> topicFilters;
  private int remainingLength;

  public UnsubscribeMessage(short packetId, List<String> topicFilters) {
    super(MessageType.UNSUBSCRIBE, (byte) 0x02);
    this.packetId = packetId;
    this.topicFilters = topicFilters;
    remainingLength = 2;
    for (String topicFilter : topicFilters) {
      remainingLength += 2 + topicFilter.getBytes(ProtocolUtils.UTF_8).length;
    }
  }

  @Override
  public short getPacketId() {
    return packetId;
  }

  public List<String> getTopicFilters() {
    return topicFilters;
  }

  @Override
  public int getRemainingLength() {
    return remainingLength;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof UnsubscribeMessage) {
      UnsubscribeMessage message = (UnsubscribeMessage) obj;
      return topicFilters.equals(message.topicFilters) && super.equals(message);
    } else {
      return false;
    }
  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    result = result * 37 + topicFilters.hashCode();
    return result;
  }

}
