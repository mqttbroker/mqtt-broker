package io.mqute.msg;

import io.mqute.msg.enums.MessageType;


public abstract class MqttMessage {

  private final MessageType messageType;
  private final byte flags;

  public MqttMessage(MessageType messageType, byte flags) {
    this.messageType = messageType;
    this.flags = flags;
  }

  public MessageType getMessageType() {
    return messageType;
  }

  public byte getFlags() {
    return flags;
  }

  public abstract int getRemainingLength();

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof MqttMessage) {
      MqttMessage message = (MqttMessage) obj;
      return messageType == message.messageType && flags == message.flags
          && getRemainingLength() == message.getRemainingLength();
    } else {
      return false;
    }
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = result * 37 + messageType.hashCode();
    result = result * 37 + (int) flags;
    result = result * 37 + getRemainingLength();
    return result;
  }

  @Override
  public String toString() {
    return "MqttMessage{" +
        "messageType=" + messageType +
        ", flags=" + flags +
        '}';
  }
}
