package io.mqute.msg;

import io.mqute.msg.enums.ConnectReturnCode;
import io.mqute.msg.enums.MessageType;

/**
 * MQTT Section 3.2
 */
public class ConnAckMessage extends MqttMessage {

  private final ConnectReturnCode returnCode;
  private final boolean sessionPresent;

  public ConnAckMessage(ConnectReturnCode returnCode, boolean sessionPresent) {
    super(MessageType.CONNACK, (byte) 0x00);
    this.returnCode = returnCode;
    this.sessionPresent = sessionPresent;
  }

  @Override
  public int getRemainingLength() {
    return 2;
  }

  /**
   * MQTT Section 3.2.2.3
   *
   * @return return code for CONNECT request
   */
  public ConnectReturnCode getReturnCode() {
    return returnCode;
  }

  /**
   * MQTT Section 3.2.2.2
   *
   * @return if the session is present
   */
  public boolean isSessionPresent() {
    return sessionPresent;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof ConnAckMessage) {
      ConnAckMessage message = (ConnAckMessage) obj;
      return returnCode == message.returnCode && sessionPresent == message.sessionPresent && super.equals(message);
    } else {
      return false;
    }
  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    result = result * 37 + returnCode.hashCode();
    result = result * 37 + (sessionPresent ? 1 : 0);
    return result;
  }

}
