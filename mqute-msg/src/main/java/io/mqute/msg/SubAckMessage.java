package io.mqute.msg;

import io.mqute.msg.enums.MessageType;
import io.mqute.msg.enums.QoSType;

import java.util.Arrays;

/**
 * MQTT Section 3.9
 */
public class SubAckMessage extends IdentifiableMqttMessage {

  private final short packetId;
  private final QoSType[] returnCodes;
  private final int remainingLength;

  public SubAckMessage(short packetId, QoSType[] returnCodes) {
    super(MessageType.SUBACK, (byte) 0x00);
    this.packetId = packetId;
    this.returnCodes = returnCodes;
    remainingLength = 2 + returnCodes.length;
  }

  @Override
  public short getPacketId() {
    return packetId;
  }

  public QoSType[] getReturnCodes() {
    return returnCodes;
  }

  @Override
  public int getRemainingLength() {
    return remainingLength;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof SubAckMessage) {
      SubAckMessage message = (SubAckMessage) obj;
      return Arrays.equals(returnCodes, message.returnCodes) && super.equals(message);
    } else {
      return false;
    }
  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    result = result * 37 + Arrays.hashCode(returnCodes);
    return result;
  }

}
