package io.mqute.msg;

import io.mqute.msg.enums.MessageType;

/**
 * MQTT Section 3.4
 */
public class PubAckMessage extends IdentifiableMqttMessage {

  private final short packetId;

  public PubAckMessage(short packetId) {
    super(MessageType.PUBACK, (byte) 0x00);
    this.packetId = packetId;
  }

  @Override
  public short getPacketId() {
    return packetId;
  }

  @Override
  public int getRemainingLength() {
    return 2;
  }

}
