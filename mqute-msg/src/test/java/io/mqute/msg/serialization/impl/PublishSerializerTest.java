package io.mqute.msg.serialization.impl;

import io.mqute.msg.PublishMessage;
import io.mqute.msg.enums.MessageType;
import io.mqute.msg.enums.QoSType;
import io.mqute.msg.serialization.util.ProtocolUtils;
import org.junit.Assert;
import org.junit.Test;

import java.nio.ByteBuffer;


public class PublishSerializerTest extends MqttSerializerTest<PublishMessage> {

  private PublishSerializer serializer = new PublishSerializer();
  private short packetId = (short) 123;
  private String topicName = "a/b/c";
  private byte[] payload = "hello world".getBytes(ProtocolUtils.UTF_8);

  @Override
  AbstractMqttSerializer<PublishMessage> serializer() {
    return serializer;
  }

  @Override
  int remainingLength() {
    return 2 + 2 + topicName.length() + payload.length;
  }

  @Override
  PublishMessage message() {
    return new PublishMessage(false, QoSType.LEAST_ONCE, true, packetId, topicName, payload);
  }

  @Override
  ByteBuffer rawData() {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.PUBLISH, (byte) 0x03));
    ProtocolUtils.encodeRemainingLength(remainingLength(), byteBuffer);
    ProtocolUtils.encodeString(topicName, byteBuffer);
    byteBuffer.putShort(packetId);
    byteBuffer.put(payload);
    byteBuffer.reset();
    return byteBuffer;
  }

  @Override
  ByteBuffer rawDataWithBadMessageType() {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.PUBACK, (byte) 0x03));
    ProtocolUtils.encodeRemainingLength(remainingLength(), byteBuffer);
    ProtocolUtils.encodeString(topicName, byteBuffer);
    byteBuffer.putShort(packetId);
    byteBuffer.put(payload);
    byteBuffer.reset();
    return byteBuffer;
  }

  @Override
  ByteBuffer rawDataWithBadFlag() {
    return null;
  }

  @Override
  @Test
  public void testDeserializeCorruptedFlags() throws Exception {
    // header flags are not reserved in PUBLISH packet
    // Disable this test for PUBLISH packet
    Assert.assertTrue(true);
  }
}