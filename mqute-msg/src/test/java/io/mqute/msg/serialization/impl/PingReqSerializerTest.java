package io.mqute.msg.serialization.impl;

import io.mqute.msg.PingReqMessage;
import io.mqute.msg.enums.MessageType;
import io.mqute.msg.error.CorruptedMqttPacketException;
import io.mqute.msg.serialization.util.ProtocolUtils;
import org.junit.Test;

import java.nio.ByteBuffer;


public class PingReqSerializerTest extends MqttSerializerTest<PingReqMessage> {

  private PingReqSerializer serializer = new PingReqSerializer();

  @Override
  AbstractMqttSerializer<PingReqMessage> serializer() {
    return serializer;
  }

  @Override
  int remainingLength() {
    return 0;
  }

  @Override
  PingReqMessage message() {
    return new PingReqMessage();
  }

  @Override
  ByteBuffer rawData() {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.PINGREQ, (byte) 0x00));
    ProtocolUtils.encodeRemainingLength(remainingLength(), byteBuffer);
    byteBuffer.reset();
    return byteBuffer;
  }

  @Override
  ByteBuffer rawDataWithBadMessageType() {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.PINGRESP, (byte) 0x00));
    ProtocolUtils.encodeRemainingLength(remainingLength(), byteBuffer);
    byteBuffer.reset();
    return byteBuffer;
  }

  @Override
  ByteBuffer rawDataWithBadFlag() {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.PINGREQ, (byte) 0x01));
    ProtocolUtils.encodeRemainingLength(remainingLength(), byteBuffer);
    byteBuffer.reset();
    return byteBuffer;
  }

  @Test(expected = CorruptedMqttPacketException.class)
  public void testDeserializeCorruptedRemainingLength() throws Exception {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.PINGREQ, (byte) 0x00));
    byteBuffer.put((byte) 0x01);
    byteBuffer.reset();

    serializer.deserialize(byteBuffer);
  }

}