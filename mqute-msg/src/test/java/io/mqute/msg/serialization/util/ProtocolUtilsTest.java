package io.mqute.msg.serialization.util;

import io.mqute.msg.enums.MessageType;
import io.mqute.msg.error.CorruptedMqttPacketException;
import org.junit.Assert;
import org.junit.Test;

import java.nio.ByteBuffer;


public class ProtocolUtilsTest {

  @Test
  public void testDecodeRemainingLength0() throws Exception {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(2);
    byteBuffer.put((byte) 0x00);
    byteBuffer.put((byte) 0x00);
    byteBuffer.position(1);
    Assert.assertEquals(0, ProtocolUtils.decodeRemainingLength(byteBuffer));
  }

  @Test
  public void testDecodeRemainingLength127() throws Exception {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(2);
    byteBuffer.put((byte) 0x00);
    byteBuffer.put((byte) 0x7F);
    byteBuffer.position(1);
    Assert.assertEquals(127, ProtocolUtils.decodeRemainingLength(byteBuffer));
  }

  @Test
  public void testDecodeRemainingLength128() throws Exception {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(3);
    byteBuffer.put((byte) 0x00);
    byteBuffer.put((byte) 0x80);
    byteBuffer.put((byte) 0x01);
    byteBuffer.position(1);
    Assert.assertEquals(128, ProtocolUtils.decodeRemainingLength(byteBuffer));
  }

  @Test
  public void testDecodeRemainingLength16383() throws Exception {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(3);
    byteBuffer.put((byte) 0x00);
    byteBuffer.put((byte) 0xFF);
    byteBuffer.put((byte) 0x7F);
    byteBuffer.position(1);
    Assert.assertEquals(16383, ProtocolUtils.decodeRemainingLength(byteBuffer));
  }

  @Test
  public void testDecodeRemainingLength16384() throws Exception {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(4);
    byteBuffer.put((byte) 0x00);
    byteBuffer.put((byte) 0x80);
    byteBuffer.put((byte) 0x80);
    byteBuffer.put((byte) 0x01);
    byteBuffer.position(1);
    Assert.assertEquals(16384, ProtocolUtils.decodeRemainingLength(byteBuffer));
  }

  @Test
  public void testDecodeRemainingLength2097151() throws Exception {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(4);
    byteBuffer.put((byte) 0x00);
    byteBuffer.put((byte) 0xFF);
    byteBuffer.put((byte) 0xFF);
    byteBuffer.put((byte) 0x7F);
    byteBuffer.position(1);
    Assert.assertEquals(2097151, ProtocolUtils.decodeRemainingLength(byteBuffer));
  }

  @Test
  public void testDecodeRemainingLength2097152() throws Exception {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(5);
    byteBuffer.put((byte) 0x00);
    byteBuffer.put((byte) 0x80);
    byteBuffer.put((byte) 0x80);
    byteBuffer.put((byte) 0x80);
    byteBuffer.put((byte) 0x01);
    byteBuffer.position(1);
    Assert.assertEquals(2097152, ProtocolUtils.decodeRemainingLength(byteBuffer));
  }

  @Test
  public void testDecodeRemainingLength268435455() throws Exception {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(5);
    byteBuffer.put((byte) 0x00);
    byteBuffer.put((byte) 0xFF);
    byteBuffer.put((byte) 0xFF);
    byteBuffer.put((byte) 0xFF);
    byteBuffer.put((byte) 0x7F);
    byteBuffer.position(1);
    Assert.assertEquals(268435455, ProtocolUtils.decodeRemainingLength(byteBuffer));
  }

  @Test(expected = IllegalArgumentException.class)
  public void testDecodeRemainingLengthBadBufferPosition() throws Exception {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(2);
    byteBuffer.put((byte) 0x00);
    byteBuffer.put((byte) 0x00);
    byteBuffer.position(0);
    ProtocolUtils.decodeRemainingLength(byteBuffer);
  }

  @Test(expected = CorruptedMqttPacketException.class)
  public void testDecodeRemainingLengthBufferUnderFlow() throws Exception {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(1);
    byteBuffer.put((byte) 0x00);
    byteBuffer.position(1);
    ProtocolUtils.decodeRemainingLength(byteBuffer);
  }

  @Test
  public void testEncodeRemainingLength0() throws Exception {
    int length = 0;
    byte[] expected = {0x00};
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(2);
    byteBuffer.put((byte) 0x00);
    ProtocolUtils.encodeRemainingLength(length, byteBuffer);
    byteBuffer.position(1);
    byte[] actual = new byte[1];
    byteBuffer.get(actual);
    Assert.assertArrayEquals(expected, actual);
  }

  @Test
  public void testEncodeRemainingLength127() throws Exception {
    int length = 127;
    byte[] expected = {0x7F};
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(2);
    byteBuffer.put((byte) 0x00);
    ProtocolUtils.encodeRemainingLength(length, byteBuffer);
    byteBuffer.position(1);
    byte[] actual = new byte[1];
    byteBuffer.get(actual);
    Assert.assertArrayEquals(expected, actual);
  }

  @Test
  public void testEncodeRemainingLength128() throws Exception {
    int length = 128;
    byte[] expected = {(byte) 0x80, 0x01};
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(3);
    byteBuffer.put((byte) 0x00);
    ProtocolUtils.encodeRemainingLength(length, byteBuffer);
    byteBuffer.position(1);
    byte[] actual = new byte[2];
    byteBuffer.get(actual);
    Assert.assertArrayEquals(expected, actual);
  }

  @Test
  public void testEncodeRemainingLength16383() throws Exception {
    int length = 16383;
    byte[] expected = {(byte) 0xFF, 0x7F};
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(3);
    byteBuffer.put((byte) 0x00);
    ProtocolUtils.encodeRemainingLength(length, byteBuffer);
    byteBuffer.position(1);
    byte[] actual = new byte[2];
    byteBuffer.get(actual);
    Assert.assertArrayEquals(expected, actual);
  }

  @Test
  public void testEncodeRemainingLength16384() throws Exception {
    int length = 16384;
    byte[] expected = {(byte) 0x80, (byte) 0x80, 0x01};
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(4);
    byteBuffer.put((byte) 0x00);
    ProtocolUtils.encodeRemainingLength(length, byteBuffer);
    byteBuffer.position(1);
    byte[] actual = new byte[3];
    byteBuffer.get(actual);
    Assert.assertArrayEquals(expected, actual);
  }

  @Test
  public void testEncodeRemainingLength2097151() throws Exception {
    int length = 2097151;
    byte[] expected = {(byte) 0xFF, (byte) 0xFF, 0x7F};
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(4);
    byteBuffer.put((byte) 0x00);
    ProtocolUtils.encodeRemainingLength(length, byteBuffer);
    byteBuffer.position(1);
    byte[] actual = new byte[3];
    byteBuffer.get(actual);
    Assert.assertArrayEquals(expected, actual);
  }

  @Test
  public void testEncodeRemainingLength2097152() throws Exception {
    int length = 2097152;
    byte[] expected = {(byte) 0x80, (byte) 0x80, (byte) 0x80, 0x01};
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(5);
    byteBuffer.put((byte) 0x00);
    ProtocolUtils.encodeRemainingLength(length, byteBuffer);
    byteBuffer.position(1);
    byte[] actual = new byte[4];
    byteBuffer.get(actual);
    Assert.assertArrayEquals(expected, actual);
  }

  @Test
  public void testEncodeRemainingLength268435455() throws Exception {
    int length = 268435455;
    byte[] expected = {(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, 0x7F};
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(5);
    byteBuffer.put((byte) 0x00);
    ProtocolUtils.encodeRemainingLength(length, byteBuffer);
    byteBuffer.position(1);
    byte[] actual = new byte[4];
    byteBuffer.get(actual);
    Assert.assertArrayEquals(expected, actual);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testEncodeRemainingLengthBadBufferPosition() throws Exception {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(2);
    ProtocolUtils.encodeRemainingLength(0, byteBuffer);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testEncodeRemainingLengthOutOfLowerBonnd() throws Exception {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(2);
    ProtocolUtils.encodeRemainingLength(-1, byteBuffer);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testEncodeRemainingLengthOutOfUpperBound() throws Exception {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(2);
    ProtocolUtils.encodeRemainingLength(268435456, byteBuffer);
  }

  @Test(expected = CorruptedMqttPacketException.class)
  public void testEncodeRemainingLengthBufferOverFlow() throws Exception {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(1);
    byteBuffer.put((byte) 0x00);
    byteBuffer.position(1);
    ProtocolUtils.encodeRemainingLength(0, byteBuffer);
  }

  @Test
  public void testNumBytesToEncode1() throws Exception {
    int min = 0;
    int max = 127;
    Assert.assertEquals(1, ProtocolUtils.numOfBytesToEncode(min));
    Assert.assertEquals(1, ProtocolUtils.numOfBytesToEncode((min + max) / 2));
    Assert.assertEquals(1, ProtocolUtils.numOfBytesToEncode(max));
  }

  @Test
  public void testNumBytesToEncode2() throws Exception {
    int min = 128;
    int max = 16383;
    Assert.assertEquals(2, ProtocolUtils.numOfBytesToEncode(min));
    Assert.assertEquals(2, ProtocolUtils.numOfBytesToEncode((min + max) / 2));
    Assert.assertEquals(2, ProtocolUtils.numOfBytesToEncode(max));
  }

  @Test
  public void testNumBytesToEncode3() throws Exception {
    int min = 16384;
    int max = 2097151;
    Assert.assertEquals(3, ProtocolUtils.numOfBytesToEncode(min));
    Assert.assertEquals(3, ProtocolUtils.numOfBytesToEncode((min + max) / 2));
    Assert.assertEquals(3, ProtocolUtils.numOfBytesToEncode(max));
  }

  @Test
  public void testNumBytesToEncode4() throws Exception {
    int min = 2097152;
    int max = 268435455;
    Assert.assertEquals(4, ProtocolUtils.numOfBytesToEncode(min));
    Assert.assertEquals(4, ProtocolUtils.numOfBytesToEncode((min + max) / 2));
    Assert.assertEquals(4, ProtocolUtils.numOfBytesToEncode(max));
  }

  @Test(expected = IllegalArgumentException.class)
  public void testNumBytesToEncodeOutOfLowerBound() throws Exception {
    ProtocolUtils.numOfBytesToEncode(-1);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testNumBytesToEncodeOutOfUpperBound() throws Exception {
    ProtocolUtils.numOfBytesToEncode(268435456);
  }

  @Test
  public void testDecodeString() throws Exception {
    String string = "Hello World";
    byte[] bytes = string.getBytes(ProtocolUtils.UTF_8);
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(2 + bytes.length);
    byteBuffer.mark();
    byteBuffer.putShort((short) bytes.length);
    byteBuffer.put(bytes);
    byteBuffer.reset();
    Assert.assertEquals(string, ProtocolUtils.decodeString(byteBuffer));
  }

  @Test(expected = CorruptedMqttPacketException.class)
  public void testDecodeStringException() throws Exception {
    String string = "Hello World";
    byte[] bytes = string.getBytes(ProtocolUtils.UTF_8);
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(2 + bytes.length - 1);
    byteBuffer.mark();
    byteBuffer.putShort((short) bytes.length);
    byteBuffer.put(bytes, 0, bytes.length - 1);
    byteBuffer.reset();
    ProtocolUtils.decodeString(byteBuffer);
  }

  @Test
  public void testEncodeString() throws Exception {
    String string = "Hello World";
    byte[] bytes = string.getBytes(ProtocolUtils.UTF_8);
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(2 + bytes.length);
    byteBuffer.mark();
    ProtocolUtils.encodeString(string, byteBuffer);
    byteBuffer.reset();
    Assert.assertEquals(bytes.length, (int) byteBuffer.getShort());
    byte[] result = new byte[bytes.length];
    byteBuffer.get(result);
    Assert.assertArrayEquals(bytes, result);
  }

  @Test(expected = CorruptedMqttPacketException.class)
  public void testEncodeStringException() throws Exception {
    String string = "Hello World";
    byte[] bytes = string.getBytes(ProtocolUtils.UTF_8);
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(2 + bytes.length - 1);
    byteBuffer.mark();
    ProtocolUtils.encodeString(string, byteBuffer);
  }

  @Test
  public void testConstructFirstByte() throws Exception {
    byte firstByte = ProtocolUtils.constructFirstByte(MessageType.PUBLISH, (byte) 0x0B);
    Assert.assertEquals((byte) 0x3B, firstByte);
  }

  @Test
  public void testGetMessageType() throws Exception {
    byte firstByte = (byte) 0xEA;
    MessageType messageType = ProtocolUtils.getMessageType(firstByte);
    Assert.assertEquals(MessageType.DISCONNECT, messageType);
  }

  @Test(expected = CorruptedMqttPacketException.class)
  public void testGetMessageTypeException() throws Exception {
    byte firstByte = (byte) 0xFA;
    ProtocolUtils.getMessageType(firstByte);
  }

  @Test
  public void testGetFlags() throws Exception {
    byte firstByte = (byte) 0xEA;
    byte flags = ProtocolUtils.getFlags(firstByte);
    Assert.assertEquals((byte) 0x0A, flags);
  }

  @Test
  public void testValidateTopicFilter() throws Exception {
    Assert.assertFalse(ProtocolUtils.validateTopicFilter(null));
    Assert.assertFalse(ProtocolUtils.validateTopicFilter(""));
    Assert.assertFalse(ProtocolUtils.validateTopicFilter("/a/b"));
    Assert.assertFalse(ProtocolUtils.validateTopicFilter("a/b/"));
    Assert.assertTrue(ProtocolUtils.validateTopicFilter("a"));
    Assert.assertTrue(ProtocolUtils.validateTopicFilter("a/b/c"));
    Assert.assertTrue(ProtocolUtils.validateTopicFilter("a/+/c"));
    Assert.assertTrue(ProtocolUtils.validateTopicFilter("a/b/#"));
    Assert.assertFalse(ProtocolUtils.validateTopicFilter("a/#/c"));
  }

  @Test
  public void testValidateTopicName() throws Exception {
    Assert.assertFalse(ProtocolUtils.validateTopicName(null));
    Assert.assertFalse(ProtocolUtils.validateTopicName(""));
    Assert.assertFalse(ProtocolUtils.validateTopicName("/a/b"));
    Assert.assertFalse(ProtocolUtils.validateTopicName("a/b/"));
    Assert.assertTrue(ProtocolUtils.validateTopicName("a"));
    Assert.assertTrue(ProtocolUtils.validateTopicName("a/b/c"));
    Assert.assertFalse(ProtocolUtils.validateTopicName("a/+/c"));
    Assert.assertFalse(ProtocolUtils.validateTopicName("a/b/#"));
    Assert.assertFalse(ProtocolUtils.validateTopicName("a/#/c"));
  }
}