package io.mqute.msg.serialization.impl;


import io.mqute.msg.MqttMessage;
import io.mqute.msg.error.CorruptedMqttPacketException;
import io.mqute.msg.serialization.util.ProtocolUtils;
import org.junit.Assert;
import org.junit.Test;

import java.nio.ByteBuffer;

public abstract class MqttSerializerTest<Message extends MqttMessage> {

  int bufferSize() {
    return 1 + ProtocolUtils.numOfBytesToEncode(remainingLength()) + remainingLength();
  }

  abstract AbstractMqttSerializer<Message> serializer();

  abstract int remainingLength();

  abstract Message message();

  abstract ByteBuffer rawData();

  abstract ByteBuffer rawDataWithBadMessageType();

  abstract ByteBuffer rawDataWithBadFlag();

  @Test
  public void testSerialize() throws Exception {
    Assert.assertEquals(rawData(), serializer().serialize(message()));
  }

  @Test
  public void testDeserialize() throws Exception {
    Assert.assertEquals(message(), serializer().deserialize(rawData()));
  }

  @Test(expected = CorruptedMqttPacketException.class)
  public void testDeserializeCorruptedMessageType() throws Exception {
    serializer().deserialize(rawDataWithBadMessageType());
  }

  @Test(expected = CorruptedMqttPacketException.class)
  public void testDeserializeCorruptedFlags() throws Exception {
    serializer().deserialize(rawDataWithBadFlag());
  }

  @Test(expected = CorruptedMqttPacketException.class)
  public void testDeserializeCorruptedBuffer() throws Exception {
    ByteBuffer corruptedBuffer = rawData();
    corruptedBuffer.limit(bufferSize() - 1);

    serializer().deserialize(corruptedBuffer);
  }
}
