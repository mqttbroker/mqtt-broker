package io.mqute.msg.serialization.impl;

import io.mqute.msg.SubscribeMessage;
import io.mqute.msg.enums.MessageType;
import io.mqute.msg.enums.QoSType;
import io.mqute.msg.serialization.util.ProtocolUtils;
import org.junit.BeforeClass;

import java.nio.ByteBuffer;
import java.util.LinkedHashMap;
import java.util.Map;


public class SubscribeSerializerTest extends MqttSerializerTest<SubscribeMessage> {

  private static LinkedHashMap<String, QoSType> subscriptions;
  private SubscribeSerializer serializer = new SubscribeSerializer();
  private short packetId = (short) 123;

  @BeforeClass
  public static void beforeAll() throws Exception {
    subscriptions = new LinkedHashMap<>();
    subscriptions.put("a/b", QoSType.MOST_ONCE);
    subscriptions.put("a/b/c", QoSType.LEAST_ONCE);
    subscriptions.put("a/#", QoSType.EXACT_ONCE);
  }

  @Override
  AbstractMqttSerializer<SubscribeMessage> serializer() {
    return serializer;
  }

  @Override
  int remainingLength() {
    int result = 2;
    for (Map.Entry<String, QoSType> entry : subscriptions.entrySet()) {
      String topic = entry.getKey();
      result += 2 + topic.getBytes(ProtocolUtils.UTF_8).length + 1;
    }
    return result;
  }

  @Override
  SubscribeMessage message() {
    return new SubscribeMessage(packetId, subscriptions);
  }

  @Override
  ByteBuffer rawData() {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.SUBSCRIBE, (byte) 0x02));
    ProtocolUtils.encodeRemainingLength(remainingLength(), byteBuffer);
    byteBuffer.putShort(packetId);
    subscriptions.entrySet().stream().forEach(entry -> {
      ProtocolUtils.encodeString(entry.getKey(), byteBuffer);
      byteBuffer.put(entry.getValue().getByteValue());
    });
    byteBuffer.reset();
    return byteBuffer;
  }

  @Override
  ByteBuffer rawDataWithBadMessageType() {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.SUBACK, (byte) 0x02));
    ProtocolUtils.encodeRemainingLength(remainingLength(), byteBuffer);
    byteBuffer.putShort(packetId);
    subscriptions.entrySet().stream().forEach(entry -> {
      ProtocolUtils.encodeString(entry.getKey(), byteBuffer);
      byteBuffer.put(entry.getValue().getByteValue());
    });
    byteBuffer.reset();
    return byteBuffer;
  }

  @Override
  ByteBuffer rawDataWithBadFlag() {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.SUBSCRIBE, (byte) 0x01));
    ProtocolUtils.encodeRemainingLength(remainingLength(), byteBuffer);
    byteBuffer.putShort(packetId);
    subscriptions.entrySet().stream().forEach(entry -> {
      ProtocolUtils.encodeString(entry.getKey(), byteBuffer);
      byteBuffer.put(entry.getValue().getByteValue());
    });
    byteBuffer.reset();
    return byteBuffer;
  }
}