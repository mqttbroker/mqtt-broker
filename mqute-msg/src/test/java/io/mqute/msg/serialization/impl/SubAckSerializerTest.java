package io.mqute.msg.serialization.impl;

import io.mqute.msg.SubAckMessage;
import io.mqute.msg.enums.MessageType;
import io.mqute.msg.enums.QoSType;
import io.mqute.msg.serialization.util.ProtocolUtils;

import java.nio.ByteBuffer;


public class SubAckSerializerTest extends MqttSerializerTest<SubAckMessage> {

  private SubAckSerializer serializer = new SubAckSerializer();
  private short packetId = (short) 123;
  private QoSType[] returnCodes = {QoSType.MOST_ONCE, QoSType.LEAST_ONCE, QoSType.EXACT_ONCE, QoSType.FAILURE};

  @Override
  AbstractMqttSerializer<SubAckMessage> serializer() {
    return serializer;
  }

  @Override
  int remainingLength() {
    return 2 + returnCodes.length;
  }

  @Override
  SubAckMessage message() {
    return new SubAckMessage(packetId, returnCodes);
  }

  @Override
  ByteBuffer rawData() {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.SUBACK, (byte) 0x00));
    ProtocolUtils.encodeRemainingLength(remainingLength(), byteBuffer);
    byteBuffer.putShort(packetId);
    for (QoSType code : returnCodes) {
      byteBuffer.put(code.getByteValue());
    }
    byteBuffer.reset();
    return byteBuffer;
  }

  @Override
  ByteBuffer rawDataWithBadMessageType() {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.SUBSCRIBE, (byte) 0x00));
    ProtocolUtils.encodeRemainingLength(remainingLength(), byteBuffer);
    byteBuffer.putShort(packetId);
    for (QoSType code : returnCodes) {
      byteBuffer.put(code.getByteValue());
    }
    byteBuffer.reset();
    return byteBuffer;
  }

  @Override
  ByteBuffer rawDataWithBadFlag() {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.SUBACK, (byte) 0x01));
    ProtocolUtils.encodeRemainingLength(remainingLength(), byteBuffer);
    byteBuffer.putShort(packetId);
    for (QoSType code : returnCodes) {
      byteBuffer.put(code.getByteValue());
    }
    byteBuffer.reset();
    return byteBuffer;
  }
}