package io.mqute.msg.serialization.impl;

import io.mqute.msg.ConnectMessage;
import io.mqute.msg.Credential;
import io.mqute.msg.PublishMessage;
import io.mqute.msg.enums.MessageType;
import io.mqute.msg.enums.QoSType;
import io.mqute.msg.serialization.util.ProtocolUtils;

import java.nio.ByteBuffer;


public class ConnectSerializerTest extends MqttSerializerTest<ConnectMessage> {

  private ConnectSerializer serializer = new ConnectSerializer();
  private String protocolName = "MQTT";
  private byte protocolLevel = (byte) 0x04;
  private short keepAlive = (byte) 1000;
  private String clientId = "1234567";
  private Credential credential = new Credential("username", "password".getBytes());
  private PublishMessage willMessage = getWillMessage();

  private PublishMessage getWillMessage() {
    return new PublishMessage(false, QoSType.LEAST_ONCE, false, (short) 0, "a/b/c", "hello world".getBytes(ProtocolUtils.UTF_8));
  }

  @Override
  AbstractMqttSerializer<ConnectMessage> serializer() {
    return serializer;
  }

  @Override
  int remainingLength() {
    return 2 + protocolName.length() + 4 + 2 + clientId.length() + 2
        + "a/b/c".length() + 2 + "hello world".length()
        + 2 + credential.getUsername().length() + 2 + credential.getPassword().length;
  }

  @Override
  ConnectMessage message() {
    return new ConnectMessage(protocolName, protocolLevel, keepAlive, true, clientId, credential, willMessage);
  }

  @Override
  ByteBuffer rawData() {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.CONNECT, (byte) 0x00));
    ProtocolUtils.encodeRemainingLength(remainingLength(), byteBuffer);
    ProtocolUtils.encodeString(protocolName, byteBuffer);
    byteBuffer.put(protocolLevel);
    byteBuffer.put((byte) 0xCE);
    byteBuffer.putShort(keepAlive);
    ProtocolUtils.encodeString(clientId, byteBuffer);
    ProtocolUtils.encodeString("a/b/c", byteBuffer);
    ProtocolUtils.encodeString("hello world", byteBuffer);
    ProtocolUtils.encodeString("username", byteBuffer);
    ProtocolUtils.encodeString("password", byteBuffer);
    byteBuffer.reset();
    return byteBuffer;
  }

  @Override
  ByteBuffer rawDataWithBadMessageType() {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.CONNACK, (byte) 0x00));
    ProtocolUtils.encodeRemainingLength(remainingLength(), byteBuffer);
    ProtocolUtils.encodeString(protocolName, byteBuffer);
    byteBuffer.put(protocolLevel);
    byteBuffer.put((byte) 0xCE);
    byteBuffer.putShort(keepAlive);
    ProtocolUtils.encodeString(clientId, byteBuffer);
    ProtocolUtils.encodeString("a/b/c", byteBuffer);
    ProtocolUtils.encodeString("hello world", byteBuffer);
    ProtocolUtils.encodeString("username", byteBuffer);
    ProtocolUtils.encodeString("password", byteBuffer);
    byteBuffer.reset();
    return byteBuffer;
  }

  @Override
  ByteBuffer rawDataWithBadFlag() {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.CONNECT, (byte) 0x01));
    ProtocolUtils.encodeRemainingLength(remainingLength(), byteBuffer);
    ProtocolUtils.encodeString(protocolName, byteBuffer);
    byteBuffer.put(protocolLevel);
    byteBuffer.put((byte) 0xCE);
    byteBuffer.putShort(keepAlive);
    ProtocolUtils.encodeString(clientId, byteBuffer);
    ProtocolUtils.encodeString("a/b/c", byteBuffer);
    ProtocolUtils.encodeString("hello world", byteBuffer);
    ProtocolUtils.encodeString("username", byteBuffer);
    ProtocolUtils.encodeString("password", byteBuffer);
    byteBuffer.reset();
    return byteBuffer;
  }
}