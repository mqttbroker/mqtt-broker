package io.mqute.msg.serialization.impl;

import io.mqute.msg.UnsubscribeMessage;
import io.mqute.msg.enums.MessageType;
import io.mqute.msg.serialization.util.ProtocolUtils;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;


public class UnsubscribeSerializerTest extends MqttSerializerTest<UnsubscribeMessage> {

  private UnsubscribeSerializer serializer = new UnsubscribeSerializer();
  private short packetId = (short) 123;
  private List<String> topics = Arrays.asList("a/b", "a/c", "a/b/c");

  @Override
  AbstractMqttSerializer<UnsubscribeMessage> serializer() {
    return serializer;
  }

  @Override
  int remainingLength() {
    int result = 2;
    for (String topic : topics) {
      result += 2 + topic.length();
    }
    return result;
  }

  @Override
  UnsubscribeMessage message() {
    return new UnsubscribeMessage(packetId, topics);
  }

  @Override
  ByteBuffer rawData() {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.UNSUBSCRIBE, (byte) 0x02));
    ProtocolUtils.encodeRemainingLength(remainingLength(), byteBuffer);
    byteBuffer.putShort(packetId);
    for (String topic : topics) {
      ProtocolUtils.encodeString(topic, byteBuffer);
    }
    byteBuffer.reset();
    return byteBuffer;
  }

  @Override
  ByteBuffer rawDataWithBadMessageType() {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.UNSUBACK, (byte) 0x02));
    ProtocolUtils.encodeRemainingLength(remainingLength(), byteBuffer);
    byteBuffer.putShort(packetId);
    for (String topic : topics) {
      ProtocolUtils.encodeString(topic, byteBuffer);
    }
    byteBuffer.reset();
    return byteBuffer;
  }

  @Override
  ByteBuffer rawDataWithBadFlag() {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.UNSUBSCRIBE, (byte) 0x01));
    ProtocolUtils.encodeRemainingLength(remainingLength(), byteBuffer);
    byteBuffer.putShort(packetId);
    for (String topic : topics) {
      ProtocolUtils.encodeString(topic, byteBuffer);
    }
    byteBuffer.reset();
    return byteBuffer;
  }

}