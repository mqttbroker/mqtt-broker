package io.mqute.msg.serialization.impl;

import io.mqute.msg.DisconnectMessage;
import io.mqute.msg.enums.MessageType;
import io.mqute.msg.error.CorruptedMqttPacketException;
import io.mqute.msg.serialization.util.ProtocolUtils;
import org.junit.Test;

import java.nio.ByteBuffer;


public class DisconnectSerializerTest extends MqttSerializerTest<DisconnectMessage> {

  private DisconnectSerializer serializer = new DisconnectSerializer();

  @Override
  AbstractMqttSerializer<DisconnectMessage> serializer() {
    return serializer;
  }

  @Override
  int remainingLength() {
    return 0;
  }

  @Override
  DisconnectMessage message() {
    return new DisconnectMessage();
  }

  @Override
  ByteBuffer rawData() {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.DISCONNECT, (byte) 0x00));
    ProtocolUtils.encodeRemainingLength(remainingLength(), byteBuffer);
    byteBuffer.reset();
    return byteBuffer;
  }

  @Override
  ByteBuffer rawDataWithBadMessageType() {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.CONNECT, (byte) 0x00));
    ProtocolUtils.encodeRemainingLength(remainingLength(), byteBuffer);
    byteBuffer.reset();
    return byteBuffer;
  }

  @Override
  ByteBuffer rawDataWithBadFlag() {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.DISCONNECT, (byte) 0x01));
    byteBuffer.put((byte) 0x00);
    byteBuffer.reset();
    return byteBuffer;
  }

  @Test(expected = CorruptedMqttPacketException.class)
  public void testDeserializeCorruptedRemainingLength() throws Exception {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.DISCONNECT, (byte) 0x00));
    byteBuffer.put((byte) 0x01);
    byteBuffer.reset();

    serializer.deserialize(byteBuffer);
  }

}