package io.mqute.msg.serialization.impl;

import io.mqute.msg.PubRelMessage;
import io.mqute.msg.enums.MessageType;
import io.mqute.msg.error.CorruptedMqttPacketException;
import io.mqute.msg.serialization.util.ProtocolUtils;
import org.junit.Test;

import java.nio.ByteBuffer;


public class PubRelSerializerTest extends MqttSerializerTest<PubRelMessage> {

  private PubRelSerializer serializer = new PubRelSerializer();
  private short packetId = (short) 123;

  @Override
  AbstractMqttSerializer<PubRelMessage> serializer() {
    return serializer;
  }

  @Override
  int remainingLength() {
    return 2;
  }

  @Override
  PubRelMessage message() {
    return new PubRelMessage(packetId);
  }

  @Override
  ByteBuffer rawData() {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.PUBREL, (byte) 0x02));
    ProtocolUtils.encodeRemainingLength(remainingLength(), byteBuffer);
    byteBuffer.putShort(packetId);
    byteBuffer.reset();
    return byteBuffer;
  }

  @Override
  ByteBuffer rawDataWithBadMessageType() {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.PUBLISH, (byte) 0x02));
    ProtocolUtils.encodeRemainingLength(remainingLength(), byteBuffer);
    byteBuffer.putShort(packetId);
    byteBuffer.reset();
    return byteBuffer;
  }

  @Override
  ByteBuffer rawDataWithBadFlag() {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.PUBREL, (byte) 0x01));
    ProtocolUtils.encodeRemainingLength(remainingLength(), byteBuffer);
    byteBuffer.putShort(packetId);
    byteBuffer.reset();
    return byteBuffer;
  }

  @Test(expected = CorruptedMqttPacketException.class)
  public void testDeserializeCorruptedRemainingLength() throws Exception {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.PUBREL, (byte) 0x00));
    byteBuffer.put((byte) 0x01);
    byteBuffer.putShort(packetId);
    byteBuffer.reset();

    serializer.deserialize(byteBuffer);
  }
}