package io.mqute.msg.serialization.impl;


import io.mqute.msg.ConnAckMessage;
import io.mqute.msg.enums.ConnectReturnCode;
import io.mqute.msg.enums.MessageType;
import io.mqute.msg.serialization.util.ProtocolUtils;

import java.nio.ByteBuffer;

public class ConnAckSerializerTest extends MqttSerializerTest<ConnAckMessage> {

  private ConnAckSerializer serializer = new ConnAckSerializer();

  @Override
  AbstractMqttSerializer<ConnAckMessage> serializer() {
    return serializer;
  }

  @Override
  int remainingLength() {
    return 2;
  }

  @Override
  ConnAckMessage message() {
    return new ConnAckMessage(ConnectReturnCode.ACCEPTED, true);
  }

  @Override
  ByteBuffer rawData() {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.CONNACK, (byte) 0x00));
    ProtocolUtils.encodeRemainingLength(remainingLength(), byteBuffer);
    byteBuffer.put((byte) 0x01);
    byteBuffer.put(ConnectReturnCode.ACCEPTED.getByteValue());
    byteBuffer.reset();
    return byteBuffer;
  }

  @Override
  ByteBuffer rawDataWithBadMessageType() {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.CONNECT, (byte) 0x00));
    ProtocolUtils.encodeRemainingLength(remainingLength(), byteBuffer);
    byteBuffer.put(ConnectReturnCode.ACCEPTED.getByteValue());
    byteBuffer.put((byte) 0x01);
    byteBuffer.reset();
    return byteBuffer;
  }

  @Override
  ByteBuffer rawDataWithBadFlag() {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.CONNACK, (byte) 0x01));
    ProtocolUtils.encodeRemainingLength(remainingLength(), byteBuffer);
    byteBuffer.put(ConnectReturnCode.ACCEPTED.getByteValue());
    byteBuffer.put((byte) 0x01);
    byteBuffer.reset();
    return byteBuffer;
  }
}