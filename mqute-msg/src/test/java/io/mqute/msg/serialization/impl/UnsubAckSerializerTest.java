package io.mqute.msg.serialization.impl;

import io.mqute.msg.UnsubAckMessage;
import io.mqute.msg.enums.MessageType;
import io.mqute.msg.error.CorruptedMqttPacketException;
import io.mqute.msg.serialization.util.ProtocolUtils;
import org.junit.Test;

import java.nio.ByteBuffer;


public class UnsubAckSerializerTest extends MqttSerializerTest<UnsubAckMessage> {

  private UnsubAckSerializer serializer = new UnsubAckSerializer();
  private short packetId = (short) 123;

  @Override
  AbstractMqttSerializer<UnsubAckMessage> serializer() {
    return serializer;
  }

  @Override
  int remainingLength() {
    return 2;
  }

  @Override
  UnsubAckMessage message() {
    return new UnsubAckMessage(packetId);
  }

  @Override
  ByteBuffer rawData() {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.UNSUBACK, (byte) 0x00));
    ProtocolUtils.encodeRemainingLength(remainingLength(), byteBuffer);
    byteBuffer.putShort(packetId);
    byteBuffer.reset();
    return byteBuffer;
  }

  @Override
  ByteBuffer rawDataWithBadMessageType() {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.UNSUBSCRIBE, (byte) 0x00));
    ProtocolUtils.encodeRemainingLength(remainingLength(), byteBuffer);
    byteBuffer.putShort(packetId);
    byteBuffer.reset();
    return byteBuffer;
  }

  @Override
  ByteBuffer rawDataWithBadFlag() {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.UNSUBACK, (byte) 0x01));
    ProtocolUtils.encodeRemainingLength(remainingLength(), byteBuffer);
    byteBuffer.putShort(packetId);
    byteBuffer.reset();
    return byteBuffer;
  }

  @Test(expected = CorruptedMqttPacketException.class)
  public void testDeserializeCorruptedRemainingLength() throws Exception {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize());
    byteBuffer.mark();
    byteBuffer.put(ProtocolUtils.constructFirstByte(MessageType.UNSUBACK, (byte) 0x00));
    byteBuffer.put((byte) 0x01);
    byteBuffer.putShort(packetId);
    byteBuffer.reset();

    serializer.deserialize(byteBuffer);
  }
}