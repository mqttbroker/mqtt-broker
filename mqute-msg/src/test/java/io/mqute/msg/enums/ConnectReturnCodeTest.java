package io.mqute.msg.enums;

import org.junit.Assert;
import org.junit.Test;


public class ConnectReturnCodeTest {

  @Test
  public void testGetByteValue() throws Exception {
    Assert.assertEquals((byte) 0x00, ConnectReturnCode.ACCEPTED.getByteValue());
    Assert.assertEquals((byte) 0x01, ConnectReturnCode.BAD_PROTOCOL_VERSION.getByteValue());
    Assert.assertEquals((byte) 0x02, ConnectReturnCode.ID_REJECTED.getByteValue());
    Assert.assertEquals((byte) 0x03, ConnectReturnCode.SERVER_UNAVAILABLE.getByteValue());
    Assert.assertEquals((byte) 0x04, ConnectReturnCode.BAD_USERNAME_OR_PASSWORD.getByteValue());
    Assert.assertEquals((byte) 0x05, ConnectReturnCode.NOT_AUTHORIZED.getByteValue());
  }

  @Test
  public void testFromByte() throws Exception {
    Assert.assertEquals(ConnectReturnCode.ACCEPTED, ConnectReturnCode.fromByte((byte) 0x00));
    Assert.assertEquals(ConnectReturnCode.BAD_PROTOCOL_VERSION, ConnectReturnCode.fromByte((byte) 0x01));
    Assert.assertEquals(ConnectReturnCode.ID_REJECTED, ConnectReturnCode.fromByte((byte) 0x02));
    Assert.assertEquals(ConnectReturnCode.SERVER_UNAVAILABLE, ConnectReturnCode.fromByte((byte) 0x03));
    Assert.assertEquals(ConnectReturnCode.BAD_USERNAME_OR_PASSWORD, ConnectReturnCode.fromByte((byte) 0x04));
    Assert.assertEquals(ConnectReturnCode.NOT_AUTHORIZED, ConnectReturnCode.fromByte((byte) 0x05));
  }

  @Test(expected = IllegalArgumentException.class)
  public void testFromByteException() throws Exception {
    ConnectReturnCode.fromByte((byte) 0x06);
  }
}