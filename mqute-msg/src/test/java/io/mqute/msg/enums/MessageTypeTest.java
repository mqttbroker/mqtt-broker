package io.mqute.msg.enums;

import org.junit.Assert;
import org.junit.Test;


public class MessageTypeTest {

  @Test
  public void testGetByteValue() throws Exception {
    Assert.assertEquals((byte) 0x01, MessageType.CONNECT.getByteValue());
    Assert.assertEquals((byte) 0x02, MessageType.CONNACK.getByteValue());
    Assert.assertEquals((byte) 0x03, MessageType.PUBLISH.getByteValue());
    Assert.assertEquals((byte) 0x04, MessageType.PUBACK.getByteValue());
    Assert.assertEquals((byte) 0x05, MessageType.PUBREC.getByteValue());
    Assert.assertEquals((byte) 0x06, MessageType.PUBREL.getByteValue());
    Assert.assertEquals((byte) 0x07, MessageType.PUBCOMP.getByteValue());
    Assert.assertEquals((byte) 0x08, MessageType.SUBSCRIBE.getByteValue());
    Assert.assertEquals((byte) 0x09, MessageType.SUBACK.getByteValue());
    Assert.assertEquals((byte) 0x0A, MessageType.UNSUBSCRIBE.getByteValue());
    Assert.assertEquals((byte) 0x0B, MessageType.UNSUBACK.getByteValue());
    Assert.assertEquals((byte) 0x0C, MessageType.PINGREQ.getByteValue());
    Assert.assertEquals((byte) 0x0D, MessageType.PINGRESP.getByteValue());
    Assert.assertEquals((byte) 0x0E, MessageType.DISCONNECT.getByteValue());
  }

  @Test
  public void testFromByte() throws Exception {
    Assert.assertEquals(MessageType.CONNECT, MessageType.fromByte((byte) 0x01));
    Assert.assertEquals(MessageType.CONNACK, MessageType.fromByte((byte) 0x02));
    Assert.assertEquals(MessageType.PUBLISH, MessageType.fromByte((byte) 0x03));
    Assert.assertEquals(MessageType.PUBACK, MessageType.fromByte((byte) 0x04));
    Assert.assertEquals(MessageType.PUBREC, MessageType.fromByte((byte) 0x05));
    Assert.assertEquals(MessageType.PUBREL, MessageType.fromByte((byte) 0x06));
    Assert.assertEquals(MessageType.PUBCOMP, MessageType.fromByte((byte) 0x07));
    Assert.assertEquals(MessageType.SUBSCRIBE, MessageType.fromByte((byte) 0x08));
    Assert.assertEquals(MessageType.SUBACK, MessageType.fromByte((byte) 0x09));
    Assert.assertEquals(MessageType.UNSUBSCRIBE, MessageType.fromByte((byte) 0x0A));
    Assert.assertEquals(MessageType.UNSUBACK, MessageType.fromByte((byte) 0x0B));
    Assert.assertEquals(MessageType.PINGREQ, MessageType.fromByte((byte) 0x0C));
    Assert.assertEquals(MessageType.PINGRESP, MessageType.fromByte((byte) 0x0D));
    Assert.assertEquals(MessageType.DISCONNECT, MessageType.fromByte((byte) 0x0E));
  }

  @Test(expected = IllegalArgumentException.class)
  public void testFromByteException() throws Exception {
    MessageType.fromByte((byte) 0x1A);
  }
}