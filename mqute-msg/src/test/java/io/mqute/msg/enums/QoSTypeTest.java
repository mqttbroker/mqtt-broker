package io.mqute.msg.enums;

import org.junit.Assert;
import org.junit.Test;


public class QoSTypeTest {

  @Test
  public void testGetByteValue() throws Exception {
    Assert.assertEquals((byte) 0x00, QoSType.MOST_ONCE.getByteValue());
    Assert.assertEquals((byte) 0x01, QoSType.LEAST_ONCE.getByteValue());
    Assert.assertEquals((byte) 0x02, QoSType.EXACT_ONCE.getByteValue());
    Assert.assertEquals((byte) 0x80, QoSType.FAILURE.getByteValue());
  }

  @Test
  public void testFromByte() throws Exception {
    Assert.assertEquals(QoSType.MOST_ONCE, QoSType.fromByte((byte) 0x00));
    Assert.assertEquals(QoSType.LEAST_ONCE, QoSType.fromByte((byte) 0x01));
    Assert.assertEquals(QoSType.EXACT_ONCE, QoSType.fromByte((byte) 0x02));
  }

  @Test(expected = IllegalArgumentException.class)
  public void testFromByteException() throws Exception {
    QoSType.fromByte((byte) 0x80);
  }
}