package io.mqute.edge;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import io.mqute.msg.PubRelMessage;
import io.mqute.msg.PublishMessage;
import io.mqute.msg.enums.QoSType;

/**
 * Created by huz on 7/22/16.
 *
 * The EdgeClient represents an Edge/Connection getEndpoint where the MQTT TCP connection is opened.
 * The EdgeClient uses Aeron library to do reliable UDP unicast to deliver the messages (rather than
 * RPC style), it has backpressure built in. The EdgeClient is a state machine, it starts IDLE, #pub
 * transits IDLE to QUEUED, or QUEUED to SLOW (if too many pendings), #ack transits QUEUED to IDLE,
 * or SLOW to QUEUED (if persistent storage empties)
 *
 * The reason to introduce SLOW state is to unblock the enclosing SUBSCRIPTION partition from
 * irresponsive or keepAlives subscribing to too many things (wildcard), whatever the underneath
 * reason, SLOW keepAlives enqueue the PUBLISH messages by immediately persist the message into its
 * persistent queue (REDIS as of now), as we batch writes (persist) the queues, it's much faster
 * than waiting for an ACK to arrive.
 */
public class EdgeClient implements Client {

  private static final Logger LOGGER = LoggerFactory.getLogger(EdgeClient.class);

  private final String clientID;
  private final boolean persistentSession;

  private volatile String endpoint;
  private volatile State state;
  private volatile int nextPacketID;

  //this is my queue of acks;
  //TODO timeout should be enforced here
  private transient Map<Integer, Pair<CompletableFuture<Void>, QoSType>> waitForAcks;

  @JsonCreator
  public EdgeClient(final @JsonProperty("clientID") String clientID,
                    final @JsonProperty("persistentSession") boolean persistentSession,
                    final @JsonProperty("getEndpoint") String endpoint,
                    final @JsonProperty("state") State state,
                    final @JsonProperty("nextPacketID") int nextPacketID) {

    this.clientID = clientID;
    this.persistentSession = persistentSession;
    this.endpoint = endpoint;
    this.state = state;
    this.nextPacketID = nextPacketID;

    this.waitForAcks = Maps.newConcurrentMap();
  }

  @Override
  public String clientID() {
    return clientID;
  }

  @Override
  public boolean persistentSession() {
    return persistentSession;
  }

  @Override
  public String endpoint() {
    return endpoint;
  }

  @Override
  public State state() {
    return state;
  }

  @Override
  public State transit(State destination) {
    try {
      return state;
    } finally {
      this.state = destination;
    }
  }

  @Override
  public int nextPacketID() {
    return nextPacketID;
  }

  @Override
  public Pair<PublishMessage, CompletableFuture<Void>> pub(final PublishMessage publishMessage, final QoSType qosType) {
    final int thisMessageID = nextPacketID();
    nextPacketID += 1;

    final PublishMessage brokerPublish = nextPublish(publishMessage, qosType, thisMessageID);
    LOGGER.info("[edgeclient:{}] pub:{} with qos:{}", clientID, thisMessageID, brokerPublish.getQos());

    if (QoSType.MOST_ONCE.equals(brokerPublish.getQos())) {
      //BUGFIX, at most once, should not wait for ack
      return ImmutablePair.of(brokerPublish, CompletableFuture.completedFuture(null));
    }

    final CompletableFuture<Void> onAck = new CompletableFuture<>();
    waitForAcks.put(thisMessageID, ImmutablePair.of(onAck, brokerPublish.getQos()));

    transit(State.QUEUED);

    if (waitForAcks.size() > 32) {
      transit(State.SLOW);
    }

    return ImmutablePair.of(brokerPublish, onAck);
  }

  @Override
  public void takeOver(final String endpoint) {

    this.endpoint = endpoint;

    //TODO take over should trigger persistent queue loading behavior as if we got some ACK
  }

  @Override
  public CompletableFuture<Void> ack(final int packetID) {

    LOGGER.info("[edgeclient:{}] ack: {}", clientID, packetID);

    final Pair<CompletableFuture<Void>, QoSType> waitForWithQos = waitForAcks.remove(packetID);
    if (waitForWithQos == null) {
      throw new IllegalStateException("cannot find pending future of messageID:" + packetID);
    }

    final CompletableFuture<Void> waitFor = waitForWithQos.getLeft();

    LOGGER.info("[edgeclient:{}] ack: {} completed future", clientID, packetID);

    waitFor.complete(null);//finally acknowledged!

    if (this.state == State.QUEUED && waitForAcks.isEmpty()) {
      transit(State.IDLE);
    }

    return waitFor;
  }

  @Override
  public Pair<PubRelMessage, CompletableFuture<Void>> rec(final int packetID) {

    final Pair<CompletableFuture<Void>, QoSType> waitForWithQos = waitForAcks.get(packetID);
    if (waitForWithQos == null) {
      throw new IllegalStateException("cannot find pending future of messageID:" + packetID);
    }

    final CompletableFuture<Void> waitFor = waitForWithQos.getLeft();
    final PubRelMessage pubRelMessage = new PubRelMessage((short) packetID);

    return ImmutablePair.of(pubRelMessage, waitFor);
  }

  @Override
  public CompletableFuture<Void> comp(final int packetID) {

    final Pair<CompletableFuture<Void>, QoSType> waitForWithQos = waitForAcks.remove(packetID);
    if (waitForWithQos == null) {
      throw new IllegalStateException("cannot find pending future of messageID:" + packetID);
    }

    final CompletableFuture<Void> waitFor = waitForWithQos.getLeft();
    waitFor.complete(null);//finally acknowledged!

    if (this.state == State.QUEUED && waitForAcks.isEmpty()) {
      transit(State.IDLE);
    }

    return waitFor;
  }

  @Override
  public List<CompletableFuture<Void>> pendings() {

    return Lists.newArrayList(Iterables.transform(waitForAcks.values(), Pair::getLeft));
  }

  @Override
  public void close() throws IOException {

    pendings().forEach(future -> future.cancel(true));
  }

  @Override
  public String toString() {
    return String.format("[edgeclient]\n\tendpoint=%s\n\tclientID=%s\n\tpersistentSession=%s\n\tstate=%s\n\tnextPacketID=%s", endpoint, clientID, persistentSession, state, nextPacketID);
  }

  protected static PublishMessage nextPublish(final PublishMessage publishMessage,
                                              final QoSType qosType,
                                              final int nextPacketID) {

    return new PublishMessage(publishMessage.isDup(), qosType, publishMessage.isRetain(), (short) nextPacketID, publishMessage.getTopicName(), publishMessage.getPayload());
  }
}
