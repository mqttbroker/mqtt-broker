package io.mqute.edge;

import com.google.protobuf.ByteString;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

import io.mqute.msg.OutgoingMessageWrapperOuterClass;
import io.mqute.msg.internal.OutgoingMessageWrapper;
import io.mqute.msg.serialization.MqttSerialization;


public class CallbackUdpClient {

  private final String host;
  private final int port;
  private final DatagramSocket socket;

  public CallbackUdpClient(String host, int port) throws SocketException {
    this.host = host;
    this.port = port;
    socket = new DatagramSocket();
  }

  public void send(final OutgoingMessageWrapper message) throws IOException {

    String clientId = message.getClientId();
    int partition = message.getPartition();
    ByteBuffer byteBuffer = MqttSerialization.getInstance().serialize(message.getOriginalMessage());
    byte[] bytes = OutgoingMessageWrapperOuterClass.OutgoingMessageWrapper.newBuilder()
        .setOriginalMessage(ByteString.copyFrom(byteBuffer))
        .setClientID(clientId)
        .setPartition(partition)
        .build()
        .toByteArray();
    InetAddress address = InetAddress.getByName(host);
    DatagramPacket packet = new DatagramPacket(bytes, bytes.length, address, port);
    socket.send(packet);
  }
}
