package io.mqute.edge;

/**
 * Created by huz on 7/21/16.
 */
public enum State {
    IDLE,
    QUEUED,
    SLOW,
    DISCONNECTED
}
