package io.mqute.edge;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.mqute.msg.PubRelMessage;
import io.mqute.msg.PublishMessage;
import io.mqute.msg.enums.QoSType;

import org.apache.commons.lang3.tuple.Pair;

import java.io.Closeable;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * Created by huz on 7/21/16.
 * <p>
 * a stateful representation of the netty edge getEndpoint
 */
public interface Client extends Closeable {

  @JsonProperty("clientID")
  String clientID();

  @JsonProperty("persistentSession")
  boolean persistentSession();

  @JsonProperty("getEndpoint")
  String endpoint();

  @JsonProperty("state")
  State state();

  @JsonProperty("nextPacketID")
  int nextPacketID();

  State transit(State destination);

  void takeOver(final String endpoint);

  Pair<PublishMessage, CompletableFuture<Void>> pub(final PublishMessage publishMessage, final QoSType qosType);

  CompletableFuture<Void> ack(final int packetID);

  Pair<PubRelMessage, CompletableFuture<Void>> rec(final int packetID);

  CompletableFuture<Void> comp(final int packetID);

  List<CompletableFuture<Void>> pendings();
}
