package io.mqute.process.kafka;

import com.google.common.cache.LoadingCache;
import com.google.common.collect.*;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.clients.consumer.internals.ConsumerCoordinator;
import org.apache.kafka.clients.consumer.internals.Fetcher;
import org.apache.kafka.clients.consumer.internals.SubscriptionState;
import org.apache.kafka.common.Metric;
import org.apache.kafka.common.MetricName;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by huz on 7/26/16.
 */
public class PacedKafkaConsumer<K, V> implements Consumer<K, V>, Closeable {

  private static final Logger LOGGER = LoggerFactory.getLogger(PacedKafkaConsumer.class);

  private final LoadingCache<TopicPartition, ConcurrentNavigableMap<Long, List<ConsumerRecord<K, V>>>> pacedRecords;
  private final Set<TopicPartition> pausedTps;
  private final KafkaConsumer<K, V> delegate;
  private final Field subscriptionsField;
  private final Field coordinatorField;
  private final Field fetcherField;
  private final ExecutorService singleExecutor;

  public PacedKafkaConsumer(final LoadingCache<TopicPartition, ConcurrentNavigableMap<Long, List<ConsumerRecord<K, V>>>> pacedRecords,
                            final ExecutorService executorService,
                            final KafkaConsumer<K, V> delegate) {

    try {
      subscriptionsField = KafkaConsumer.class.getDeclaredField("subscriptions");
      subscriptionsField.setAccessible(true);
      coordinatorField = KafkaConsumer.class.getDeclaredField("coordinator");
      coordinatorField.setAccessible(true);
      fetcherField = KafkaConsumer.class.getDeclaredField("fetcher");
      fetcherField.setAccessible(true);
    } catch (NoSuchFieldException e) {
      LOGGER.error("[pacer] cannot initialize as `subscriptions`|`coordinator` field is missing!");
      throw new IllegalStateException(e);
    }

    this.pausedTps = Sets.newConcurrentHashSet();
    this.pacedRecords = pacedRecords;
    this.delegate = delegate;
    this.singleExecutor = executorService;
  }

  protected SubscriptionState subscriptionStates(final KafkaConsumer instance) {
    try {
      return (SubscriptionState) subscriptionsField.get(instance);
    } catch (Exception e) {
      LOGGER.error("[pacer] cannot get `subscriptions` field is missing!");
      throw new IllegalStateException(e);
    }
  }

  protected void postSubscribeOrAssign() {
    //this was done at time of delegate#pollOnce, and we've moved it upfront to untangle from the modified #poll logic
    try {
      final ConsumerCoordinator coordinator = (ConsumerCoordinator) coordinatorField.get(delegate);
      coordinator.ensurePartitionAssignment();
      coordinator.refreshCommittedOffsetsIfNeeded();
      //make sure that we'll have committed offsets of our initial position for each topic partition.
    } catch (IllegalAccessException e) {
      LOGGER.error("[pacer] cannot initialize because `coordinator` field failed due to: {}", e);
      throw new IllegalStateException(e);
    }
  }

  protected void beforePoll(final SubscriptionState states) {
    try {

      final ConsumerCoordinator coordinator = (ConsumerCoordinator) coordinatorField.get(delegate);
      if (states.partitionsAutoAssigned()) {
        coordinator.ensurePartitionAssignment();
      }
      if (states.refreshCommitsNeeded()) {
        coordinator.refreshCommittedOffsetsIfNeeded();
      }
      if (!states.hasAllFetchPositions()) {
        LOGGER.info("[paced] update fetch positions for:{}", states.missingFetchPositions());
        final Fetcher<K, V> fetcher = (Fetcher<K, V>) fetcherField.get(delegate);
        fetcher.updateFetchPositions(states.missingFetchPositions());
      }
      //make sure that we'll have committed offsets of our initial position for each topic partition.
    } catch (IllegalAccessException e) {
      LOGGER.error("[pacer] cannot initialize because `coordinator` field failed due to: {}", e);
      throw new IllegalStateException(e);
    }
  }

  @Override
  public Set<TopicPartition> assignment() {
    return delegate.assignment();
  }

  @Override
  public Set<String> subscription() {
    return delegate.subscription();
  }

  @Override
  public void subscribe(Collection<String> topics) {
    delegate.subscribe(topics);
    postSubscribeOrAssign();
  }

  @Override
  public void subscribe(Collection<String> topics, ConsumerRebalanceListener callback) {
    delegate.subscribe(topics, callback);
    postSubscribeOrAssign();
  }

  @Override
  public void assign(Collection<TopicPartition> partitions) {
    delegate.assign(partitions);
    postSubscribeOrAssign();
  }

  @Override
  public void subscribe(Pattern pattern, ConsumerRebalanceListener callback) {
    delegate.subscribe(pattern, callback);
    postSubscribeOrAssign();
  }

  @Override
  public void unsubscribe() {
    delegate.unsubscribe();
  }

  @Override
  /**
   * the motivation behind {@link PacedKafkaConsumer} is reflected in {@link #poll(long)} here
   * the use case we're facing is a bit different, in a JVM process, we have multiple consumer groups, each likely to have only one topic to subscribe (but all partitions)
   * those consumers as from different consumer groups, while consuming the exact same topic and partitions, will have to repeatedly process the consumer protocol step by step
   * and of course, the worst part would be massive data transfer from brokers to this JVM process repeating the same set of binaries, and parsed, and then consumed
   *
   * note that, each consumer should faithfully follow the commit handling, as each should have its individual commit logs, it's only the data transfer and parsing that we'd like to reuse.
   *
   * for this particular use case, we introduced {@link #pacedRecords} cache, it doesn't and shouldn't cache many, as long as it can keep each consumer at close paces (therefore the name).
   * pacing is at the granularity of {@link TopicPartition} so that we could have the most probability of sharing results, but as a trade off, each #poll is likely to loop among the partitions.
   */
  public ConsumerRecords<K, V> poll(final long timeout) {

    final Set<TopicPartition> assigned = delegate.assignment();
    final SubscriptionState states = subscriptionStates(delegate);
    if (!states.hasAllFetchPositions()) {
      beforePoll(states);
    }

    final Set<TopicPartition> actives = assigned.stream().filter(tp -> !pausedTps.contains(tp)).collect(Collectors.toSet());

    if (actives.isEmpty()) {
      return ConsumerRecords.empty();
    }

    final Map<TopicPartition, List<ConsumerRecord<K, V>>> aggregates = Maps.newHashMap();
    try {
      aggregates.putAll(singleExecutor.submit(() -> {

        final Map<TopicPartition, List<ConsumerRecord<K, V>>> activeRecords = Maps.newHashMap();

        actives.forEach(active -> {
          final Long beyond = states.position(active);
          try {
            final ConcurrentNavigableMap<Long, List<ConsumerRecord<K, V>>> thresholds = pacedRecords.get(active);
            final NavigableSet<Long> paces = Collections.unmodifiableNavigableSet(thresholds.keySet());
            LOGGER.debug("[paced] states positions:{} committed:{}, and paces:{}", beyond, states.committed(active), paces);
            final List<ConsumerRecord<K, V>> records = Lists.newLinkedList();

            //handle only cache misses, cache hit will go directly to fetch from pacedRecords
            LOGGER.debug("[paced] paces not discovered, need to check range inclusion");
            //need to check the range of all cached records instead and hope some range contains the lookup position
            final Long floor = paces.floor(beyond);
            if (floor != null) {
              final List<ConsumerRecord<K, V>> examine = thresholds.get(floor);
              if(!examine.isEmpty()) {
                final long higher = examine.listIterator(examine.size()).previous().offset();
                if (beyond <= higher) {
                  records.addAll(examine.stream().filter(r -> r.offset() >= beyond).collect(Collectors.toList()));
                }
              }
            }
            activeRecords.put(active, records);
          } catch (ExecutionException e) {
            LOGGER.error("");
          }
        });

        try{
          delegate.pause(activeRecords.keySet());

          final ListMultimap<TopicPartition, ConsumerRecord<K, V>> updateRecords = Multimaps.newListMultimap(Maps.newHashMap(), Lists::newLinkedList);
          delegate.poll(timeout).forEach(record -> {
            final TopicPartition tp = new TopicPartition(record.topic(), record.partition());
            if(!activeRecords.containsKey(tp)){
              activeRecords.put(tp, Lists.newLinkedList());
            }
            activeRecords.get(tp).add(record);
            updateRecords.put(tp, record);
          });

          Multimaps.asMap(updateRecords).forEach((tp, records) -> {
            final ConcurrentNavigableMap<Long, List<ConsumerRecord<K, V>>> update = pacedRecords.getIfPresent(tp);
            update.putIfAbsent(records.listIterator(0).next().offset(), records);
            //avoid unbounded growth of this cache
            while(update.size() > 1024){
              update.remove(update.firstKey());
            }
          });
        }
        finally {
          delegate.resume(activeRecords.keySet());
        }

        return activeRecords;

      }).get());

      aggregates.forEach((tp, records) -> states.position(tp, records.listIterator(records.size()).previous().offset() + 1));
    } catch (Exception e) {

    }

    return new ConsumerRecords<>(aggregates);
  }

  @Override
  public void commitSync() {
    delegate.commitSync();
  }

  @Override
  public void commitSync(Map<TopicPartition, OffsetAndMetadata> offsets) {
    delegate.commitSync(offsets);
  }

  @Override
  public void commitAsync() {
    delegate.commitAsync();
  }

  @Override
  public void commitAsync(OffsetCommitCallback callback) {
    delegate.commitAsync(callback);
  }

  @Override
  public void commitAsync(Map<TopicPartition, OffsetAndMetadata> offsets, OffsetCommitCallback callback) {
    delegate.commitAsync(offsets, callback);
  }

  @Override
  public void seek(TopicPartition partition, long offset) {
    delegate.seek(partition, offset);
  }

  @Override
  public void seekToBeginning(Collection<TopicPartition> partitions) {
    delegate.seekToBeginning(partitions);
  }

  @Override
  public void seekToEnd(Collection<TopicPartition> partitions) {
    delegate.seekToEnd(partitions);
  }

  @Override
  public long position(TopicPartition partition) {
    return delegate.position(partition);
  }

  @Override
  public OffsetAndMetadata committed(TopicPartition partition) {
    return delegate.committed(partition);
  }

  @Override
  public Map<MetricName, ? extends Metric> metrics() {
    return delegate.metrics();
  }

  @Override
  public List<PartitionInfo> partitionsFor(String topic) {
    return delegate.partitionsFor(topic);
  }

  @Override
  public Map<String, List<PartitionInfo>> listTopics() {
    return delegate.listTopics();
  }

  @Override
  public Set<TopicPartition> paused() {
    return pausedTps;
  }

  @Override
  public void pause(Collection<TopicPartition> partitions) {
    pausedTps.addAll(partitions);
  }

  @Override
  public void resume(Collection<TopicPartition> partitions) {
    pausedTps.removeAll(partitions);
  }

  @Override
  public void close() {
    delegate.close();
  }

  @Override
  public void wakeup() {
    delegate.wakeup();
  }


}
