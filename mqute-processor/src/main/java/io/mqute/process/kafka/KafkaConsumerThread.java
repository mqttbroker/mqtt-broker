package io.mqute.process.kafka;

import org.apache.kafka.clients.consumer.CommitFailedException;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.common.errors.InterruptException;
import org.apache.kafka.common.errors.WakeupException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import io.mqute.kafka.msg.KafkaMqttMessageWrapper;
import io.mqute.util.Threads;

/**
 * Created by huz on 7/26/16.
 */
public class KafkaConsumerThread<T> implements Runnable {

  private static final Logger LOGGER = LoggerFactory.getLogger(KafkaConsumerThread.class);

  private final String threadName;
  private final Consumer<String, T> consumer;
  private final Function<ConsumerRecords<String, T>, Void> consume;

  public KafkaConsumerThread(final String threadGroup,
                             final Consumer<String, T> consumer,
                             final Function<ConsumerRecords<String, T>, Void> consume) {

    this.threadName = Threads.nextThreadName(threadGroup);
    this.consumer = consumer;
    this.consume = consume;
  }

  @Override
  public void run() {
    Thread.currentThread().setName(threadName);
    LOGGER.info("[consumer] activated: {}", threadName);
    while (true) {
      try {
        final long before = System.nanoTime();
        final ConsumerRecords<String, T> msgs = consumer.poll(TimeUnit.SECONDS.toMillis(1));
        final long middle = System.nanoTime();
        consume.apply(msgs);
        final long after = System.nanoTime();
        LOGGER.trace("[consumer] {} polled messages in:{}ns, processed in:{}ns, total:{}ns", threadName, middle - before, after - middle, after - before);
        consumer.commitSync();
      } catch (CommitFailedException ex) {
        LOGGER.error("[consumer] commit failed due to: {}", ex);
        consumer.close();
        return;
      } catch (InterruptException | WakeupException ex) {
        LOGGER.warn("[consumer] interrupted or waked up, stop the consumer: {}", threadName);
        consumer.close();
        return;
      } catch (Exception ex) {
        LOGGER.warn("[consumer] caught unknown error:{}", ex);
      }
    }
  }
}
