package io.mqute.process;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;

/**
 * Created by huz on 7/27/16.
 */
public interface Traverse<T, R> {

  void traverse(Consumer<String, T> consumer,
                Iterable<ConsumerRecord<String, T>> records,
                R keepAlive);
}
