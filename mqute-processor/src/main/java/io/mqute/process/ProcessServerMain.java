package io.mqute.process;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import io.mqute.IMessageDispatcher;
import io.mqute.kafka.consumer.MquteKafkaConsumerFactory;
import io.mqute.kafka.producer.KafkaMessageDispatcher;
import io.mqute.persistence.*;
import io.mqute.persistence.redis.RedisService;
import io.mqute.process.batch.*;
import io.mqute.process.publication.traverse.RetainModification;
import io.mqute.process.publication.traverse.RetainPublications;
import io.mqute.process.publication.traverse.RouterModification;
import io.mqute.process.publication.traverse.RouterPublications;
import io.mqute.process.subscription.traverse.Acknowledge;
import io.mqute.process.subscription.traverse.ReleaseDone;
import io.mqute.process.subscription.traverse.ExactlyOnce;
import io.netty.util.HashedWheelTimer;
import io.netty.util.Timer;

/**
 * Created by zhuwang on 8/6/16.
 */
public class ProcessServerMain {

  public static void main(String[] args) {
    final MquteKafkaConsumerFactory consumerFactory = new MquteKafkaConsumerFactory();
    final Timer timer = new HashedWheelTimer(512, TimeUnit.MILLISECONDS);
    // FIXME: 9/4/16 is the callback address and port correct here?
    IMessageDispatcher dispatcher = new KafkaMessageDispatcher("", 0);

    // DAOs
    String redisUri = "redis://127.0.0.1:6379/0";
    PersistenceService service = new RedisService(redisUri);
    PartitionRootDao partitionRootDao = new PartitionRootDao(service);
    RetainedPublishDao retainedPublishDao = new RetainedPublishDao(service);
    RouterDao routerDao = new RouterDao(service);
    ClientsDao clientsDao = new ClientsDao(service);
    BacklogDao backlogDao = new BacklogDao(service);
    AdvanceDao advanceDao = new AdvanceDao(service);

    // All services
    Executor executor = Executors.newFixedThreadPool(16);
    ReleaseService releaseService = new ReleaseService(executor, 2);
    ConnAckService connAckService = new ConnAckService(executor, 2);
    BacklogService backlogService = new BacklogService(backlogDao, executor, 2);
    CleanupService cleanupService = new CleanupService(dispatcher, backlogDao, executor, 2);
    PublishService publishService = new PublishService(backlogService, executor, 2);
    AdvanceService advanceService = new AdvanceService(advanceDao, executor, 2);
    LoadingService loadingService = new LoadingService(backlogDao, advanceDao, publishService, releaseService, executor, 2);

    // subscription
    io.mqute.process.subscription.traverse.Publication subPublication =
        new io.mqute.process.subscription.traverse.Publication(publishService, backlogService);
    io.mqute.process.subscription.traverse.Modification subModification =
        new io.mqute.process.subscription.traverse.Modification(
            partitionRootDao, connAckService, cleanupService, loadingService
        );
    ReleaseDone releaseDone = new ReleaseDone(advanceService, loadingService);
    ExactlyOnce exactlyOnce = new ExactlyOnce(advanceService, releaseService);
    Acknowledge acknowledge = new Acknowledge(advanceService, loadingService);
    io.mqute.process.subscription.Consumption subConsumption =
        new io.mqute.process.subscription.Consumption(
            consumerFactory, timer, partitionRootDao, subModification,
            subPublication, acknowledge, exactlyOnce, releaseDone
        );
    subConsumption.consume();

    // retained
    RetainPublications retainRetainPublications =
        new RetainPublications(dispatcher);
    RetainModification retainRetainModification =
        new RetainModification(retainedPublishDao);
    RouterModification routerModification =
        new RouterModification(routerDao);
    RouterPublications routerPublications =
        new RouterPublications(dispatcher);
    io.mqute.process.publication.Consumption retainConsumption =
        new io.mqute.process.publication.Consumption(
            consumerFactory, timer, retainedPublishDao, routerDao, retainRetainModification, retainRetainPublications, routerModification, routerPublications);
    retainConsumption.consume();

    // keep alive
//    io.mqute.process.keepalive.traverse.Modification klModification =
//        new io.mqute.process.keepalive.traverse.Modification(clientsDao);
//    Monitoring monitoring = new Monitoring(dispatcher);
//    TouchPoint touchPoint = new TouchPoint();
//    io.mqute.process.keepalive.Consumption klConsumption =
//        new io.mqute.process.keepalive.Consumption(
//            consumerFactory, clientsDao, klModification, touchPoint, monitoring
//        );
//    klConsumption.consume();
  }

}
