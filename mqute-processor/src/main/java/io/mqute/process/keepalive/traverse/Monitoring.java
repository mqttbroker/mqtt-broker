package io.mqute.process.keepalive.traverse;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.mqute.IMessageDispatcher;
import io.mqute.kafka.msg.KafkaMqttMessageWrapper;
import io.mqute.msg.ConnectMessage;
import io.mqute.msg.DisconnectMessage;
import io.mqute.process.Traverse;
import io.mqute.process.keepalive.Clients;

/**
 * Created by huz on 7/28/16.
 */
public class Monitoring implements Traverse<KafkaMqttMessageWrapper, Map<Integer, Clients>> {

  private final IMessageDispatcher dispatcher;

  public Monitoring(final IMessageDispatcher dispatcher) {
    this.dispatcher = dispatcher;
  }

  @Override
  public void traverse(final Consumer<String, KafkaMqttMessageWrapper> consumer,
                       final Iterable<ConsumerRecord<String, KafkaMqttMessageWrapper>> records,
                       final Map<Integer, Clients> clients) {

    final Long now = System.currentTimeMillis();

    clients.values().forEach(suspects -> {
      suspects.keepAlives().forEach((clientID, pair) -> {
        final ConnectMessage connectMessage = pair.getRight();
        final Long touchPoint = pair.getLeft();
        if (now - touchPoint > TimeUnit.SECONDS.toMillis(connectMessage.getKeepAlive() + connectMessage.getKeepAlive() / 2)) {
          connectMessage.getWillMessageOpt().ifPresent(willMessage -> dispatcher.dispatch(willMessage, clientID));
          dispatcher.dispatch(new DisconnectMessage(), clientID);
        }
      });

      suspects.others().forEach((clientID, touchPoint) -> {
        //handle purge, pseudo DISCONNECT
        if (now - touchPoint > TimeUnit.DAYS.toMillis(1)) {
          dispatcher.dispatch(new DisconnectMessage(), clientID);
        }
      });
    });
  }
}
