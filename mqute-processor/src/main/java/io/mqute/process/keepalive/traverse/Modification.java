package io.mqute.process.keepalive.traverse;

import com.google.common.collect.Sets;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.Deserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import io.mqute.kafka.msg.KafkaMqttMessageWrapper;
import io.mqute.kafka.serialization.KafkaMqttMessageWrapperDeserializer;
import io.mqute.msg.ConnectMessage;
import io.mqute.persistence.ClientsDao;
import io.mqute.process.Traverse;
import io.mqute.process.keepalive.Clients;

/**
 * Created by huz on 7/27/16.
 */
public class Modification implements Traverse<KafkaMqttMessageWrapper, Map<Integer, Clients>> {

  private static final Logger LOGGER = LoggerFactory.getLogger(Modification.class);

  private final ClientsDao clientsDao;

  public Modification(final ClientsDao clientsDao) {
    this.clientsDao = clientsDao;
  }

  @Override
  public void traverse(final Consumer<String, KafkaMqttMessageWrapper> consumer,
                       final Iterable<ConsumerRecord<String, KafkaMqttMessageWrapper>> records,
                       final Map<Integer, Clients> clients) {

    final Set<Integer> impacts = Sets.newHashSetWithExpectedSize(clients.size());

    records.forEach(record -> {

      final KafkaMqttMessageWrapper wrapper = record.value();
      if (wrapper.getSenderHost() == null || wrapper.getSenderPort() < 0) {
        return;//hey, that's what i sent, ignore it.
      }

      final int partition = record.partition();
      if (!clients.containsKey(partition)) {
        synchronized (clients) {
          if (!clients.containsKey(partition)) {
            try {
              clients.put(partition, clientsDao.get(String.valueOf(partition)).get(10, TimeUnit.SECONDS));
            } catch (Exception e) {
              LOGGER.error("[modification] keepalive failed to load clients due to: {}", e);
            }
          }
        }
      }

      final Clients root = clients.get(partition);
      final String clientID = wrapper.getClientId();
      final Long now = System.currentTimeMillis();
      // Only care about CONNECT and DISCONNECT
      switch (wrapper.getOriginalMessage().getMessageType()) {
        case CONNECT:
          final ConnectMessage connectMessage = (ConnectMessage) wrapper.getOriginalMessage();
          if (connectMessage.getKeepAlive() > 0) {
            root.others().remove(clientID);
            root.keepAlives().put(clientID, ImmutablePair.of(now, connectMessage));
          } else {
            root.keepAlives().remove(clientID);
            root.others().put(clientID, now);
          }
          impacts.add(partition);
          break;
        case DISCONNECT:
          root.keepAlives().remove(clientID);
          root.others().remove(clientID);
          impacts.add(partition);
          break;
        default:
          break;
      }
    });

    impacts.forEach(partition -> clientsDao.update(String.valueOf(partition), clients.get(partition)));
  }
}
