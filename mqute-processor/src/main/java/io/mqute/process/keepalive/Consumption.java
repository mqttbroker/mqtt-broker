package io.mqute.process.keepalive;

import com.google.common.collect.Maps;

import io.mqute.kafka.msg.KafkaMqttMessageWrapper;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import io.mqute.kafka.KafkaTopics;
import io.mqute.kafka.consumer.MquteKafkaConsumerFactory;
import io.mqute.persistence.ClientsDao;
import io.mqute.process.kafka.KafkaConsumerThread;
import io.mqute.process.keepalive.traverse.Modification;
import io.mqute.process.keepalive.traverse.Monitoring;
import io.mqute.process.keepalive.traverse.TouchPoint;
import io.netty.util.Timeout;
import io.netty.util.Timer;

/**
 * Created by huz on 7/20/16.
 */
public class Consumption {

  private static final Logger LOGGER = LoggerFactory.getLogger(Consumption.class);
  private final MquteKafkaConsumerFactory consumerFactory;
  private final Timer timer;
  private final ClientsDao clientsDao;
  private final Map<Integer, Clients> clients;
  private final Modification modification;
  private final TouchPoint touchPoint;
  private final Monitoring monitoring;

  private ExecutorService executors = Executors.newCachedThreadPool();

  public Consumption(final MquteKafkaConsumerFactory consumerFactory,
                     final Timer timer,
                     final ClientsDao clientsDao,
                     final Modification modification,
                     final TouchPoint touchPoint,
                     final Monitoring monitoring) {

    this.clients = Maps.newConcurrentMap();

    this.consumerFactory = consumerFactory;
    this.timer = timer;
    this.clientsDao = clientsDao;
    this.modification = modification;
    this.touchPoint = touchPoint;
    this.monitoring = monitoring;
  }

  public void consume() {

    final KafkaConsumer<String, KafkaMqttMessageWrapper> keepAliveConsumer = consumerFactory.create("keepalive-consumer", "keepalive-processor");

    keepAliveConsumer.subscribe(Arrays.asList(KafkaTopics.GENERAL_TOPIC, KafkaTopics.KEEP_ALIVE_TOPIC), new ConsumerRebalanceListener() {

      final Map<TopicPartition, Timeout> delayedCancellations = Maps.newConcurrentMap();
      final Map<TopicPartition, Future<?>> runnings = Maps.newConcurrentMap();

      @Override
      public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
        partitions.forEach(p -> delayedCancellations.put(p, timer.newTimeout(timeout -> {
          Optional.ofNullable(runnings.remove(p)).ifPresent(r -> r.cancel(true));
          Consumption.this.clients.remove(p.partition());
        }, 1L, TimeUnit.MINUTES)));
      }

      @Override
      public void onPartitionsAssigned(Collection<TopicPartition> partitions) {
        partitions.forEach(p -> Optional.ofNullable(delayedCancellations.remove(p)).ifPresent(Timeout::cancel));
        partitions.stream().filter(p -> !runnings.containsKey(p)).forEach(p -> clientsDao.get(String.valueOf(p.partition())).thenAccept(clients -> {
          synchronized (Consumption.this.clients) {
            if (!Consumption.this.clients.containsKey(p.partition())) {
              Consumption.this.clients.put(p.partition(), clients);
            }
          }

//          final Consumer<String, KafkaMqttMessageWrapper> activityConsumer = consumerFactory.create("keepalive-consumer-" + p.partition(), "keepalive-processor");
//          runnings.put(p, executors.submit(new KafkaConsumerThread("alive-consume-pub-ack", activityConsumer, msgs -> {
//              touchPoint.traverse(activityConsumer, msgs.records(KafkaTopics.KEEP_ALIVE_TOPIC), Consumption.this.clients);
//              return null;
//          })));
//          activityConsumer.assign(Collections.singletonList(new TopicPartition(KafkaTopics.KEEP_ALIVE_TOPIC, p.partition())));
        }));
      }
    });

    executors.submit(new KafkaConsumerThread<>("alive-consume-con", keepAliveConsumer, msgs -> {
      modification.traverse(keepAliveConsumer, msgs.records(KafkaTopics.GENERAL_TOPIC), clients);
      touchPoint.traverse(keepAliveConsumer, msgs.records(KafkaTopics.KEEP_ALIVE_TOPIC), clients);
      monitoring.traverse(keepAliveConsumer, Collections.emptyList(), clients);
      return null;
    }));
  }
}
