package io.mqute.process.keepalive;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.apache.commons.lang3.tuple.Pair;

import java.util.Map;
import java.util.concurrent.ConcurrentMap;

import io.mqute.msg.ConnectMessage;

/**
 * Created by huz on 7/27/16.
 */
public class Clients {

  private final ConcurrentMap<String, Pair<Long, ConnectMessage>> keepAlives;
  private final ConcurrentMap<String, Long> others;

  @JsonCreator
  public Clients(final @JsonProperty("keepAlives") ConcurrentMap<String, Pair<Long, ConnectMessage>> keepAlives,
                 final @JsonProperty("others") ConcurrentMap<String, Long> others) {

    this.keepAlives = keepAlives;
    this.others = others;
  }

  @JsonProperty("keepAlives")
  public Map<String, Pair<Long, ConnectMessage>> keepAlives() {
    return keepAlives;
  }
  //client, last touch point timestamp, and original connect message
  //NOTE that only keepAlives indicating `keepAlive` will be tracked here
  //`ConnectMessage` will provide the will message publishes that's needed

  @JsonProperty("others")
  public Map<String, Long> others() {
    return others;
  }

}
