package io.mqute.process.keepalive.traverse;

import io.mqute.kafka.msg.KafkaMqttMessageWrapper;
import io.mqute.msg.enums.MessageType;
import io.mqute.process.Traverse;
import io.mqute.process.keepalive.Clients;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.util.Map;

/**
 * Created by huz on 7/27/16.
 */
public class TouchPoint implements Traverse<KafkaMqttMessageWrapper, Map<Integer, Clients>> {

  @Override
  public void traverse(final Consumer<String, KafkaMqttMessageWrapper> consumer,
                       final Iterable<ConsumerRecord<String, KafkaMqttMessageWrapper>> records,
                       final Map<Integer, Clients> clients) {
    records.forEach(record -> {

      if(MessageType.PINGRESP == record.value().getOriginalMessage().getMessageType()) {
        final String clientId = record.value().getClientId();
        final int partition = record.partition();
        final Clients root = clients.get(partition);
        final Long now = System.currentTimeMillis();

        if (root.keepAlives().containsKey(clientId)) {
          root.keepAlives().put(clientId, ImmutablePair.of(now, root.keepAlives().get(clientId).getRight()));
        }
        if (root.others().containsKey(clientId)) {
          root.others().put(clientId, now);
        }
      }
    });
  }
}
