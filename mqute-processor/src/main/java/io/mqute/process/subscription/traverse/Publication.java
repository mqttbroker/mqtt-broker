package io.mqute.process.subscription.traverse;

import com.google.common.collect.*;
import io.mqute.edge.Client;
import io.mqute.kafka.KafkaTopics;
import io.mqute.kafka.msg.KafkaMqttMessageWrapper;
import io.mqute.msg.PublishMessage;
import io.mqute.msg.enums.MessageType;
import io.mqute.msg.enums.QoSType;
import io.mqute.process.Traverse;
import io.mqute.process.batch.BacklogService;
import io.mqute.process.batch.PublishService;
import io.mqute.process.subscription.PartitionRoot;
import io.mqute.process.subscription.Subscription;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.TopicPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

/**
 * Created by huz on 7/21/16.
 */
public class Publication implements Traverse<KafkaMqttMessageWrapper, PartitionRoot> {

  private static final Logger LOGGER = LoggerFactory.getLogger(Publication.class);

  final PublishService publishService;
  final BacklogService backlogService;

  public Publication(final PublishService publishService,
                     final BacklogService backlogService) {
    this.publishService = publishService;
    this.backlogService = backlogService;
  }

  @Override
  public void traverse(final Consumer<String, KafkaMqttMessageWrapper> consumer,
                       final Iterable<ConsumerRecord<String, KafkaMqttMessageWrapper>> records,
                       final PartitionRoot partitionRoot) {
    //quickly bypass empty records
    if(Iterables.isEmpty(records)){
      return;
    }

    final Multimap<Integer, CompletableFuture<?>> pauses = Multimaps.newListMultimap(Maps.newHashMapWithExpectedSize(Iterables.size(records)), Lists::newLinkedList);
    //partition key is what the PUBLISH (edge node) uses, could be either PUBLISHER's clientID or the TOPIC name it publishes to.
    records.forEach(record -> {
      final KafkaMqttMessageWrapper kafkaMessage = record.value();
      if (kafkaMessage.getOriginalMessage().getMessageType() == MessageType.PUBLISH) {
        final PublishMessage publishMessage = (PublishMessage) kafkaMessage.getOriginalMessage();
        LOGGER.info("[sub][publication] handle publish of topic:{} with qos:{} and packet:{}, retained:{}", publishMessage.getTopicName(), publishMessage.getQos(), publishMessage.getPacketId(), publishMessage.isRetain());
        partitionRoot.filter(publishMessage.getTopicName()).forEach(node -> {
          node.subscriptions().forEach(subscription -> {
            final Client client = partitionRoot.clients().get(subscription.clientID()/*SUBSCRIBER's clientID*/);
            if (client == null) {
              if (subscription.persistent()) {
                pauses.put(record.partition(), backlog(publishMessage, subscription.clientID()));
              }
              return;
            }

            final Set<String> targets = Sets.newHashSet(kafkaMessage.getTargets());
            if (!targets.isEmpty() && !targets.contains(client.clientID())) {
              return;//for retained message publish handling, filter keepAlives by what's in the targets given explicitly
            }
            switch (client.state()) {
              case IDLE:
              case QUEUED:
                LOGGER.debug("[sub][publication] client:{} handles this publish immediately", client.clientID());
                pauses.put(record.partition(), waitFor(publishMessage, client, subscription));
                break;
              case SLOW:
                LOGGER.debug("[sub][publication] client:{} too slow and persists this publish", client.clientID());
                pauses.put(record.partition(), backlog(publishMessage, subscription.clientID()));
                break;
              case DISCONNECTED:
                if (subscription.persistent()) {//only PERSISTENT subscriptions will be backlogged
                  LOGGER.debug("[sub][publication] client:{} disconnected and persists this publish", client.clientID());
                  pauses.put(record.partition(), backlog(publishMessage, subscription.clientID()));
                }
                break;
            }
          });
        });
      }
    });

    if (!pauses.isEmpty()) {
      final Collection<TopicPartition> pausedTps = pauses.keySet().stream().map(p -> new TopicPartition(KafkaTopics.ROUTED_PUBLISH_TOPIC, p)).collect(Collectors.toSet());
      LOGGER.info("[sub][publication] pauses:{}", pausedTps);
      consumer.pause(pausedTps);
      pauses.asMap().forEach((paused, unblocks) -> {
        CompletableFuture.allOf(unblocks.toArray(new CompletableFuture[unblocks.size()])).whenComplete((v, ex) -> {
          final List<TopicPartition> resumedTps = Collections.singletonList(new TopicPartition(KafkaTopics.ROUTED_PUBLISH_TOPIC, paused));
          LOGGER.info("[sub][publication] resumes:{} with exception:{}", resumedTps, ex);
          consumer.resume(resumedTps);
        });
      });
    }
  }

  protected CompletableFuture<?> waitFor(final PublishMessage publishMessage, final Client client, final Subscription subscription) {
    return publishService.asyncHandle(ImmutableTriple.of(publishMessage, client, QoSType.fromByte(subscription.qos()))).whenComplete((v, ex) -> {
      LOGGER.info("[sub][publication] waitFor resolved of: {}", client.clientID());
    });
  }

  protected CompletableFuture<?> backlog(final PublishMessage publishMessage, final String clientID) {
    return backlogService.asyncHandle(ImmutablePair.of(publishMessage, clientID));
  }
}
