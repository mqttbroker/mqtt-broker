package io.mqute.process.subscription;

import com.google.common.collect.Maps;
import io.mqute.kafka.KafkaTopics;
import io.mqute.kafka.consumer.MquteKafkaConsumerFactory;
import io.mqute.kafka.msg.KafkaMqttMessageWrapper;
import io.mqute.persistence.PartitionRootDao;
import io.mqute.process.kafka.KafkaConsumerThread;
import io.mqute.process.subscription.traverse.*;
import io.netty.util.Timeout;
import io.netty.util.Timer;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.common.TopicPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by huz on 7/20/16.
 */
public class Consumption {

    private static final Logger LOGGER = LoggerFactory.getLogger(Consumption.class);

    private final MquteKafkaConsumerFactory consumerFactory;
    private final Timer timer;
    private final PartitionRootDao partitionRootDao;
    private final Map<Integer, PartitionRoot> partitions;
    private final Modification modification;
    private final Publication publication;
    private final Acknowledge acknowledge;
    private final ExactlyOnce exactlyOnce;
    private final ReleaseDone onCompleted;

    private ExecutorService executors = Executors.newCachedThreadPool();

    public Consumption(final MquteKafkaConsumerFactory consumerFactory,
                       final Timer timer,
                       final PartitionRootDao partitionRootDao,
                       final Modification modification,
                       final Publication publication,
                       final Acknowledge acknowledge,
                       final ExactlyOnce exactlyOnce,
                       final ReleaseDone onCompleted) {

        this.partitions = Maps.newConcurrentMap();
        this.consumerFactory = consumerFactory;
        this.timer = timer;
        this.partitionRootDao = partitionRootDao;
        this.modification = modification;
        this.publication = publication;
        this.acknowledge = acknowledge;
        this.exactlyOnce = exactlyOnce;
        this.onCompleted = onCompleted;
    }

    public void consume() {

        final Consumer<String, KafkaMqttMessageWrapper> subscriptionConsumer = consumerFactory.create("subscription-consumer", "subscription-processor");

        subscriptionConsumer.subscribe(Collections.singletonList(KafkaTopics.GENERAL_TOPIC), new ConsumerRebalanceListener() {

            final Map<TopicPartition, Timeout> delayedCancellations = Maps.newConcurrentMap();
            final Map<TopicPartition, Future<?>> runnings = Maps.newConcurrentMap();

            @Override
            public void onPartitionsRevoked(final Collection<TopicPartition> partitions) {
                partitions.forEach(p -> {
                    LOGGER.warn("[consumption][sub] partitions revoked:{}", partitions);
                    delayedCancellations.put(p, timer.newTimeout(timeout -> {
                        Optional.ofNullable(runnings.remove(p)).ifPresent(r -> r.cancel(true));
                        Consumption.this.partitions.remove(p.partition());
                    }, 1L, TimeUnit.MINUTES));
                });
            }

            @Override
            public void onPartitionsAssigned(final Collection<TopicPartition> partitions) {
                LOGGER.info("[consumption][sub] partitions assigned:{}", partitions);
                partitions.forEach(p -> Optional.ofNullable(delayedCancellations.remove(p)).ifPresent(Timeout::cancel));
                partitions.stream().filter(p -> !runnings.containsKey(p)).forEach(p -> {
                    partitionRootDao.get(String.valueOf(p.partition())).thenAccept(partitionRoot -> {
                        synchronized (Consumption.this.partitions) {
                            if (!Consumption.this.partitions.containsKey(p.partition())) {
                                Consumption.this.partitions.put(p.partition(), partitionRoot);
                            }
                        }

                        final Consumer<String, KafkaMqttMessageWrapper> publishAckConsumer = consumerFactory.create("publish-ack-consumer-" + p.partition(), "subscription-processor");
                        runnings.put(p, executors.submit(new KafkaConsumerThread<>("sub-consume-pub-ack", publishAckConsumer, msgs -> {
                            publication.traverse(publishAckConsumer, msgs.records(KafkaTopics.ROUTED_PUBLISH_TOPIC), partitionRoot);
                            acknowledge.traverse(publishAckConsumer, msgs.records(KafkaTopics.ACK_TOPIC), partitionRoot);
                            exactlyOnce.traverse(publishAckConsumer, msgs.records(KafkaTopics.ACK_TOPIC), partitionRoot);
                            onCompleted.traverse(publishAckConsumer, msgs.records(KafkaTopics.ACK_TOPIC), partitionRoot);
                            return null;
                        })));

                        publishAckConsumer.assign(Stream.of(KafkaTopics.ROUTED_PUBLISH_TOPIC, KafkaTopics.ACK_TOPIC).map(t -> new TopicPartition(t, p.partition())).collect(Collectors.toList()));
                    });
                });

            }
        });

        executors.submit(new KafkaConsumerThread<>("sub-consume-con", subscriptionConsumer, msgs -> {
            modification.traverse(subscriptionConsumer, msgs.records(KafkaTopics.GENERAL_TOPIC), partitions);
            return null;
        }));
    }
}
