package io.mqute.process.subscription;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.mqute.msg.enums.QoSType;

/**
 * Created by huz on 7/21/16.
 */
public class Subscription {

  private final String clientID;
  private final String topicFilter;
  private final byte qos;

  public Subscription(final @JsonProperty("clientID") String clientID,
                      final @JsonProperty("topicFilter") String topicFilter,
                      final @JsonProperty("qos") byte qos) {

    this.clientID = clientID;
    this.topicFilter = topicFilter;
    this.qos = qos;
  }

  @JsonProperty("clientID")
  public String clientID() {
    return clientID;
  }

  @JsonProperty("topicFilter")
  public String topicFilter() {
    return topicFilter;
  }

  public boolean persistent() {
    return QoSType.EXACT_ONCE.getByteValue() == qos() || QoSType.LEAST_ONCE.getByteValue() == qos();
  }

  @JsonProperty("qos")
  public byte qos() {
    return qos;
  }

}
