package io.mqute.process.subscription;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.google.common.base.MoreObjects;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import io.mqute.edge.Client;
import org.apache.commons.lang3.tuple.Pair;
import org.cliffc.high_scale_lib.NonBlockingHashMap;
import org.cliffc.high_scale_lib.NonBlockingHashSet;

import java.util.*;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

import static io.mqute.util.Topics.split;

/**
 * Created by huz up 7/21/16.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.MINIMAL_CLASS)
public class AbstractNode implements Node {

  private final ConcurrentMap<String, Node> children;
  private final Set<Subscription> subscriptions;

  @JsonCreator
  public AbstractNode(final @JsonProperty("children") ConcurrentMap<String, Node> children,
                      final @JsonProperty("subscriptions") Set<Subscription> subscriptions) {

    this.children = MoreObjects.firstNonNull(children, new NonBlockingHashMap<>());
    this.subscriptions = MoreObjects.firstNonNull(subscriptions, new NonBlockingHashSet<>());
  }

  @Override
  public ConcurrentMap<String, Node> children() {
    return children;
  }

  @Override
  public Set<Subscription> subscriptions() {
    return subscriptions;
  }

  @Override
  public Node locate(final String topicFilter) {
    if (Strings.isNullOrEmpty(topicFilter)) {
      return this;
    }

    final Pair<String, String> headAndTail = split(topicFilter);
    final String head = headAndTail.getLeft();

    if ("#".equals(head) && !Strings.isNullOrEmpty(headAndTail.getRight())) {
      throw new IllegalArgumentException("[locate] topicFilter is invalid, cannot have further filter after # " + topicFilter);
    }

    Node child;
    if (!children.containsKey(head)) {
      final AbstractNode birth = new AbstractNode(null, null);
      child = MoreObjects.firstNonNull(children.putIfAbsent(head, birth), birth);
    }
    else{
      child = children.get(head);
    }

    return child.locate(headAndTail.getRight());
  }

  @Override
  public Collection<Node> filter(final Client client) {

    final List<Node> aggregates = Lists.newLinkedList();

    final String clientID = client.clientID();
    if(subscriptions.stream().anyMatch(s -> Objects.equals(clientID, s.clientID()))){
        aggregates.add(this);
    }

    children().values().forEach(child -> aggregates.addAll(child.filter(client)));

    return aggregates;
  }

  @Override
  public Collection<Node> filter(final String topic) {
    if (Strings.isNullOrEmpty(topic)) {
      return Collections.singletonList(this);
    }

    final List<Node> aggregates = Lists.newLinkedList();

    final Node any = children.get("#");
    if (any != null) {
      aggregates.add(any);//there cannot be any further children under #
    }

    final Pair<String, String> headAndTail = split(topic);
    final Node one = children.get("+");
    if(one != null) {
      aggregates.addAll(one.filter(headAndTail.getRight()));
    }

    final Node found = children.get(headAndTail.getLeft());
    if(found != null) {
      aggregates.addAll(found.filter(headAndTail.getRight()));
    }

    return aggregates;
  }

  @Override
  public void append(final Subscription subscription) {
    subscriptions.add(subscription);
  }

  @Override
  public Subscription remove(String clientID) {
    final List<Subscription> removals = subscriptions.stream().filter(s -> Objects.equals(clientID, s.clientID())).collect(Collectors.toList());
    if(!removals.isEmpty()){
      subscriptions.removeAll(removals);
      return removals.get(0);
    }
    return null;
  }
}
