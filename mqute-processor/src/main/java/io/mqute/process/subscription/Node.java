package io.mqute.process.subscription;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collection;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;

import io.mqute.edge.Client;

/**
 * Created by huz on 7/21/16.
 */
public interface Node {

  @JsonProperty("children")
  ConcurrentMap<String, Node> children();

  @JsonProperty("subscriptions")
  Set<Subscription> subscriptions();

  /**
   * needed by subscription traversal only
   */
  Node locate(final String topicFilter);

  /**
   * needed by connect/disconnect traversal only
   */
  Collection<Node> filter(final Client client);

  /**
   * needed by publication traversal only
   */
  Collection<Node> filter(final String topic);

  void append(final Subscription subscription);

  Subscription remove(final String clientID);
}
