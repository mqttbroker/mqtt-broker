package io.mqute.process.subscription.traverse;

import com.google.common.collect.Sets;

import io.mqute.process.Advance;
import io.mqute.process.batch.AdvanceService;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;

import io.mqute.edge.Client;
import io.mqute.edge.State;
import io.mqute.kafka.msg.KafkaMqttMessageWrapper;
import io.mqute.msg.PubCompMessage;
import io.mqute.msg.enums.MessageType;
import io.mqute.process.Traverse;
import io.mqute.process.batch.LoadingService;
import io.mqute.process.subscription.PartitionRoot;

/**
 * Created by huz on 7/21/16.
 */
public class ReleaseDone implements Traverse<KafkaMqttMessageWrapper, PartitionRoot> {

  private static final Logger LOGGER = LoggerFactory.getLogger(ReleaseDone.class);

  private final AdvanceService advanceService;
  private final LoadingService loadingService;

  public ReleaseDone(final AdvanceService advanceService,
                     final LoadingService loadingService) {

    this.advanceService = advanceService;
    this.loadingService = loadingService;
  }

  @Override
  public void traverse(final Consumer<String, KafkaMqttMessageWrapper> consumer,
                       final Iterable<ConsumerRecord<String, KafkaMqttMessageWrapper>> records,
                       final PartitionRoot root) {

    final Set<Client> slowClientsWithEmptyQueues = Sets.newHashSet();

    records.forEach(record -> {
      final String clientID = record.key();
      if(record.value().getOriginalMessage().getMessageType() == MessageType.PUBCOMP) {
        final PubCompMessage compMessage = (PubCompMessage) record.value().getOriginalMessage();
        if (root.clients().containsKey(clientID)) {
          final Client client = root.clients().get(clientID);
          try {
            client.comp(compMessage.getPacketId()).get();
            //ACK should have a resolved future by now
          } catch (Exception e) {
            LOGGER.error("[COMP] handle failure due to: {}", e);
          }
          //exactlyOnceClients are mutually exclusive from slowClientsWithEmptyQueues, as they cannot have empty queues yet.
          if (client.state() == State.SLOW && client.pendings().isEmpty()) {
            slowClientsWithEmptyQueues.add(client);
          }
        }
        else{
          advanceService.asyncHandle(ImmutableTriple.of(clientID, compMessage.getPacketId(), Advance.COMP_RECEIVED));
        }
      }
    });

    slowClientsWithEmptyQueues.stream().map(loadingService::asyncHandle);
  }
}
