package io.mqute.process.subscription;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;

import io.mqute.edge.Client;

/**
 * Created by huz on 7/21/16.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.MINIMAL_CLASS)
public class PartitionRoot extends AbstractNode {

  private final int partition;
  private final ConcurrentMap<String, Client> clients;

  @JsonCreator
  public PartitionRoot(final @JsonProperty("partition") int partition,
                       final @JsonProperty("clients") ConcurrentMap<String, Client> clients,
                       final @JsonProperty("children") ConcurrentMap<String, Node> children,
                       final @JsonProperty("subscriptions") Set<Subscription> subscriptions) {

    super(children, subscriptions);

    this.partition = partition;
    this.clients = clients;
  }

  @JsonProperty("partition")
  public int partition() {
    return partition;
  }

  @JsonProperty("clients")
  public Map<String, Client> clients() {
    return clients;
  }
}
