package io.mqute.process.subscription.traverse;

import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import io.mqute.edge.Client;
import io.mqute.edge.EdgeClient;
import io.mqute.edge.State;
import io.mqute.kafka.msg.KafkaMqttMessageWrapper;
import io.mqute.msg.ConnAckMessage;
import io.mqute.msg.ConnectMessage;
import io.mqute.msg.SubscribeMessage;
import io.mqute.msg.UnsubscribeMessage;
import io.mqute.msg.enums.ConnectReturnCode;
import io.mqute.persistence.PartitionRootDao;
import io.mqute.process.Traverse;
import io.mqute.process.batch.CleanupService;
import io.mqute.process.batch.ConnAckService;
import io.mqute.process.batch.LoadingService;
import io.mqute.process.subscription.PartitionRoot;
import io.mqute.process.subscription.Subscription;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by huz on 7/21/16.
 */
public class Modification implements Traverse<KafkaMqttMessageWrapper, Map<Integer, PartitionRoot>> {

  private static final Logger LOGGER = LoggerFactory.getLogger(Modification.class);

  private final PartitionRootDao partitionRootDao;
  private final ConnAckService connAckService;
  private final CleanupService cleanupService;
  private final LoadingService loadingService;

  public Modification(final PartitionRootDao partitionRootDao,
                      final ConnAckService connAckService,
                      final CleanupService cleanupService,
                      final LoadingService loadingService) {
    this.partitionRootDao = partitionRootDao;

    this.connAckService = connAckService;
    this.cleanupService = cleanupService;
    this.loadingService = loadingService;
  }

  @Override
  public void traverse(final Consumer<String, KafkaMqttMessageWrapper> consumer,
                       final Iterable<ConsumerRecord<String, KafkaMqttMessageWrapper>> records,
                       final Map<Integer, PartitionRoot> partitions) {

    final Set<Integer> impacts = Sets.newHashSetWithExpectedSize(partitions.size());

    records.forEach(record -> {
      final int partition = record.partition();/*SUBSCRIBER's clientID*/

      if (!partitions.containsKey(partition)) {
        synchronized (partitions) {
          if (!partitions.containsKey(partition)) {
            try {
              partitions.put(partition, partitionRootDao.get(String.valueOf(partition)).get(10, TimeUnit.SECONDS));
            } catch (Exception e) {
              LOGGER.error("[subscription] failed to load from persistence due to: {}", e);
            }
          }
        }
      }

      final PartitionRoot root = partitions.get(partition);
      final String clientID = record.key();
      switch (record.value().getOriginalMessage().getMessageType()) {
        case CONNECT:
          LOGGER.info("[sub][modification] handle connect from:{}", clientID);
          final ConnectMessage connectMessage = (ConnectMessage) record.value().getOriginalMessage();
          boolean sessionPresent = false;
          if (root.clients().containsKey(clientID)) {
            final Client knownClient = root.clients().get(clientID);
            knownClient.takeOver(record.value().getSenderHost() + ':' + record.value().getSenderPort());
            if (connectMessage.isCleanSession()) {
              final List<Subscription> subscriptions = root.filter(knownClient).stream().map(node -> node.remove(clientID)).collect(Collectors.toList());
              cleanupService.asyncHandle(ImmutablePair.of(knownClient, subscriptions));
            } else {
              //persistent session detected
              knownClient.transit(State.SLOW);
              loadingService.asyncHandle(knownClient);
              sessionPresent = true;
            }
          }//TODO load from persistent storage to get client's info when a partition increase happens, and need to handle duplicate problem
          else {
            //treated as an absolute new client
            final Client suspectClient = new EdgeClient(clientID, !connectMessage.isCleanSession(), record.value().getSenderHost() + ':' + record.value().getSenderPort(), State.IDLE, 0);
            root.clients().put(clientID, suspectClient);
            if (!root.filter(suspectClient).isEmpty()) {
              //persistent session detected
              suspectClient.transit(State.SLOW);
              loadingService.asyncHandle(suspectClient);
              sessionPresent = true;
            }
          }
          connAckService.asyncHandle(ImmutableTriple.of(new ConnAckMessage(ConnectReturnCode.ACCEPTED, sessionPresent), root.clients().get(clientID), root.partition()));
          impacts.add(partition);
          break;
        case DISCONNECT://KeepAlive processor will monitor and tell if DISCONNECT happens
          LOGGER.info("[sub][modification] disconnect of:{}", clientID);
          Optional.ofNullable(root.clients().remove(clientID)).map(client -> {
            try {
              if (!client.persistentSession()) {
                final List<Subscription> subscriptions = root.filter(client).stream().map(node -> node.remove(clientID)).collect(Collectors.toList());
                cleanupService.asyncHandle(ImmutablePair.of(client, subscriptions));
              }
              client.close();
            } catch (IOException e) {
              LOGGER.warn("client not closed properly due to:{}", e);
            }
            return client;
          });
          impacts.add(partition);
          break;
        case SUBSCRIBE:
          final SubscribeMessage subscribeMessage = (SubscribeMessage) record.value().getOriginalMessage();
          LOGGER.info("[sub][modification] subscribe {}", subscribeMessage.getSubscriptions());
          subscribeMessage.getSubscriptions().forEach((topicFilter, qoSType) -> {
            if (Strings.isNullOrEmpty(topicFilter)) {
              LOGGER.warn("[sub][modification] ignored empty subscription");
            } else {
              root.locate(topicFilter).append(new Subscription(clientID, topicFilter, qoSType.getByteValue()));
            }
          });
          impacts.add(partition);
          break;
        case UNSUBSCRIBE:
          if(record.value().getTargets().length > 1){
            //this unsubscribe is created by cleanup service and will be silently ignored by subscription processor
            //its target is meant for publish processor only to maintain its routing map
            break;
          }

          final UnsubscribeMessage unsubscribeMessage = (UnsubscribeMessage) record.value().getOriginalMessage();
          LOGGER.info("[sub][modification] unsubscribe {}", unsubscribeMessage.getTopicFilters());
          unsubscribeMessage.getTopicFilters().forEach(topicFilter -> {
            if (Strings.isNullOrEmpty(topicFilter)) {
              LOGGER.warn("[sub][modification] ignored empty subscription");
            } else {
              root.locate(topicFilter).remove(clientID);
            }
          });
          impacts.add(partition);
          break;
        default:
          break;
      }
    });

    impacts.forEach(partition -> partitionRootDao.update(String.valueOf(partition), partitions.get(partition)));
  }
}
