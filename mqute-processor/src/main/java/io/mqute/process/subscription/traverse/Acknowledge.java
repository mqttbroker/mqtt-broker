package io.mqute.process.subscription.traverse;

import com.google.common.collect.Sets;
import io.mqute.edge.Client;
import io.mqute.edge.State;
import io.mqute.kafka.msg.KafkaMqttMessageWrapper;
import io.mqute.msg.PubAckMessage;
import io.mqute.msg.enums.MessageType;
import io.mqute.process.Advance;
import io.mqute.process.Traverse;
import io.mqute.process.batch.LoadingService;
import io.mqute.process.batch.AdvanceService;
import io.mqute.process.subscription.PartitionRoot;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;

/**
 * Created by huz on 7/21/16.
 */
public class Acknowledge implements Traverse<KafkaMqttMessageWrapper, PartitionRoot> {

  private static final Logger LOGGER = LoggerFactory.getLogger(Acknowledge.class);

  private final AdvanceService advanceService;
  private final LoadingService loadingService;

  public Acknowledge(final AdvanceService advanceService,
                     final LoadingService loadingService) {

    this.advanceService = advanceService;
    this.loadingService = loadingService;
  }

  @Override
  public void traverse(final Consumer<String, KafkaMqttMessageWrapper> consumer,
                       final Iterable<ConsumerRecord<String, KafkaMqttMessageWrapper>> records,
                       final PartitionRoot root) {

    final Set<Client> slowClientsWithEmptyQueues = Sets.newHashSet();

    records.forEach(record -> {
      final String clientID = record.key();
      LOGGER.info("[sub][acknowledge] handles ack from:{}", clientID);
      if (root.clients().containsKey(clientID) && record.value().getOriginalMessage().getMessageType() == MessageType.PUBACK) {
        final PubAckMessage ackMessage = (PubAckMessage) record.value().getOriginalMessage();
        final Client client = root.clients().get(clientID);

        try {
          client.ack(ackMessage.getPacketId()).get();
          //ACK should have a resolved future by now
        } catch (Exception e) {
          LOGGER.error("[ack] handle failure due to: {}", e);
          advanceService.asyncHandle(ImmutableTriple.of(clientID, ackMessage.getPacketId(), Advance.ACK_RECEIVED));
        }
        //exactlyOnceClients are mutually exclusive from slowClientsWithEmptyQueues, as they cannot have empty queues yet.
        if (client.state() == State.SLOW && client.pendings().isEmpty()) {
          slowClientsWithEmptyQueues.add(client);
        }
      }
    });

    slowClientsWithEmptyQueues.stream().map(loadingService::asyncHandle);
  }
}
