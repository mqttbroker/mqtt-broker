package io.mqute.process.subscription.traverse;

import com.google.common.collect.Sets;
import io.mqute.edge.Client;
import io.mqute.kafka.msg.KafkaMqttMessageWrapper;
import io.mqute.msg.PubRecMessage;
import io.mqute.msg.PubRelMessage;
import io.mqute.msg.enums.MessageType;
import io.mqute.process.Advance;
import io.mqute.process.Traverse;
import io.mqute.process.batch.AdvanceService;
import io.mqute.process.batch.ReleaseService;
import io.mqute.process.subscription.PartitionRoot;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.util.Set;

/**
 * Created by huz on 7/21/16.
 */
public class ExactlyOnce implements Traverse<KafkaMqttMessageWrapper, PartitionRoot> {

  private final AdvanceService advanceService;
  private final ReleaseService releaseService;

  public ExactlyOnce(final AdvanceService advanceService,
                     final ReleaseService releaseService) {

    this.advanceService = advanceService;
    this.releaseService = releaseService;
  }

  @Override
  public void traverse(final Consumer<String, KafkaMqttMessageWrapper> consumer,
                       final Iterable<ConsumerRecord<String, KafkaMqttMessageWrapper>> records,
                       final PartitionRoot root) {

    final Set<Pair<PubRelMessage, Client>> exactlyOnceClients = Sets.newHashSet();

    records.forEach(record -> {
      final String clientID = record.key();
      if(record.value().getOriginalMessage().getMessageType() == MessageType.PUBREC) {
        final PubRecMessage recMessage = (PubRecMessage) record.value().getOriginalMessage();
        if (root.clients().containsKey(clientID)) {
          final Client client = root.clients().get(clientID);
          exactlyOnceClients.add(ImmutablePair.of(client.rec(recMessage.getPacketId()).getLeft(), client));
        }
        else{
          advanceService.asyncHandle(ImmutableTriple.of(clientID, recMessage.getPacketId(), Advance.REC_RECEIVED));
        }
      }
    });

    exactlyOnceClients.stream().map(releaseService::asyncHandle);
  }
}
