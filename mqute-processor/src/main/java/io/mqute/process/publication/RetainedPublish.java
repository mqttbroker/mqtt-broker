package io.mqute.process.publication;

import com.google.common.base.MoreObjects;
import com.google.common.collect.Lists;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.apache.commons.lang3.tuple.Pair;
import org.cliffc.high_scale_lib.NonBlockingHashMap;

import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.ConcurrentMap;

import io.mqute.msg.PublishMessage;

import static io.mqute.util.Topics.split;

/**
 * Created by huz on 7/29/16.
 */
public class RetainedPublish {

  private final ConcurrentMap<String, RetainedPublish> children;
  private volatile PublishMessage publishMessage;

  @JsonCreator
  public RetainedPublish(final @JsonProperty("children") ConcurrentMap<String, RetainedPublish> children,
                         final @JsonProperty("publishMessage") PublishMessage publishMessage) {

    this.children = MoreObjects.firstNonNull(children, new NonBlockingHashMap<>());
    this.publishMessage = publishMessage;
  }

  @JsonProperty
  public ConcurrentMap<String, RetainedPublish> children() {
    return children;
  }

  @JsonProperty
  public PublishMessage publishMessage() {
    return publishMessage;
  }

  public RetainedPublish locate(final String topicName) {
    if (topicName.isEmpty()) {
      return this;
    }

    final Pair<String, String> splitted = split(topicName);
    if (!children.containsKey(splitted.getLeft())) {
      children.put(splitted.getLeft(), new RetainedPublish(null, null));
    }
    return children.get(splitted.getLeft()).locate(splitted.getRight());
  }

  public Collection<PublishMessage> filter(final String topicFilter) {
    if (topicFilter.isEmpty()) {
      return publishMessage != null ? Collections.singletonList(publishMessage) : Collections.emptyList();
    }

    final Collection<PublishMessage> aggregates = Lists.newLinkedList();
    final Pair<String, String> splitted = split(topicFilter);
    switch (splitted.getLeft()) {
      case "#":
        if (publishMessage != null) {
          aggregates.add(publishMessage);
        }
        children.values().forEach(child -> aggregates.addAll(child.filter("#")));
        break;
      case "+":
        children.values().forEach(child -> aggregates.addAll(child.filter(splitted.getRight())));
        break;
      default:
        if(children.containsKey(splitted.getLeft())) {
          aggregates.addAll(children.get(splitted.getLeft()).filter(splitted.getRight()));
        }
        break;
    }

    return aggregates;
  }

  public void update(final PublishMessage publishMessage) {
    this.publishMessage = publishMessage;
  }
}
