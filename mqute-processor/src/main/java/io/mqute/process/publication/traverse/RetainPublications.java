package io.mqute.process.publication.traverse;

import io.mqute.kafka.KafkaTopics;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.util.Map;

import io.mqute.IMessageDispatcher;
import io.mqute.kafka.msg.KafkaMqttMessageWrapper;
import io.mqute.msg.SubscribeMessage;
import io.mqute.msg.enums.MessageType;
import io.mqute.process.Traverse;
import io.mqute.process.publication.RetainedPublish;

/**
 * Created by huz on 7/25/16.
 */
public class RetainPublications implements Traverse<KafkaMqttMessageWrapper, Map<Integer, RetainedPublish>> {

  private final IMessageDispatcher dispatcher;

  public RetainPublications(final IMessageDispatcher dispatcher) {
    this.dispatcher = dispatcher;
  }

  @Override
  public void traverse(final Consumer<String, KafkaMqttMessageWrapper> consumer,
                       final Iterable<ConsumerRecord<String, KafkaMqttMessageWrapper>> records,
                       final Map<Integer, RetainedPublish> retainedRoots) {

    //the publish will have specific targets instead!
    records.forEach(record -> {
      final KafkaMqttMessageWrapper subscribeWrapped = record.value();
      if (subscribeWrapped.getOriginalMessage().getMessageType() == MessageType.SUBSCRIBE) {
        final SubscribeMessage subscribeMessage = (SubscribeMessage) subscribeWrapped.getOriginalMessage();
        subscribeMessage.getSubscriptions().keySet().forEach(subscription -> {
          retainedRoots.values().forEach(root -> root.filter(subscription).forEach(publishMessage -> {
            dispatcher.dispatch(publishMessage, subscribeWrapped.getClientId(), KafkaTopics.ROUTED_PUBLISH_TOPIC, record.partition(), subscribeWrapped.getClientId());
          }));
        });
      }
    });
  }
}
