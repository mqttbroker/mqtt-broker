package io.mqute.process.publication.traverse;

import io.mqute.IMessageDispatcher;
import io.mqute.kafka.KafkaTopics;
import io.mqute.kafka.msg.KafkaMqttMessageWrapper;
import io.mqute.msg.PublishMessage;
import io.mqute.msg.enums.MessageType;
import io.mqute.process.Traverse;
import io.mqute.process.publication.Router;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Created by huz on 7/25/16.
 */
public class RouterPublications implements Traverse<KafkaMqttMessageWrapper, Map<Integer, Router>> {

  private static final Logger LOGGER = LoggerFactory.getLogger(RouterPublications.class);

  private final IMessageDispatcher dispatcher;

  public RouterPublications(final IMessageDispatcher dispatcher) {
    this.dispatcher = dispatcher;
  }

  @Override
  public void traverse(final Consumer<String, KafkaMqttMessageWrapper> consumer,
                       final Iterable<ConsumerRecord<String, KafkaMqttMessageWrapper>> records,
                       final Map<Integer, Router> routers) {

    records.forEach(record -> {

      final KafkaMqttMessageWrapper publishWrapped = record.value();
      if (publishWrapped.getOriginalMessage().getMessageType() == MessageType.PUBLISH) {
        final PublishMessage publishMessage = (PublishMessage) publishWrapped.getOriginalMessage();
        routers.forEach((partition, router) -> {
          if(router.route(publishMessage.getTopicName())){
            dispatcher.dispatch(publishMessage, publishWrapped.getClientId(), KafkaTopics.ROUTED_PUBLISH_TOPIC, partition);
          }
        });
      }
    });
  }
}
