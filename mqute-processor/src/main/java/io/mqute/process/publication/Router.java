package io.mqute.process.publication;

import java.util.Collection;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by huz on 8/30/16.
 */
public interface Router {

    ConcurrentMap<String, Router> children();

    Router locate(final String topicFilter);

    Collection<Router> filter(final String topic);

    boolean route(final String topic);

    int actives();

    int up();

    int down();
}
