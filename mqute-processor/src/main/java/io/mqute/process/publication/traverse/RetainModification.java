package io.mqute.process.publication.traverse;

import com.google.common.collect.Sets;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import io.mqute.kafka.msg.KafkaMqttMessageWrapper;
import io.mqute.msg.PublishMessage;
import io.mqute.msg.enums.MessageType;
import io.mqute.persistence.RetainedPublishDao;
import io.mqute.process.Traverse;
import io.mqute.process.publication.RetainedPublish;

/**
 * Created by huz on 7/25/16.
 */
public class RetainModification implements Traverse<KafkaMqttMessageWrapper, Map<Integer, RetainedPublish>> {

  private static final Logger LOGGER = LoggerFactory.getLogger(RetainModification.class);

  private final RetainedPublishDao retainedPublishDao;

  public RetainModification(final RetainedPublishDao retainedPublishDao) {
    this.retainedPublishDao = retainedPublishDao;
  }

  @Override
  public void traverse(final Consumer<String, KafkaMqttMessageWrapper> consumer,
                       final Iterable<ConsumerRecord<String, KafkaMqttMessageWrapper>> records,
                       final Map<Integer, RetainedPublish> retainedPublishes) {

    final Set<Integer> impacts = Sets.newHashSetWithExpectedSize(retainedPublishes.size());

    records.forEach(record -> {

      final int partition = record.partition();
      if (!retainedPublishes.containsKey(partition)) {
        synchronized (retainedPublishes) {
          if (!retainedPublishes.containsKey(partition)) {
            try {
              retainedPublishes.put(partition, retainedPublishDao.get(String.valueOf(partition)).get(10, TimeUnit.SECONDS));
            } catch (Exception e) {
              LOGGER.error("[retained] failed to load from persistence due to: {}", e);
            }
          }
        }
      }

      final KafkaMqttMessageWrapper publishWrapped = record.value();
      if (!shouldIgnore(publishWrapped) && publishWrapped.getOriginalMessage().getMessageType() == MessageType.PUBLISH) {
        final PublishMessage publishMessage = (PublishMessage) publishWrapped.getOriginalMessage();
        if (publishMessage.isRetain()) {
          impacts.add(partition);
          retainedPublishes.get(partition).locate(publishMessage.getTopicName()).update(publishMessage);
        }
      }
    });

    impacts.forEach(partition -> retainedPublishDao.update(String.valueOf(partition), retainedPublishes.get(partition)));
  }

  protected static boolean shouldIgnore(final KafkaMqttMessageWrapper wrapper) {
    //i sent it out myself
    return wrapper.getTargets().length > 0;
  }
}
