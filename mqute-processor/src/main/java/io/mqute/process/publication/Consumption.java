package io.mqute.process.publication;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Maps;
import io.mqute.kafka.KafkaTopics;
import io.mqute.kafka.consumer.MquteKafkaConsumerFactory;
import io.mqute.kafka.msg.KafkaMqttMessageWrapper;
import io.mqute.persistence.RetainedPublishDao;
import io.mqute.persistence.RouterDao;
import io.mqute.process.kafka.KafkaConsumerThread;
import io.mqute.process.kafka.PacedKafkaConsumer;
import io.mqute.process.publication.traverse.RetainModification;
import io.mqute.process.publication.traverse.RetainPublications;
import io.mqute.process.publication.traverse.RouterModification;
import io.mqute.process.publication.traverse.RouterPublications;
import io.netty.util.Timeout;
import io.netty.util.Timer;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;

import java.util.*;
import java.util.concurrent.*;

/**
 * Created by huz on 7/20/16.
 */
public class Consumption {

  private final MquteKafkaConsumerFactory consumerFactory;
  private final Timer timer;
  private final RetainedPublishDao retainedPublishDao;
  private final RouterDao routerDao;
  private final Map<Integer, RetainedPublish> retains;
  private final Map<Integer, Router> routers;
  private final RetainModification retainModification;
  private final RetainPublications retainPublications;
  private final RouterModification routerModification;
  private final RouterPublications routerPublications;

  private ExecutorService executors = Executors.newCachedThreadPool();

  public Consumption(final MquteKafkaConsumerFactory consumerFactory,
                     final Timer timer,
                     final RetainedPublishDao retainedPublishDao,
                     final RouterDao routerDao,
                     final RetainModification retainModification,
                     final RetainPublications retainPublications,
                     final RouterModification routerModification,
                     final RouterPublications routerPublications) {

    this.retains = Maps.newConcurrentMap();
    this.routers = Maps.newConcurrentMap();

    this.consumerFactory = consumerFactory;
    this.timer = timer;
    this.retainedPublishDao = retainedPublishDao;
    this.routerDao = routerDao;
    this.retainModification = retainModification;
    this.retainPublications = retainPublications;
    this.routerModification = routerModification;
    this.routerPublications = routerPublications;
  }

  public void consume() {

    final KafkaConsumer<String, KafkaMqttMessageWrapper> publishConsumer = consumerFactory.create("publish-consumer", "publication-processor");
    publishConsumer.subscribe(Collections.singletonList(KafkaTopics.PUBLISH_TOPIC), new ConsumerRebalanceListener() {

      final LoadingCache<TopicPartition, ConcurrentNavigableMap<Long, List<ConsumerRecord<String, KafkaMqttMessageWrapper>>>> paces = CacheBuilder.newBuilder()
              .maximumSize(1024).build(new CacheLoader<TopicPartition, ConcurrentNavigableMap<Long, List<ConsumerRecord<String, KafkaMqttMessageWrapper>>>>() {

        @Override
        public ConcurrentNavigableMap<Long, List<ConsumerRecord<String, KafkaMqttMessageWrapper>>> load(TopicPartition key) throws Exception {
          return new ConcurrentSkipListMap<>();
        }
      });

      final Map<TopicPartition, Timeout> delayedCancellations = Maps.newConcurrentMap();
      final Map<TopicPartition, Future<?>> runnings = Maps.newConcurrentMap();

      @Override
      public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
        partitions.forEach(p -> {
          delayedCancellations.put(p, timer.newTimeout(timeout -> {
            Optional.ofNullable(runnings.remove(p)).ifPresent(r -> r.cancel(true));
          }, 1L, TimeUnit.MINUTES));
        });
      }

      @Override
      public void onPartitionsAssigned(Collection<TopicPartition> partitions) {

        partitions.forEach(p -> Optional.ofNullable(delayedCancellations.remove(p)).ifPresent(Timeout::cancel));
        partitions.stream().filter(p -> !runnings.containsKey(p)).forEach(p -> {
          retainedPublishDao.get(String.valueOf(p.partition())).thenAccept(retainedPublish -> {
            synchronized (retains) {
              if(!retains.containsKey(p.partition())) {
                retains.put(p.partition(), retainedPublish);
              }
            }
            routerDao.get(String.valueOf((p.partition()))).thenAccept(router -> {
              synchronized (routers){
                if(routers.containsKey(p.partition())) {
                  routers.put(p.partition(), router);
                }
              }
              final Consumer<String, KafkaMqttMessageWrapper> subscribeConsumer = new PacedKafkaConsumer<>(paces,
                      Executors.newSingleThreadExecutor(),
                      consumerFactory.create("subscription-consumer", "publication-processor-" + p.partition()));
              subscribeConsumer.subscribe(Collections.singletonList(KafkaTopics.GENERAL_TOPIC));
              runnings.put(p, executors.submit(new KafkaConsumerThread<>("publish-consume-sub", subscribeConsumer, msgs -> {
                routerModification.traverse(subscribeConsumer, msgs.records(KafkaTopics.GENERAL_TOPIC), routers);
                retainPublications.traverse(subscribeConsumer, msgs.records(KafkaTopics.GENERAL_TOPIC), retains);
                return null;
              })));
            });
          });
        });
      }
    });

    executors.submit(new KafkaConsumerThread<>("publish-consume-pub", publishConsumer, msgs -> {
      retainModification.traverse(publishConsumer, msgs.records(KafkaTopics.PUBLISH_TOPIC), retains);
      routerPublications.traverse(publishConsumer, msgs.records(KafkaTopics.PUBLISH_TOPIC), routers);
      return null;
    }));
  }
}
