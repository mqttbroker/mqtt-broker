package io.mqute.process.publication.traverse;

import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import io.mqute.kafka.msg.KafkaMqttMessageWrapper;
import io.mqute.msg.SubscribeMessage;
import io.mqute.msg.UnsubscribeMessage;
import io.mqute.persistence.RouterDao;
import io.mqute.process.Traverse;
import io.mqute.process.publication.Router;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Set;

/**
 * Created by huz on 7/25/16.
 */
public class RouterModification implements Traverse<KafkaMqttMessageWrapper, Map<Integer, Router>> {

  private static final Logger LOGGER = LoggerFactory.getLogger(RouterModification.class);

  private final RouterDao routerDao;

  public RouterModification(final RouterDao routerDao) {
    this.routerDao = routerDao;
  }

  @Override
  public void traverse(final Consumer<String, KafkaMqttMessageWrapper> consumer,
                       final Iterable<ConsumerRecord<String, KafkaMqttMessageWrapper>> records,
                       final Map<Integer, Router> routers) {

    final Set<Integer> impacts = Sets.newHashSetWithExpectedSize(routers.size());

    records.forEach(record -> {

      final int partition = record.partition();
      final Router router = routers.get(partition);

      final KafkaMqttMessageWrapper subscribeWrapped = record.value();
      switch (subscribeWrapped.getOriginalMessage().getMessageType()) {
        case SUBSCRIBE:
          final SubscribeMessage subscribeMessage = (SubscribeMessage) record.value().getOriginalMessage();
          LOGGER.info("[pub][modification] subscribe {}", subscribeMessage.getSubscriptions());
          subscribeMessage.getSubscriptions().forEach((topicFilter, qoSType) -> {
            if (Strings.isNullOrEmpty(topicFilter)) {
              LOGGER.warn("[pub][modification] ignored empty subscription");
            } else {
              router.locate(topicFilter).up();
            }
          });
          impacts.add(partition);
          break;
        case UNSUBSCRIBE:
          final UnsubscribeMessage unsubscribeMessage = (UnsubscribeMessage) record.value().getOriginalMessage();
          LOGGER.info("[pub][modification] unsubscribe {}", unsubscribeMessage.getTopicFilters());
          unsubscribeMessage.getTopicFilters().forEach(topicFilter -> {
            if (Strings.isNullOrEmpty(topicFilter)) {
              LOGGER.warn("[pub][modification] ignored empty subscription");
            } else {
              router.locate(topicFilter).down();
            }
          });
          impacts.add(partition);
          break;
      }
    });

    impacts.forEach(partition -> routerDao.update(String.valueOf(partition), routers.get(partition)));
  }
}
