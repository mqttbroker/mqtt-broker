package io.mqute.process.publication;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.tuple.Pair;
import org.cliffc.high_scale_lib.NonBlockingHashMap;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

import static io.mqute.util.Topics.split;

/**
 * Created by huz up 8/30/16.
 */
public class BaseRouter implements Router {

    public static final int DEEPEST_ROUTER = 3;

    private final ConcurrentMap<String, Router> children;
    private final AtomicInteger actives;

    @JsonCreator
    public BaseRouter(final @JsonProperty("children") ConcurrentMap<String, Router> children,
                      final @JsonProperty("actives") int actives) {

        this.children = MoreObjects.firstNonNull(children, new NonBlockingHashMap<>());
        this.actives = new AtomicInteger(actives);
    }

    @Override
    public int actives() {
        return actives.get();
    }

    @Override
    public ConcurrentMap<String, Router> children() {
        return children;
    }

    @Override
    public Router locate(final String topicFilter) {
        return locate(this, topicFilter, DEEPEST_ROUTER);
    }

    protected static Router locate(final Router router, final String topicFilter, final int depth) {

        if (Strings.isNullOrEmpty(topicFilter) || depth == 0) {
            return router;
        }

        final Pair<String, String> headAndTail = split(topicFilter);
        final String head = headAndTail.getLeft();
        if ("#".equals(head) && !Strings.isNullOrEmpty(headAndTail.getRight())) {
            throw new IllegalArgumentException("[locate] topicFilter is invalid, cannot have further filter after # " + topicFilter);
        }

        final ConcurrentMap<String, Router> children = router.children();
        Router child;
        if(!children.containsKey(head)){
            final Router birth = new BaseRouter(null, 0);
            child = MoreObjects.firstNonNull(children.putIfAbsent(head, birth), birth);
        }
        else{
            child = children.get(head);
        }

        return locate(child, headAndTail.getRight(), depth - 1);
    }

    @Override
    public Collection<Router> filter(final String topic) {

        return filter(this, topic, DEEPEST_ROUTER);
    }

    protected static Collection<Router> filter(final Router router, final String topic, final int depth) {
        if (Strings.isNullOrEmpty(topic) || depth == 0) {
            return Collections.singletonList(router);
        }

        final List<Router> aggregates = Lists.newLinkedList();
        final ConcurrentMap<String, Router> children = router.children();

        final Router any = children.get("#");
        if(any != null) {
            aggregates.add(any);//there cannot be any further children after #
        }

        final Pair<String, String> headAndTail = split(topic);
        final Router one = children.get("+");
        if(one != null){
            aggregates.addAll(filter(one, headAndTail.getRight(), depth - 1));
        }

        final Router found = children.get(headAndTail.getLeft());
        if(found != null){
            aggregates.addAll(filter(found, headAndTail.getRight(), depth - 1));
        }

        return aggregates;
    }

    @Override
    public boolean route(final String topic) {
        return filter(topic).stream().anyMatch(r -> r.actives() > 0);
    }

    @Override
    public int up() {
        return actives.incrementAndGet();
    }

    @Override
    public int down() {
        return actives.decrementAndGet();
    }

    @Override
    public String toString() {
        return "BaseRouter{" +
                ", actives=" + actives +
                ", children=" + children +
                '}';
    }
}
