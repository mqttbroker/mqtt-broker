package io.mqute.process.batch;

import io.mqute.IMessageDispatcher;
import io.mqute.edge.Client;
import io.mqute.msg.UnsubscribeMessage;
import io.mqute.persistence.BacklogDao;
import io.mqute.process.subscription.Subscription;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.stream.Collectors;

/**
 * Created by huz on 7/22/16.
 */
public class CleanupService extends AbstractBatchService<Pair<Client, Collection<Subscription>>> {

    public CleanupService(final IMessageDispatcher dispatcher,
                          final BacklogDao backlogDao,
                          final Executor batchSvcExecutor,
                          final int parallelism) {

        super("cleanup", batchSvcExecutor, cleanups -> CompletableFuture.allOf(cleanups.stream().map(pair -> {

            final Client client = pair.getLeft();
            final List<String> topicFilters = pair.getRight().stream().map(Subscription::topicFilter).collect(Collectors.toList());
            //unsubscribe on behalf of the disconnecting client or clean session connect client
            //this is mainly for publish processor to purge its routing map
            //disconnected none persistent session client or reconnecting clean session client will give up their previous subscriptions
            //and that must be reflected upon the routing map of each publish processor
            return dispatcher.dispatch(new UnsubscribeMessage((short) client.nextPacketID(), topicFilters), client.clientID(), client.clientID())
                    .thenCombine(backlogDao.remove(client.clientID()), (sent, removed) -> removed);
            //TODO make sure this won't conflict with reconnect subscribes
        }).collect(Collectors.toList()).toArray(new CompletableFuture[cleanups.size()])), parallelism);
    }
}
