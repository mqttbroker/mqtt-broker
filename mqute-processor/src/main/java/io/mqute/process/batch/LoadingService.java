package io.mqute.process.batch;

import io.mqute.msg.PubRelMessage;
import io.mqute.persistence.AdvanceDao;
import io.mqute.process.Advance;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.stream.Collectors;

import io.mqute.edge.Client;
import io.mqute.edge.State;
import io.mqute.persistence.BacklogDao;

/**
 * Created by huz on 7/24/16.
 */
public class LoadingService extends AbstractBatchService<Client> {

  public LoadingService(final BacklogDao backlogDao,
                        final AdvanceDao advanceDao,
                        final PublishService publishService,
                        final ReleaseService releaseService,
                        final Executor batchSvcExecutor,
                        final int parallelism) {

    super("loading", batchSvcExecutor, clients -> loadAll(publishService, releaseService, backlogDao, advanceDao, clients), parallelism);
  }

  protected static CompletableFuture<Void> load(final PublishService publishService,
                                                final ReleaseService releaseService,
                                                final BacklogDao backlogDao,
                                                final AdvanceDao advanceDao,
                                                final Client client) {

    return backlogDao.take(client.clientID(), 32).thenCompose(publishMessages -> {
      final List<CompletableFuture<Void>> publishes = publishMessages.stream().map(publishMessage -> advanceDao.get(client.clientID() + ":" + publishMessage.getPacketId()).thenCompose(advanced -> {
          if(Advance.ACK_RECEIVED == advanced || Advance.COMP_RECEIVED == advanced){
            //no need to load any more
            return CompletableFuture.completedFuture(null);
          }
          else if(Advance.REC_RECEIVED == advanced){
            return releaseService.asyncHandle(ImmutablePair.of(new PubRelMessage(publishMessage.getPacketId()), client));
          }
          else{
            return publishService.asyncHandle(ImmutableTriple.of(publishMessage, client, publishMessage.getQos()));
          }
        })).collect(Collectors.toList());

      if (publishes.isEmpty()) {
        return CompletableFuture.completedFuture(null);
      } else {
        return CompletableFuture.allOf(publishes.toArray(new CompletableFuture[publishes.size()])).thenApply(p -> {
          if (client.state() == State.SLOW && publishMessages.size() < 32) {
            client.transit(State.QUEUED);
          }
          return p;
        });
      }
    });
  }

  protected static CompletableFuture<Void> loadAll(final PublishService publishService,
                                                   final ReleaseService releaseService,
                                                   final BacklogDao backlogDao,
                                                   final AdvanceDao advanceDao,
                                                   final Collection<Client> clients) {

    return CompletableFuture.allOf(clients.stream().map(client -> load(publishService, releaseService, backlogDao, advanceDao, client)).collect(Collectors.toList()).toArray(new CompletableFuture[clients.size()]));
  }
}

