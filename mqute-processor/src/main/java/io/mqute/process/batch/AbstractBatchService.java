package io.mqute.process.batch;

import com.google.common.collect.Lists;
import com.google.common.collect.MapMaker;
import com.google.common.collect.Queues;
import io.mqute.util.Threads;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.*;
import java.util.function.Function;

/**
 * Created by huz on 7/21/16.
 * <p>
 * to handle persistence, message sending in batch mode
 */
public abstract class AbstractBatchService<ID> {

  private static final Logger LOGGER = LoggerFactory.getLogger(AbstractBatchService.class);

  protected final String threadGroup;
  protected final BlockingQueue<ID> ids;
  protected final Map<ID, CompletableFuture<Void>> progress;
  protected final Executor batchSvcExecutor;
  protected final Function<Collection<ID>, CompletableFuture<Void>> handle;

  public AbstractBatchService(final String threadGroup,
                              final Executor batchSvcExecutor,
                              final Function<Collection<ID>, CompletableFuture<Void>> handle,
                              final int parallelism) {

    this.threadGroup = threadGroup;
    this.batchSvcExecutor = batchSvcExecutor;
    this.handle = handle;
    this.ids = new LinkedTransferQueue<>();//TODO migrate to LMAX Disruptor iff becomes a perf bottleneck
    this.progress = new MapMaker().weakValues().makeMap();

    for (int worker = 0; worker < parallelism; worker += 1) {
      batchSvcExecutor.execute(new HandleWorker());
    }
  }

  public CompletableFuture<Void> asyncHandle(final ID id) {

    //duplicate query
    final CompletableFuture<Void> exists = progress.get(id);
    if (exists != null) {
      return exists;
    }

    final CompletableFuture<Void> pending = new CompletableFuture<>();
    progress.put(id, pending);
    ids.offer(id);

    return pending;
  }

  protected int getBatchSize() {
    return 256;
  }

  protected long getBatchWindowInMillis() {
    return 128L;
  }

  protected class HandleWorker implements Runnable {

    @Override
    public void run() {

      Thread.currentThread().setName(Threads.nextThreadName(threadGroup));

      while (true) {

        final List<ID> batch = Lists.newArrayListWithCapacity(getBatchSize());
        try {
          Queues.drain(ids, batch, getBatchSize(), getBatchWindowInMillis(), TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
          LOGGER.error("[batch] draining failed due to: {}", e);
          continue;
        }

        if (!batch.isEmpty()) {
          try {
            handle.apply(batch).whenComplete((vs, ex) -> {
              LOGGER.info("[batch] {} completed", batch);
              batch.forEach(id -> Optional.ofNullable(progress.remove(id)).ifPresent(completable -> {
                LOGGER.info("[batch] {} complete individual:{}", batch, id);
                completable.complete(null);
              }));
            });
          } catch (Exception e) {
            LOGGER.error("[batch] handle failed due to: {}", e);
          }
        }
      }
    }
  }
}
