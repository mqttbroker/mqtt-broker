package io.mqute.process.batch;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

import io.mqute.edge.Client;
import io.mqute.msg.PubRelMessage;
import io.mqute.msg.internal.OutgoingMessageWrapper;
import io.mqute.util.Endpoints;

/**
 * Created by huz on 7/23/16.
 */
public class ReleaseService extends AbstractBatchService<Pair<PubRelMessage, Client>> {

  private static final Logger LOGGER = LoggerFactory.getLogger(ReleaseService.class);

  public ReleaseService(final Executor batchSvcExecutor,
                        final int parallelism) {

    super("release", batchSvcExecutor, batchedPublishes -> {

      final Multimap<String, Client> groupByEndpoints = Multimaps.newListMultimap(Maps.newHashMap(), Lists::newLinkedList);
      final Multimap<Client, PubRelMessage> groupByClients = Multimaps.newListMultimap(Maps.newHashMap(), Lists::newLinkedList);
      batchedPublishes.forEach(pair -> {
        final Client client = pair.getRight();
        groupByEndpoints.put(client.endpoint(), client);
        groupByClients.put(client, pair.getLeft());
      });

      groupByEndpoints.asMap().forEach((endpoint, clients) ->
          clients.forEach(client ->
              groupByClients.asMap().get(client).forEach(pubRelMessage -> {
                try {
                  Endpoints.get(endpoint).send(new OutgoingMessageWrapper(pubRelMessage, client.clientID()));
                } catch (Exception e) {
                  LOGGER.error("[release] failed to send to edge server due to: {}", e);
                }
              })
          )
      );

      return CompletableFuture.completedFuture(null);
    }, parallelism);
  }
}
