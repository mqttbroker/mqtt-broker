package io.mqute.process.batch;

import com.google.common.collect.Maps;
import io.mqute.persistence.AdvanceDao;
import io.mqute.process.Advance;
import org.apache.commons.lang3.tuple.Triple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.stream.Collectors;

/**
 * Created by huz on 7/22/16.
 */
public class AdvanceService extends AbstractBatchService<Triple<String, Short, Advance>> {

    public AdvanceService(final AdvanceDao advanceDao,
                          final Executor batchSvcExecutor,
                          final int parallelism) {

        super("advance", batchSvcExecutor, advances -> {

            final Map<String, Advance> bulks = Maps.newHashMapWithExpectedSize(advances.size());
            advances.forEach(triple -> bulks.put(triple.getLeft() + ":" + triple.getMiddle(), triple.getRight()));

            return advanceDao.offer(bulks);
        }, parallelism);
    }
}
