package io.mqute.process.batch;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.stream.Collectors;

import io.mqute.msg.PublishMessage;
import io.mqute.persistence.BacklogDao;

/**
 * Created by huz on 7/22/16.
 */
public class BacklogService extends AbstractBatchService<Pair<PublishMessage, String>> {

  private static final Logger LOGGER = LoggerFactory.getLogger(BacklogService.class);

  public BacklogService(final BacklogDao backlogDao,
                        final Executor batchSvcExecutor,
                        final int parallelism) {

    super("backlog", batchSvcExecutor, backlogs -> groupBacklog(backlogDao, backlogs), parallelism);
  }

  public static CompletableFuture<Void> groupBacklog(final BacklogDao backlogDao,
                                                     final Collection<Pair<PublishMessage, String>> backlogs) {

    final Multimap<String, PublishMessage> groupByClientIDs = Multimaps.newListMultimap(Maps.newHashMap(), Lists::newLinkedList);
    backlogs.forEach(pair -> groupByClientIDs.put(pair.getRight(), pair.getLeft()));

    return CompletableFuture.allOf(groupByClientIDs.asMap().entrySet().stream().map(entry -> {
      final Collection<PublishMessage> publishMessages = entry.getValue();
      LOGGER.info("[sub][backlog] saving:{} with {} publishes", entry.getKey(), publishMessages.size());
      return backlogDao.offer(entry.getKey(), publishMessages.toArray(new PublishMessage[publishMessages.size()]));
    }).collect(Collectors.toList()).toArray(new CompletableFuture[groupByClientIDs.keySet().size()]));
  }
}
