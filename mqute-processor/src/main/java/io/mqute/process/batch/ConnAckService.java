package io.mqute.process.batch;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

import io.mqute.edge.CallbackUdpClient;
import io.mqute.edge.Client;
import io.mqute.msg.ConnAckMessage;
import io.mqute.msg.internal.OutgoingMessageWrapper;
import io.mqute.util.Endpoints;

/**
 * Created by huz on 7/23/16.
 */
public class ConnAckService extends AbstractBatchService<Triple<ConnAckMessage, Client, Integer>> {

  private static final Logger LOGGER = LoggerFactory.getLogger(ConnAckService.class);

  public ConnAckService(final Executor batchSvcExecutor,
                        final int parallelism) {

    super("connack", batchSvcExecutor, batchedConnAcks -> {

      final Multimap<String, Client> groupByEndpoints = Multimaps.newListMultimap(Maps.newHashMap(), Lists::newLinkedList);
      final Multimap<Client, Pair<ConnAckMessage, Integer>> groupByClients = Multimaps.newListMultimap(Maps.newHashMap(), Lists::newLinkedList);
      batchedConnAcks.forEach(triple -> {
        final Client client = triple.getMiddle();
        groupByEndpoints.put(client.endpoint(), client);
        groupByClients.put(client, ImmutablePair.of(triple.getLeft(), triple.getRight()));
      });

      LOGGER.info("[connack] batch processing:{}", groupByEndpoints);
      final List<CompletableFuture<Void>> waitFors = Lists.newLinkedList();
      groupByEndpoints.asMap().forEach((endpoint, clients) -> {
        final CallbackUdpClient sender = Endpoints.get(endpoint);
        clients.forEach(client -> groupByClients.asMap().get(client).forEach(pair -> {
          try {
            LOGGER.info("[connack] send ack to:{} for client:{}", endpoint, client.clientID());
            sender.send(new OutgoingMessageWrapper(pair.getLeft(), client.clientID(), pair.getRight()));
          } catch (Exception e) {
            LOGGER.error("[connack] failed to send a publish message due to: {}", e);
          }
        }));
      });

      return CompletableFuture.allOf(waitFors.toArray(new CompletableFuture[waitFors.size()]));
    }, parallelism);
  }
}
