package io.mqute.process.batch;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

import io.mqute.edge.CallbackUdpClient;
import io.mqute.edge.Client;
import io.mqute.edge.State;
import io.mqute.msg.PublishMessage;
import io.mqute.msg.enums.QoSType;
import io.mqute.msg.internal.OutgoingMessageWrapper;
import io.mqute.util.Endpoints;
import io.netty.util.HashedWheelTimer;
import io.netty.util.Timeout;

/**
 * Created by huz on 7/23/16.
 */
public class PublishService extends AbstractBatchService<Triple<PublishMessage, Client, QoSType>> {

  private static final Logger LOGGER = LoggerFactory.getLogger(PublishService.class);
  private static final HashedWheelTimer TIMEOUTS = new HashedWheelTimer(256, TimeUnit.MILLISECONDS);

  public PublishService(final BacklogService backlogService,
                        final Executor batchSvcExecutor,
                        final int parallelism) {

    super("publish", batchSvcExecutor, batchedPublishes -> {

      final Multimap<String, Client> groupByEndpoints = Multimaps.newListMultimap(Maps.newHashMap(), Lists::newLinkedList);
      final Multimap<Client, Pair<PublishMessage, QoSType>> groupByClients = Multimaps.newListMultimap(Maps.newHashMap(), Lists::newLinkedList);
      batchedPublishes.forEach(triple -> {
        final Client client = triple.getMiddle();
        groupByEndpoints.put(client.endpoint(), client);
        groupByClients.put(client, ImmutablePair.of(triple.getLeft(), triple.getRight()));
      });

      final Multimap<String, CompletableFuture<Void>> waitFors = Multimaps.newListMultimap(Maps.newHashMap(), Lists::newLinkedList);
      groupByEndpoints.asMap().forEach((endpoint, clients) -> {
        final Map<PublishMessage, String> publishToClients = Maps.newHashMap();
        waitFors.putAll(endpoint, Lists.newArrayList(Iterables.concat(clients.stream().map(client -> {
          final Collection<Pair<PublishMessage, QoSType>> needToPublishes = groupByClients.asMap().get(client);
          final List<Pair<PublishMessage, CompletableFuture<Void>>> actualPublishes = needToPublishes.stream().map(pair -> client.pub(pair.getLeft(), pair.getRight())).collect(Collectors.toList());
          final List<PublishMessage> messages = Lists.transform(actualPublishes, Pair::getLeft);
          final List<CompletableFuture<Void>> guardedPublishes = Lists.transform(actualPublishes, pair -> {

            final CompletableFuture<Void> original = pair.getRight();
            if (original.isDone() || original.isCompletedExceptionally()) {
              LOGGER.info("[publish] immediately completed most once publication");
              return original;
            }

            final Timeout timeout = TIMEOUTS.newTimeout(t -> {
              LOGGER.error("[publish] recover from publish timeout");
              backlogService.asyncHandle(ImmutablePair.of(pair.getLeft(), client.clientID())).thenAccept(backlog -> client.transit(State.SLOW));
              original.completeExceptionally(new TimeoutException("timeout of client: " + client.clientID()));
            }, 4, TimeUnit.SECONDS);
            return original.thenApply(v -> {
              timeout.cancel();
              return v;
            }).exceptionally(ex -> null);
          });
          //associate the PublishMessage with the intended clientID for edge node to understand
          messages.forEach(message -> publishToClients.put(message, client.clientID()));
          return guardedPublishes;
        }).collect(Collectors.toList()))));

        final CallbackUdpClient sender = Endpoints.get(endpoint);
        if (sender == null) {
          LOGGER.error("[sender] not available for: {}, fail pending futures: {}", endpoint, waitFors.get(endpoint).size());
        } else {
          publishToClients.forEach((publishMessage, client) -> {
            try {
              sender.send(new OutgoingMessageWrapper(publishMessage, client));
            } catch (Exception e) {
              LOGGER.error("[publish] failed to send a publish message due to: {}", e);
            }
          });
        }
      });

      return CompletableFuture.allOf(waitFors.values().toArray(new CompletableFuture[waitFors.values().size()])).whenComplete((v, ex) -> {
        LOGGER.info("[publish] iteration completed with exception: {}", ex);
      });
    }, parallelism);
  }
}
