package io.mqute.process;

/**
 * Created by huz on 10/25/16.
 */
public enum Advance {

    ACK_RECEIVED,
    REC_RECEIVED,
    COMP_RECEIVED
}
