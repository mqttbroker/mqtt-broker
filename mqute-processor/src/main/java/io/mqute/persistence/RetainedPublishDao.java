package io.mqute.persistence;

import com.google.protobuf.InvalidProtocolBufferException;

import org.cliffc.high_scale_lib.NonBlockingHashMap;

import java.util.concurrent.CompletableFuture;

import io.mqute.process.publication.RetainedPublish;
import io.mqute.util.Protobufs;

import static io.mqute.util.Protobufs.persist;

/**
 * Created by huz on 8/3/16.
 */
public class RetainedPublishDao implements IAsyncDao<RetainedPublish> {

  private static final String RETAINED_PUBLISHES_REDIS_KEY = "retained-snapshot";

  private final PersistenceService service;
  private final Codec<RetainedPublish> codec;

  public RetainedPublishDao(PersistenceService service) {
    this.service = service;
    codec = new RetainedPublishCodec();
  }

  @Override
  public CompletableFuture<Boolean> update(final String partition,
                                           final RetainedPublish retainedPublish) {
    return service.update(key(partition), codec, retainedPublish);
  }

  @Override
  public CompletableFuture<Boolean> remove(final String partition) {
    return service.remove(key(partition));
  }

  @Override
  public CompletableFuture<RetainedPublish> get(final String partition) {
    return service.get(key(partition), codec).exceptionally(e ->
        new RetainedPublish(new NonBlockingHashMap<>(), null)
    ).toCompletableFuture();
  }

  private String key(String partition) {
    return RETAINED_PUBLISHES_REDIS_KEY + ":" + partition;
  }

  private static class RetainedPublishCodec implements Codec<RetainedPublish> {
    @Override
    public byte[] encode(RetainedPublish data) {
      return persist(data).toByteArray();
    }

    @Override
    public RetainedPublish decode(byte[] bytes) {
      try {
        return Protobufs.load(RetainedPublishOuterClass.RetainedPublish.parseFrom(bytes));
      } catch (InvalidProtocolBufferException e) {
        throw new RuntimeException("Failed to decode RetainedPublish", e);
      }
    }
  }
}
