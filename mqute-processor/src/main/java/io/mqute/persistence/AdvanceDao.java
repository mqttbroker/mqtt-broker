package io.mqute.persistence;

import io.mqute.process.Advance;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * Created by huz on 8/3/16.
 */
public class AdvanceDao implements IAsyncQueueDao<Advance> {

    private final PersistenceService service;
    private final Codec<Advance> codec;

    public AdvanceDao(final PersistenceService service) {
        this.service = service;
        codec = new ProgressCodec();
    }

    @Override
    public CompletableFuture<Long> offer(String key, Advance... data) {
        return service.offer(key, codec, data);
    }

    @Override
    public CompletableFuture<Void> offer(Map<String, Advance> data) {
        return service.offer(data, codec);
    }

    @Override
    public CompletableFuture<Boolean> remove(final String key) {
        return service.remove(key);
    }

    public CompletableFuture<Advance> get(String key) {
        return service.get(key, codec);
    }

    @Override
    public CompletableFuture<List<Advance>> take(String key, int length) {
        return service.take(key, codec, length);
    }

    private static class ProgressCodec implements Codec<Advance> {

        @Override
        public byte[] encode(Advance data) {
            return new byte[]{(byte) data.ordinal()};
        }

        @Override
        public Advance decode(byte[] bytes) {
            return Advance.values()[(int) bytes[0]];
        }
    }
}
