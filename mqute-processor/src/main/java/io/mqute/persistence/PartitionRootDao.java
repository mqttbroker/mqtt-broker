package io.mqute.persistence;

import com.google.protobuf.InvalidProtocolBufferException;

import org.cliffc.high_scale_lib.NonBlockingHashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.concurrent.CompletableFuture;

import io.mqute.process.subscription.PartitionRoot;
import io.mqute.util.Protobufs;

import static io.mqute.util.Protobufs.persist;

/**
 * Created by huz on 8/3/16.
 */
public class PartitionRootDao implements IAsyncDao<PartitionRoot> {

  private static final Logger LOGGER = LoggerFactory.getLogger(PartitionRootDao.class);
  private static final String SUBSCRIPTIONS_REDIS_KEY = "subscription-snapshot";

  private final PersistenceService service;
  private final Codec<PartitionRoot> codec;

  public PartitionRootDao(PersistenceService service) {
    this.service = service;
    codec = new PartitionRootCodec();
  }


  @Override
  public CompletableFuture<Boolean> update(final String partition, final PartitionRoot root) {
    return service.update(key(partition), codec, root);
  }

  @Override
  public CompletableFuture<Boolean> remove(final String partition) {
    return service.remove(key(partition));
  }

  @Override
  public CompletableFuture<PartitionRoot> get(final String partition) {
    return service.get(key(partition), codec).exceptionally(e -> {
      LOGGER.error("[dao][partitionroot] read:{} failed due to:{}", partition, e);
      return new PartitionRoot(Integer.parseInt(partition), new NonBlockingHashMap<>(),
          new NonBlockingHashMap<>(), Collections.emptySet());
    }).toCompletableFuture();
  }

  private String key(String partition) {
    return SUBSCRIPTIONS_REDIS_KEY + ":" + partition;
  }

  private static class PartitionRootCodec implements Codec<PartitionRoot> {
    @Override
    public byte[] encode(PartitionRoot data) {
      return persist(data).toByteArray();
    }

    @Override
    public PartitionRoot decode(byte[] bytes) {
      try {
        return Protobufs.load(PartitionRootOuterClass.PartitionRoot.parseFrom(bytes));
      } catch (InvalidProtocolBufferException e) {
        throw new RuntimeException("Failed to decode PartitionRoot", e);
      }
    }
  }
}
