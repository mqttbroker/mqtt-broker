package io.mqute.persistence;

import com.google.protobuf.InvalidProtocolBufferException;

import org.cliffc.high_scale_lib.NonBlockingHashMap;

import java.util.concurrent.CompletableFuture;

import io.mqute.process.keepalive.Clients;
import io.mqute.util.Protobufs;

import static io.mqute.util.Protobufs.persist;

/**
 * Created by huz on 8/3/16.
 */
public class ClientsDao implements IAsyncDao<Clients> {

  private static final String CLIENTS_REDIS_KEY = "clients-snapshot";

  private final PersistenceService service;
  private final Codec<Clients> codec;

  public ClientsDao(final PersistenceService service) {
    this.service = service;
    codec = new ClientsCodec();
  }

  @Override
  public CompletableFuture<Boolean> update(final String partition, final Clients clients) {
    return service.update(key(partition), codec, clients);
  }

  @Override
  public CompletableFuture<Boolean> remove(final String partition) {
    return service.remove(key(partition));
  }

  @Override
  public CompletableFuture<Clients> get(final String partition) {
    return service.get(key(partition), codec).exceptionally(e ->
        new Clients(new NonBlockingHashMap<>(), new NonBlockingHashMap<>())
    ).toCompletableFuture();
  }

  private String key(String partition) {
    return CLIENTS_REDIS_KEY + ":" + partition;
  }

  private static class ClientsCodec implements Codec<Clients> {
    @Override
    public byte[] encode(Clients data) {
      return persist(data).toByteArray();
    }

    @Override
    public Clients decode(byte[] bytes) {
      try {
        return Protobufs.load(ClientsOuterClass.Clients.parseFrom(bytes));
      } catch (InvalidProtocolBufferException e) {
        throw new RuntimeException("Failed to decode Clients", e);
      }
    }
  }
}
