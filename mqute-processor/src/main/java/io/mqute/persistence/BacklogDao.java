package io.mqute.persistence;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import io.mqute.msg.PublishMessage;
import io.mqute.msg.serialization.MqttSerialization;
import io.mqute.process.Advance;

/**
 * Created by huz on 8/3/16.
 */
public class BacklogDao implements IAsyncQueueDao<PublishMessage> {

  private final PersistenceService service;
  private final Codec<PublishMessage> codec;

  public BacklogDao(PersistenceService service) {
    this.service = service;
    codec = new PublishCodec();
  }

  @Override
  public CompletableFuture<Long> offer(String key, PublishMessage... data) {
    return service.offer(key, codec, data);
  }

  @Override
  public CompletableFuture<Void> offer(Map<String, PublishMessage> data) {
    return service.offer(data, codec);
  }

  @Override
  public CompletableFuture<Boolean> remove(final String key) {
    return service.remove(key);
  }

  @Override
  public CompletableFuture<List<PublishMessage>> take(String key, int length) {
    return service.take(key, codec, length);
  }

  private static class PublishCodec implements Codec<PublishMessage> {

    @Override
    public byte[] encode(PublishMessage data) {
      ByteBuffer byteBuffer = MqttSerialization.getInstance().serialize(data);
      byte[] bytes = new byte[byteBuffer.remaining()];
      byteBuffer.get(bytes);
      return bytes;
    }

    @Override
    public PublishMessage decode(byte[] bytes) {
      return MqttSerialization.getInstance().deserialize(ByteBuffer.wrap(bytes));
    }
  }
}
