package io.mqute.persistence;

import com.google.protobuf.InvalidProtocolBufferException;
import io.mqute.process.publication.BaseRouter;
import io.mqute.process.publication.Router;
import io.mqute.util.Protobufs;
import org.cliffc.high_scale_lib.NonBlockingHashMap;

import java.util.concurrent.CompletableFuture;

import static io.mqute.util.Protobufs.persist;

/**
 * Created by huz on 8/3/16.
 */
public class RouterDao implements IAsyncDao<Router> {

    private static final String ROUTER_REDIS_KEY = "router-snapshot";

    private final PersistenceService service;
    private final Codec<Router> codec;

    public RouterDao(PersistenceService service) {
        this.service = service;
        codec = new RouterCodec();
    }

    @Override
    public CompletableFuture<Boolean> update(final String partition,
                                             final Router router) {
        return service.update(key(partition), codec, router);
    }

    @Override
    public CompletableFuture<Boolean> remove(final String partition) {
        return service.remove(key(partition));
    }

    @Override
    public CompletableFuture<Router> get(final String partition) {
        return service.get(key(partition), codec).exceptionally(e ->
                new BaseRouter(new NonBlockingHashMap<>(), 0)
        ).toCompletableFuture();
    }

    private String key(String partition) {
        return ROUTER_REDIS_KEY + ":" + partition;
    }

    private static class RouterCodec implements Codec<Router> {
        @Override
        public byte[] encode(Router data) {
            return persist(data).toByteArray();
        }

        @Override
        public Router decode(byte[] bytes) {
            try {
                return Protobufs.load(RouterOuterClass.Router.parseFrom(bytes));
            } catch (InvalidProtocolBufferException e) {
                throw new RuntimeException("Failed to decode RetainedPublish", e);
            }
        }
    }
}
