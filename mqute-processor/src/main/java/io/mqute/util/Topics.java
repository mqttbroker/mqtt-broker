package io.mqute.util;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

/**
 * Created by huz on 8/3/16.
 */
public abstract class Topics {

  public static Pair<String, String> split(final String topicNameOrFilter) {

    final int nextSlashAt = topicNameOrFilter.indexOf('/');

    if (nextSlashAt > 0) {
      return ImmutablePair.of(topicNameOrFilter.substring(0, nextSlashAt), topicNameOrFilter.substring(nextSlashAt + 1));
    }

    return ImmutablePair.of(topicNameOrFilter, null);
  }
}
