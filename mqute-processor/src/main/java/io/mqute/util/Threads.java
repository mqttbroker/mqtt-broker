package io.mqute.util;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by huz on 8/15/16.
 */
public abstract class Threads {

  private static final LoadingCache<String, AtomicInteger> THREAD_IDS = CacheBuilder.newBuilder().build(new CacheLoader<String, AtomicInteger>() {
    @Override
    public AtomicInteger load(String group) throws Exception {
      return new AtomicInteger(0);
    }
  });

  private Threads(){

  }

  public static String nextThreadName(final String group){
    try {
      return group + "-" + THREAD_IDS.get(group).getAndIncrement();
    }
    catch (ExecutionException e) {
      return group;
    }
  }
}
