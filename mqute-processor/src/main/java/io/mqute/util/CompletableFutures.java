package io.mqute.util;

import java.util.concurrent.CompletableFuture;

/**
 * Created by huz on 8/3/16.
 */
public abstract class CompletableFutures {

  public static final Void VOID = null;

  public static <V> CompletableFuture<V> immediateException(final Throwable th) {
    final CompletableFuture<V> failure = new CompletableFuture<>();
    failure.completeExceptionally(th);
    return failure;
  }
}
