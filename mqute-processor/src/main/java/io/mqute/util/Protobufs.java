package io.mqute.util;

import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.protobuf.ByteString;
import io.mqute.edge.Client;
import io.mqute.edge.EdgeClient;
import io.mqute.edge.State;
import io.mqute.msg.ConnectMessage;
import io.mqute.msg.PublishMessage;
import io.mqute.msg.serialization.MqttSerialization;
import io.mqute.persistence.ClientsOuterClass;
import io.mqute.persistence.PartitionRootOuterClass;
import io.mqute.persistence.RetainedPublishOuterClass;
import io.mqute.persistence.RouterOuterClass;
import io.mqute.process.keepalive.Clients;
import io.mqute.process.publication.BaseRouter;
import io.mqute.process.publication.RetainedPublish;
import io.mqute.process.publication.Router;
import io.mqute.process.subscription.AbstractNode;
import io.mqute.process.subscription.Node;
import io.mqute.process.subscription.PartitionRoot;
import io.mqute.process.subscription.Subscription;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.cliffc.high_scale_lib.NonBlockingHashMap;
import org.cliffc.high_scale_lib.NonBlockingHashSet;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

/**
 * Created by huz on 8/4/16.
 */
public abstract class Protobufs {

    public static ClientsOuterClass.Clients persist(final Clients clients) {
        ClientsOuterClass.Clients.Builder builder = ClientsOuterClass.Clients.newBuilder();
        builder = builder.putAllKeepAlives(Maps.transformValues(clients.keepAlives(), keepAlive -> ClientsOuterClass.TouchPoint.newBuilder().setLastTouched(keepAlive.getLeft()).setConnectMessage(ByteString.copyFrom(MqttSerialization.getInstance().serialize(keepAlive.getRight()))).build()));
        builder = builder.putAllOthers(clients.others());
        return builder.build();
    }

    public static Clients load(final ClientsOuterClass.Clients source) {
        final ConcurrentMap<String, Pair<Long, ConnectMessage>> keepAlives = new NonBlockingHashMap<>(source.getKeepAlivesCount());
        final ConcurrentMap<String, Long> others = new NonBlockingHashMap<>(source.getOthersCount());
        keepAlives.putAll(Maps.transformValues(source.getKeepAlivesMap(), keepAlive -> ImmutablePair.of(keepAlive.getLastTouched(), MqttSerialization.getInstance().deserialize(keepAlive.getConnectMessage().asReadOnlyByteBuffer()))));
        others.putAll(source.getOthersMap());

        return new Clients(keepAlives, others);
    }

    public static RetainedPublish load(final io.mqute.persistence.RetainedPublishOuterClass.RetainedPublish source) {
        final ConcurrentMap<String, RetainedPublish> children = new NonBlockingHashMap<>(source.getChildrenCount());
        children.putAll(Maps.transformValues(source.getChildrenMap(), child -> load(child)));
        ByteString byteString = source.getPublishMessage();
        PublishMessage message = null;
        if (!byteString.isEmpty()) {
            message = MqttSerialization.getInstance().deserialize(byteString.asReadOnlyByteBuffer());
        }
        return new RetainedPublish(children, message);
    }

    public static RetainedPublishOuterClass.RetainedPublish persist(final RetainedPublish retainedPublish) {
        RetainedPublishOuterClass.RetainedPublish.Builder builder = RetainedPublishOuterClass.RetainedPublish.newBuilder();
        if (retainedPublish.publishMessage() != null) {
            builder.setPublishMessage(ByteString.copyFrom(MqttSerialization.getInstance().serialize(retainedPublish.publishMessage())));
        }
        builder = builder.putAllChildren(Maps.transformValues(retainedPublish.children(), child -> persist(child)));
        return builder.build();
    }

    public static PartitionRootOuterClass.PartitionRoot persist(final PartitionRoot partitionRoot) {
        PartitionRootOuterClass.PartitionRoot.Builder builder = PartitionRootOuterClass.PartitionRoot.newBuilder();
        builder = builder.setPartition(partitionRoot.partition());
        builder = builder.putAllClients(Maps.transformValues(partitionRoot.clients(), client -> PartitionRootOuterClass.Client.newBuilder()
                .setClientID(client.clientID())
                .setEndpoint(client.endpoint())
                .setNextPacketID(client.nextPacketID())
                .setPersistentSession(client.persistentSession())
                .setState(PartitionRootOuterClass.Client.State.valueOf(client.state().name())).build()));
        builder = builder.putAllChildren(Maps.transformValues(partitionRoot.children(), Protobufs::persist));

        return builder.build();
    }

    public static PartitionRoot load(final PartitionRootOuterClass.PartitionRoot source) {

        final ConcurrentMap<String, Client> clients = new NonBlockingHashMap<>(source.getClientsCount());
        final ConcurrentMap<String, Node> children = new NonBlockingHashMap<>(source.getChildrenCount());
        clients.putAll(Maps.transformValues(source.getClientsMap(), client -> new EdgeClient(client.getClientID(), client.getPersistentSession(), client.getEndpoint(), State.valueOf(client.getState().name()), client.getNextPacketID())));
        children.putAll(Maps.transformValues(source.getChildrenMap(), Protobufs::load));

        return new PartitionRoot(source.getPartition(), clients, children, Collections.emptySet());
    }

    protected static PartitionRootOuterClass.Node persist(final Node node) {

        PartitionRootOuterClass.Node.Builder builder = PartitionRootOuterClass.Node.newBuilder();
        builder = builder.putAllChildren(Maps.transformValues(node.children(), Protobufs::persist));
        builder = builder.addAllSubscriptions(Iterables.transform(node.subscriptions(), Protobufs::persist));

        return builder.build();
    }

    protected static Node load(final PartitionRootOuterClass.Node source) {

        final ConcurrentMap<String, Node> children = new NonBlockingHashMap<>(source.getChildrenCount());
        final Set<Subscription> subscriptions = new NonBlockingHashSet<>();
        children.putAll(Maps.transformValues(source.getChildrenMap(), Protobufs::load));
        subscriptions.addAll(source.getSubscriptionsList().stream().map(Protobufs::load).collect(Collectors.toSet()));

        return new AbstractNode(children, subscriptions);
    }

    protected static PartitionRootOuterClass.Subscription persist(final Subscription subscription) {
        PartitionRootOuterClass.Subscription.Builder builder = PartitionRootOuterClass.Subscription.newBuilder();
        builder = builder.setClientID(subscription.clientID());
        builder = builder.setTopicFilter(subscription.topicFilter());
        builder = builder.setQos(subscription.qos());
        return builder.build();
    }

    protected static Subscription load(final PartitionRootOuterClass.Subscription source) {
        return new Subscription(source.getClientID(), source.getTopicFilter(), (byte) source.getQos());
    }

    public static RouterOuterClass.Router persist(final Router router) {
        RouterOuterClass.Router.Builder builder = RouterOuterClass.Router.newBuilder();
        builder = builder.setRoutings(router.actives());
        builder = builder.putAllChildren(Maps.transformValues(router.children(), Protobufs::persist));
        return builder.build();
    }

    public static Router load(final RouterOuterClass.Router source) {

        final ConcurrentMap<String, Router> children = new NonBlockingHashMap<>(source.getChildrenCount());
        children.putAll(Maps.transformValues(source.getChildrenMap(), Protobufs::load));

        return new BaseRouter(children, source.getRoutings());
    }
}
