package io.mqute.util;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutionException;

import io.mqute.edge.CallbackUdpClient;

/**
 * Created by huz on 8/16/16.
 */
public abstract class Endpoints {

  private static final Logger LOGGER = LoggerFactory.getLogger(Endpoints.class);

  private Endpoints() {

  }

  private static final LoadingCache<String, CallbackUdpClient> ENDPOINTS = CacheBuilder.newBuilder()
      .build(new CacheLoader<String, CallbackUdpClient>() {
        @Override
        public CallbackUdpClient load(final String endpoint) throws Exception {
          final String[] parts = endpoint.split(":");
          LOGGER.info("[endpoints] connect to: {}:{}", parts[0], parts[1]);
          return new CallbackUdpClient(parts[0], Integer.parseInt(parts[1]));
        }
      });

  public static CallbackUdpClient get(final String endpoint) {
    try {
      return ENDPOINTS.get(endpoint);
    } catch (ExecutionException e) {
      LOGGER.error("[endpoints] connect failed due to: {}", e);
      return null;
    }
  }
}
