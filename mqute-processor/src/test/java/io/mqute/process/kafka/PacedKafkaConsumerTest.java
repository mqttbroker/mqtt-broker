package io.mqute.process.kafka;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Iterables;
import com.google.common.util.concurrent.SettableFuture;
import io.mqute.kafka.KafkaTopics;
import io.mqute.kafka.consumer.MquteKafkaConsumerFactory;
import io.mqute.kafka.msg.KafkaMqttMessageWrapper;
import io.mqute.kafka.producer.KafkaMessageDispatcher;
import io.mqute.kafka.serialization.KafkaMqttMessageWrapperSerializer;
import io.mqute.msg.PublishMessage;
import io.mqute.msg.enums.QoSType;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by huz on 8/13/16.
 */
// FIXME: 8/17/16 hang on gradle build
@Ignore
public class PacedKafkaConsumerTest {

  private static final Logger LOGGER = LoggerFactory.getLogger(PacedKafkaConsumerTest.class);

  final LoadingCache<TopicPartition, ConcurrentNavigableMap<Long, List<ConsumerRecord<String, KafkaMqttMessageWrapper>>>> pacedRecords = CacheBuilder.newBuilder().maximumSize(1024).build(new CacheLoader<TopicPartition, ConcurrentNavigableMap<Long, List<ConsumerRecord<String, KafkaMqttMessageWrapper>>>>() {
    @Override
    public ConcurrentNavigableMap<Long, List<ConsumerRecord<String, KafkaMqttMessageWrapper>>> load(TopicPartition key) throws Exception {
      return new ConcurrentSkipListMap<>();
    }
  });

  final MquteKafkaConsumerFactory consumerFactory = new MquteKafkaConsumerFactory();

  static final String PRODUCER_CONF_PATH = "META-INF/kafka-producer.properties";
  static KafkaProducer<String, KafkaMqttMessageWrapper> producer;

  @BeforeClass
  public static void beforeClass() {
    final Properties PRODUCER_CONFIGURATION = new Properties();
    try {
      PRODUCER_CONFIGURATION.load(KafkaMessageDispatcher.class.getClassLoader().getResourceAsStream(PRODUCER_CONF_PATH));
    } catch (IOException ex) {
      LOGGER.warn("Failed to load kafka configuration from {}", PRODUCER_CONF_PATH, ex);
    }
    producer = new KafkaProducer<>(PRODUCER_CONFIGURATION,
        new StringSerializer(),
        new KafkaMqttMessageWrapperSerializer()
    );
  }

  @Test
  public void testPacedKafkaConsumer() throws InterruptedException, ExecutionException, TimeoutException {

    final PacedKafkaConsumer<String, KafkaMqttMessageWrapper> consumer = new PacedKafkaConsumer<>(pacedRecords,
            Executors.newSingleThreadExecutor(),
            consumerFactory.create("paced-ut", "paced-ut-group"));
    Assert.assertNotNull(consumer);

    final SettableFuture<Void> assigned = SettableFuture.create();
    final AtomicInteger numOfPartitions = new AtomicInteger(0);
    consumer.subscribe(Collections.singletonList(KafkaTopics.PUBLISH_TOPIC), new ConsumerRebalanceListener() {
      @Override
      public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
        LOGGER.warn("[revoked] {}", partitions);
      }

      @Override
      public void onPartitionsAssigned(Collection<TopicPartition> partitions) {
        LOGGER.warn("[assigned] {}", partitions);
        numOfPartitions.addAndGet(partitions.size());
        assigned.set(null);
      }
    });

    assigned.get(1, TimeUnit.MINUTES);

    Assert.assertEquals(1, numOfPartitions.get());

    for (int i = 0; i < 8; i += 1) {
      consumer.poll(TimeUnit.SECONDS.toMillis(1));
      consumer.commitSync();
    }

    for(int repeat = 0; repeat < 8; repeat += 1) {
      final PublishMessage publishMessage = new PublishMessage(false, QoSType.LEAST_ONCE, false, (short) repeat, "paced-ut", new byte[0]);
      producer.send(new ProducerRecord<>(KafkaTopics.PUBLISH_TOPIC, new KafkaMqttMessageWrapper(publishMessage, "paced-ut-client", new String[0], "0.0.0.0", 0))).get(1, TimeUnit.MINUTES);
      LOGGER.info("[paced][ut] sent publish message:{}", repeat);

      for (Iterable<ConsumerRecord<String, KafkaMqttMessageWrapper>> polled = consumer.poll(TimeUnit.SECONDS.toMillis(1)).records(KafkaTopics.PUBLISH_TOPIC); ; polled = consumer.poll(TimeUnit.SECONDS.toMillis(1)).records(KafkaTopics.PUBLISH_TOPIC)) {
        LOGGER.info("[paced][ut] polled publish message:{}", Iterables.size(polled));
        if(!Iterables.isEmpty(polled)){
          Assert.assertEquals(1, Iterables.size(polled));
          Assert.assertEquals(PublishMessage.class, Iterables.getFirst(polled, null).value().getOriginalMessage().getClass());
          Assert.assertEquals(repeat, ((PublishMessage)Iterables.getFirst(polled, null).value().getOriginalMessage()).getPacketId());
          consumer.commitSync();
          break;
        }
        consumer.commitSync();
      }
    }
  }
}
