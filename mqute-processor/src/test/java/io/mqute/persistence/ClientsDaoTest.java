package io.mqute.persistence;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import io.mqute.msg.ConnectMessage;
import io.mqute.process.keepalive.Clients;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


public class ClientsDaoTest {

  private ClientsDao dao;
  private PersistenceService service;

  @Before
  public void beforeEach() throws Exception {
    service = new MockPersistenceService();
    dao = new ClientsDao(service);
  }

  @Test
  public void testUpdateGetRemove() throws Exception {
    ConcurrentMap<String, Pair<Long, ConnectMessage>> keepAlives = new ConcurrentHashMap<>();
    keepAlives.put("abc", Pair.of(123L, new ConnectMessage("MQTT", (byte) 0x01, (short) 100, true, "abc")));
    ConcurrentMap<String, Long> others = new ConcurrentHashMap<>();
    others.put("abc", 123L);
    Clients clients = new Clients(keepAlives, others);
    assertTrue(dao.update("1", clients).get());

    clients = dao.get("1").get();
    assertNotNull(clients.keepAlives().get("abc"));
    assertNotNull(clients.others().get("abc"));

    assertTrue(dao.remove("1").get());
    clients = dao.get("1").get();
    assertTrue(clients.keepAlives().isEmpty());
    assertTrue(clients.others().isEmpty());
  }

}