package io.mqute.persistence;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Stream;


public class MockPersistenceService implements PersistenceService {

  private final ConcurrentMap<String, byte[]> store;
  private final ConcurrentMap<String, Queue<byte[]>> queueMap;

  public MockPersistenceService() {
    store = new ConcurrentHashMap<>();
    queueMap = new ConcurrentHashMap<>();
  }

  @Override
  public <T> CompletableFuture<Boolean> update(String key, Codec<T> codec, T data) {
    try {
      return CompletableFuture.completedFuture(store.put(key, codec.encode(data)) == null);
    } catch (Exception e) {
      CompletableFuture<Boolean> future = new CompletableFuture<>();
      future.completeExceptionally(e);
      return future;
    }
  }

  @Override
  public CompletableFuture<Boolean> remove(String key) {
    if (key.contains(":")) {
      return CompletableFuture.completedFuture(store.remove(key) != null);
    } else {
      return CompletableFuture.completedFuture(queueMap.remove(key) != null);
    }
  }

  @Override
  public <T> CompletableFuture<T> get(String key, Codec<T> codec) {
    try {
      return CompletableFuture.completedFuture(codec.decode(store.get(key)));
    } catch (Exception e) {
      CompletableFuture<T> future = new CompletableFuture<>();
      future.completeExceptionally(e);
      return future;
    }
  }

  @Override
  public <T> CompletableFuture<Long> offer(String key, Codec<T> codec, T... data) {
    try {
      Queue<byte[]> queue = queueMap.getOrDefault(key, new LinkedList<>());
      Stream.of(data).map(codec::encode).forEach(queue::offer);
      queueMap.put(key, queue);
      return CompletableFuture.completedFuture((long) queue.size());
    } catch (Exception e) {
      CompletableFuture<Long> future = new CompletableFuture<>();
      future.completeExceptionally(e);
      return future;
    }
  }

  @Override
  public <T> CompletableFuture<Void> offer(Map<String, T> data, Codec<T> codec) {
    return null;
  }

  @Override
  public <T> CompletableFuture<List<T>> take(String key, Codec<T> codec, int length) {
    try {
      Queue<byte[]> queue = queueMap.getOrDefault(key, new LinkedList<>());
      List<T> result = new LinkedList<>();
      for (int i = 0; i < length && !queue.isEmpty(); i++) {
        byte[] bytes = queue.poll();
        result.add(codec.decode(bytes));
      }
      return CompletableFuture.completedFuture(result);
    } catch (Exception e) {
      CompletableFuture<List<T>> future = new CompletableFuture<>();
      future.completeExceptionally(e);
      return future;
    }
  }
}
