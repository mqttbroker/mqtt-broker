package io.mqute.persistence;

import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import io.mqute.edge.Client;
import io.mqute.edge.EdgeClient;
import io.mqute.edge.State;
import io.mqute.msg.enums.QoSType;
import io.mqute.process.subscription.Node;
import io.mqute.process.subscription.PartitionRoot;
import io.mqute.process.subscription.Subscription;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by zhuwang on 8/25/16.
 */
public class PartitionRootDaoTest {

  private PartitionRootDao dao;
  private PersistenceService service;

  @Before
  public void beforeEach() throws Exception {
    service = new MockPersistenceService();
    dao = new PartitionRootDao(service);
  }

  @Test
  public void testUpdateGetRemove() throws Exception {
    ConcurrentMap<String, Client> clients = new ConcurrentHashMap<>();
    Client client = new EdgeClient("abc", true, "127.0.0.1:2172", State.IDLE, 3);
    clients.put("abc", client);
    ConcurrentMap<String, Node> children = new ConcurrentHashMap<>();
    Node child = new PartitionRoot(1, new ConcurrentHashMap<>(), new ConcurrentHashMap<>(), new HashSet<>());
    children.put("a", child);
    Set<Subscription> subscriptions = new HashSet<>();
    Subscription subscription = new Subscription("abc", "a/b/c", QoSType.MOST_ONCE.getByteValue());
    subscriptions.add(subscription);
    PartitionRoot root = new PartitionRoot(1, clients, children, subscriptions);

    assertTrue(dao.update("1", root).get());

    root = dao.get("1").get();
    assertNotNull(root);

    assertTrue(dao.remove("1").get());
    root = dao.get("1").get();
    assertNotNull(root);
  }

}