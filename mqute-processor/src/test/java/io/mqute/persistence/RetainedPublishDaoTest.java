package io.mqute.persistence;

import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import io.mqute.msg.PublishMessage;
import io.mqute.msg.enums.QoSType;
import io.mqute.process.publication.RetainedPublish;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by zhuwang on 8/25/16.
 */
public class RetainedPublishDaoTest {

  private RetainedPublishDao dao;
  private PersistenceService service;

  @Before
  public void beforeEach() throws Exception {
    service = new MockPersistenceService();
    dao = new RetainedPublishDao(service);
  }

  @Test
  public void testUpdateGetRemove() throws Exception {
    PublishMessage publishMessage = new PublishMessage(false, QoSType.LEAST_ONCE, true,
        (short) 123, "a/b/c", "hello".getBytes());
    RetainedPublish child = new RetainedPublish(null, publishMessage);

    ConcurrentMap<String, RetainedPublish> children = new ConcurrentHashMap<>();
    children.put("c", child);
    RetainedPublish retainedPublish = new RetainedPublish(children, null);

    assertTrue(dao.update("1", retainedPublish).get());

    retainedPublish = dao.get("1").get();
    assertNotNull(retainedPublish);

    assertTrue(dao.remove("1").get());
    retainedPublish = dao.get("1").get();
    assertNotNull(retainedPublish);
  }

}