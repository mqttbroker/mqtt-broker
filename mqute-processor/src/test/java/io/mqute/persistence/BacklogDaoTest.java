package io.mqute.persistence;

import org.junit.Before;
import org.junit.Test;

import io.mqute.msg.PublishMessage;
import io.mqute.msg.enums.QoSType;

import static org.junit.Assert.*;

/**
 * Created by zhuwang on 8/25/16.
 */
public class BacklogDaoTest {

  private BacklogDao dao;
  private PersistenceService service;

  @Before
  public void beforeEach() throws Exception {
    service = new MockPersistenceService();
    dao = new BacklogDao(service);
  }

  @Test
  public void testOfferTakeRemove() throws Exception {
    PublishMessage message1 = new PublishMessage(false, QoSType.LEAST_ONCE, true,
        (short) 123, "a/b/c", "hello".getBytes());
    PublishMessage message2 = new PublishMessage(false, QoSType.LEAST_ONCE, true,
        (short) 124, "a/b/c", "hello".getBytes());
    PublishMessage message3 = new PublishMessage(false, QoSType.LEAST_ONCE, true,
        (short) 125, "a/b/c", "hello".getBytes());

    assertEquals(2, dao.offer("key", message1, message2).get().longValue());
    assertEquals(3, dao.offer("key", message3).get().longValue());

    assertEquals(1, dao.take("key", 1).get().size());
    assertEquals(2, dao.take("key", 2).get().size());
    assertEquals(0, dao.take("key", 2).get().size());

    assertEquals(1, dao.offer("key", message1).get().longValue());
    assertEquals(1, dao.take("key", 2).get().size());

    assertTrue(dao.remove("key").get());
    assertEquals(0, dao.take("key", 1).get().size());
    assertFalse(dao.remove("key").get());
  }

}