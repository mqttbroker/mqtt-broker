package io.mqute.edge;

import org.junit.Test;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.concurrent.BlockingQueue;

import io.mqute.msg.ConnAckMessage;
import io.mqute.msg.MqttMessage;
import io.mqute.msg.enums.ConnectReturnCode;
import io.mqute.msg.internal.OutgoingMessageWrapper;


public class CallbackUdpClientTest {

  private static final int PORT = 9876;

  @Test
  public void send() throws Exception {
    CallbackUdpClient client = new CallbackUdpClient("localhost", PORT);
    MqttMessage message = new ConnAckMessage(ConnectReturnCode.ACCEPTED, false);
    OutgoingMessageWrapper messageWrapper = new OutgoingMessageWrapper(message, "abc");
    client.send(messageWrapper);
  }

}