package io.mqute.publication;

import io.mqute.process.publication.BaseRouter;
import io.mqute.process.publication.Router;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by huz on 8/31/16.
 */
public class RouterTest {

    @Test
    public void testRouter(){

        final Router router = new BaseRouter(null, 0);

        Assert.assertNotNull(router.children());
        Assert.assertTrue(router.children().isEmpty());
        Assert.assertEquals(0, router.actives());
        Assert.assertFalse(router.route("1/2/3"));

        final Router router3 = router.locate("1/2/3");
        Assert.assertNotNull(router3);
        Assert.assertFalse(router.children().isEmpty());
        Assert.assertTrue(router3.children().isEmpty());
        Assert.assertEquals(0, router3.actives());

        Assert.assertFalse(router.route("1/2/3"));
        Assert.assertFalse(router.route("1/2/3/4"));
        router3.up();
        Assert.assertTrue(router.route("1/2/3"));
        Assert.assertTrue(router.route("1/2/3/4"));//deeper than 3, still routes
        router3.down();
        Assert.assertFalse(router.route("1/2/3"));
        Assert.assertFalse(router.route("1/2/3/4"));

        final Router router4 = router.locate("a/b/c/d");
        router4.up();
        Assert.assertTrue(router.route("a/b/c/d"));
        Assert.assertTrue(router.route("a/b/c"));
        Assert.assertFalse(router.route("a/b"));
        Assert.assertFalse(router.route("a"));
    }
}
