#!/usr/bin/env bash

BASEDIR=$(dirname "$0")
KAFKA_HOME="${BASEDIR}/kafka_2.11-0.10.0.0"
REDIS_HOME="${BASEDIR}/redis-3.2.3"

GENERAL_TOPIC="CONNECT_DISCONNECT_SUBSCRIBE_UNSUBSCRIBE"
PUBLISH_TOPIC="PUBLISH"
ROUTED_PUBLISH_TOPIC="ROUTED_PUBLISH"
KEEP_ALIVE_TOPIC="KEEP_ALIVE"
ACK_TOPIC="PUBACK_PUBREC_PUBCOMP_PINGREQ"

PARTITION_SIZE=1
REPLICATION_FACTOR=1
DEBUG_MODE=false

while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -p|--parition)
    PARTITION_SIZE="$2"
    shift # past argument
    ;;
    -r|--replication)
    REPLICATION_FACTOR="$2"
    shift # past argument
    ;;
    -x|--debug)
    DEBUG_MODE=true
    shift # past argument
    ;;
    *)
    # unknown option
    ;;
esac
shift # past argument or value
done
echo Parition size = "${PARTITION_SIZE}"
echo Replication factor = "${REPLICATION_FACTOR}"
echo Debug mode = "${DEBUG_MODE}"
echo Kafka home = "${KAFKA_HOME}"

# start zookeeper
${KAFKA_HOME}/bin/zookeeper-server-start.sh ${KAFKA_HOME}/config/zookeeper.properties &
# start kafka
${KAFKA_HOME}/bin/kafka-server-start.sh ${KAFKA_HOME}/config/server.properties &
# create topics
${KAFKA_HOME}/bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic ${GENERAL_TOPIC}
${KAFKA_HOME}/bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic ${PUBLISH_TOPIC}
${KAFKA_HOME}/bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic ${ACK_TOPIC}
${KAFKA_HOME}/bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic ${ROUTED_PUBLISH_TOPIC}
${KAFKA_HOME}/bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic ${KEEP_ALIVE_TOPIC}

# start reids
${REDIS_HOME}/src/redis-server &

if [ "$DEBUG_MODE" = true ]; then
    echo "In debug mode, you need to start the connection node and processor node in IDE."
else
    # start process node
    ${BASEDIR}/gradlew ":mqute-processor:run" &
    # start netty node
    ${BASEDIR}/gradlew ":mqute-netty:run" &
fi