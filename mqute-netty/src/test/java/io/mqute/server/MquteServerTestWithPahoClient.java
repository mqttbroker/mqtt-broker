package io.mqute.server;


import io.mqute.IMessageDispatcher;
import io.mqute.config.MquteConfig;
import io.mqute.core.MessageDispatcherRegistry;
import io.mqute.msg.serialization.util.ProtocolUtils;
import io.mqute.server.mock.MockCallbackDispatcher;
import io.mqute.server.mock.RandomPortConfig;
import io.mqute.server.netty.CallbackUdpServer;
import io.mqute.server.netty.MquteNettyServer;
import io.mqute.test.MockAuthenticator;
import io.mqute.test.MockAuthorizor;
import org.eclipse.paho.client.mqttv3.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.Optional;

import static org.junit.Assert.*;


public class MquteServerTestWithPahoClient {

  private static IMqttServer server;
  private static IMqttServer udpServer;
  private static MquteConfig mquteConfig;

  private MqttConnectOptions options;

  private byte[] payload = "hello world".getBytes(ProtocolUtils.UTF_8);

  @BeforeClass
  public static void beforeAll() throws Exception {
    mquteConfig = new RandomPortConfig(new MockAuthenticator(), new MockAuthorizor());
    udpServer = new CallbackUdpServer(mquteConfig.downstreamPort());
    udpServer.start();
    server = new MquteNettyServer(mquteConfig);
    server.start();
    IMessageDispatcher dispatcher = new MockCallbackDispatcher("localhost", mquteConfig.downstreamPort());
    MessageDispatcherRegistry.register(dispatcher);
  }

  @AfterClass
  public static void afterAll() throws Exception {
    Field field = MessageDispatcherRegistry.class.getDeclaredField("dispatcher");
    field.setAccessible(true);
    field.set(null, null);
    field.setAccessible(false);
    server.stop();
    udpServer.stop();
  }

  @Before
  public void beforeEach() throws Exception {
    options = new MqttConnectOptions();
    options.setMqttVersion(4);
    options.setUserName("username");
    options.setPassword("password".toCharArray());
  }

  @Test(expected = MqttException.class)
  public void testOpenConnectionWithUnsupportedProtocol() throws Exception {
    IMqttClient pahoClient = new MqttClient("tcp://localhost:" + mquteConfig.tcpPort(), "TestClient" + System.nanoTime());
    options.setMqttVersion(3);
    pahoClient.connect(options);
    assertFalse(pahoClient.isConnected());
    pahoClient.close();
  }

  @Test(expected = MqttException.class)
  public void testOpenConnectionWithEmptyClientId() throws Exception {
    IMqttClient pahoClient = new MqttClient("tcp://localhost:" + mquteConfig.tcpPort(), "");
    pahoClient.connect(options);
    assertFalse(pahoClient.isConnected());
    pahoClient.close();
  }

  @Test(expected = MqttSecurityException.class)
  public void testOpenConnectionWithBadCredential() throws Exception {
    IMqttClient pahoClient = new MqttClient("tcp://localhost:" + mquteConfig.tcpPort(), "TestClient" + System.nanoTime());
    options.setUserName("someone");
    pahoClient.connect(options);
    assertFalse(pahoClient.isConnected());
    pahoClient.close();
  }

  @Test(expected = MqttSecurityException.class)
  public void testOpenConnectionWithBlockedId() throws Exception {
    IMqttClient pahoClient = new MqttClient("tcp://localhost:" + mquteConfig.tcpPort(), "blocked");
    pahoClient.connect(options);
    assertFalse(pahoClient.isConnected());
    pahoClient.close();
  }

  @Test
  public void testOpenConnection() throws Exception {
    IMqttClient pahoClient = new MqttClient("tcp://localhost:" + mquteConfig.tcpPort(), "TestClient" + System.nanoTime());
    pahoClient.connect(options);
    assertTrue(pahoClient.isConnected());
    pahoClient.disconnect();
    pahoClient.close();
  }

  @Test
  public void testSubscribe() throws Exception {
    IMqttClient subscriber = new MqttClient("tcp://localhost:" + mquteConfig.tcpPort(), "TestClient" + System.nanoTime());
    subscriber.connect(options);
    assertTrue(subscriber.isConnected());
    int[] qoses = new int[]{0, 1};
    subscriber.subscribe(new String[]{
        "topic/writeonly",
        "a/b/c"
    }, qoses);
    assertArrayEquals(new int[]{128, 1}, qoses);
    subscriber.disconnect();
  }

  @Test
  public void testPublishMostOnce() throws Exception {
    IMqttClient subscriber = new MqttClient("tcp://localhost:" + mquteConfig.tcpPort(), "TestClient" + System.nanoTime());
    TestCallback callback = new TestCallback();
    subscriber.setCallback(callback);
    subscriber.connect(options);
    assertTrue(subscriber.isConnected());
    subscriber.subscribe("a/b/c", 0);

    IMqttClient publisher = new MqttClient("tcp://localhost:" + mquteConfig.tcpPort(), "TestClient" + System.nanoTime());
    publisher.connect(options);
    publisher.publish("a/b/c", payload, 0, false);

    Thread.sleep(1000);
    assertEquals("a/b/c", callback.getTopic());
    assertArrayEquals(payload, callback.getMessage().getPayload());

    subscriber.disconnect();
    subscriber.close();
    publisher.disconnect();
    publisher.close();
  }

  @Test
  public void testPublishLeastOnce() throws Exception {
    IMqttClient subscriber = new MqttClient("tcp://localhost:" + mquteConfig.tcpPort(), "TestClient" + System.nanoTime());
    TestCallback callback = new TestCallback();
    subscriber.setCallback(callback);
    subscriber.connect(options);
    assertTrue(subscriber.isConnected());
    subscriber.subscribe("a/b/c", 1);

    IMqttClient publisher = new MqttClient("tcp://localhost:" + mquteConfig.tcpPort(), "TestClient" + System.nanoTime());
    publisher.connect(options);
    publisher.publish("a/b/c", payload, 1, false);

    Thread.sleep(1000);
    assertEquals("a/b/c", callback.getTopic());
    assertArrayEquals(payload, callback.getMessage().getPayload());

    subscriber.disconnect();
    subscriber.close();
    publisher.disconnect();
    publisher.close();
  }

  @Test
  public void testPublishExactOnce() throws Exception {
    IMqttClient subscriber = new MqttClient("tcp://localhost:" + mquteConfig.tcpPort(), "TestClient" + System.nanoTime());
    TestCallback callback = new TestCallback();
    subscriber.setCallback(callback);
    subscriber.connect(options);
    assertTrue(subscriber.isConnected());
    subscriber.subscribe("a/b/c", 2);

    IMqttClient publisher = new MqttClient("tcp://localhost:" + mquteConfig.tcpPort(), "TestClient" + System.nanoTime());
    publisher.connect(options);
    publisher.publish("a/b/c", payload, 2, false);

    Thread.sleep(1000);
    assertEquals("a/b/c", callback.getTopic());
    assertArrayEquals(payload, callback.getMessage().getPayload());

    subscriber.disconnect();
    subscriber.close();
    publisher.disconnect();
    publisher.close();
  }

  private static class TestCallback implements MqttCallback {

    private String topic;
    private MqttMessage message;

    @Override
    public void connectionLost(Throwable cause) {

    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
      this.topic = topic;
      this.message = message;
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {

    }

    public String getTopic() {
      return topic;
    }

    public MqttMessage getMessage() {
      return message;
    }
  }
}
