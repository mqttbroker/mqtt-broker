package io.mqute.server.mock;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.*;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

@WebSocket
public class MQTTWebSocket {

  private final CountDownLatch connectSentLatch;
  private Session session;
  private BlockingQueue<ByteBuffer> receivedMessages;

  public MQTTWebSocket() {
    receivedMessages = new LinkedBlockingDeque<>();
    connectSentLatch = new CountDownLatch(1);
  }

  @OnWebSocketClose
  public void onClose(int statusCode, String reason) {
    System.out.printf("Connection closed: %d - %s%n", statusCode, reason);
    this.session = null;
    receivedMessages.clear();
  }

  @OnWebSocketConnect
  public void onConnect(Session session) throws IOException {
    this.session = session;
    connectSentLatch.countDown();
  }

  public boolean awaitConnected(int duration, TimeUnit unit) throws InterruptedException {
    return this.connectSentLatch.await(duration, unit);
  }

  public void sendBytes(ByteBuffer data) throws IOException {
    session.getRemote().sendBytes(data);
  }

  public ByteBuffer receivedBytes() throws InterruptedException {
    return receivedMessages.take();
  }

  public void close() {
    session.close();
  }

  @OnWebSocketMessage
  public void onMessage(byte[] buf, int offset, int length) throws InterruptedException {
    ByteBuffer data = ByteBuffer.allocateDirect(length);
    data.mark();
    data.put(buf, offset, length);
    data.reset();
    receivedMessages.put(data);
  }

  @OnWebSocketError
  public void onError(Session session, Throwable cause) {
    System.out.printf("Got exception: %s%n", cause);
    session.close();
  }
}
