package io.mqute.server.mock;

import com.google.protobuf.ByteString;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.concurrent.CompletableFuture;

import io.mqute.IMessageDispatcher;
import io.mqute.core.MquteSubscription;
import io.mqute.core.topic.PublishTopicName;
import io.mqute.core.topic.SubscribeTopicFilter;
import io.mqute.core.topic.TopicNode;
import io.mqute.msg.ConnAckMessage;
import io.mqute.msg.MqttMessage;
import io.mqute.msg.OutgoingMessageWrapperOuterClass;
import io.mqute.msg.PingRespMessage;
import io.mqute.msg.PublishMessage;
import io.mqute.msg.SubscribeMessage;
import io.mqute.msg.UnsubscribeMessage;
import io.mqute.msg.enums.ConnectReturnCode;
import io.mqute.msg.internal.OutgoingMessageWrapper;
import io.mqute.msg.serialization.MqttSerialization;
import io.mqute.msg.serialization.util.ProtocolUtils;


public class MockCallbackDispatcher implements IMessageDispatcher {

  private final String callbackHost;
  private final int callbackPort;
  private TopicNode root;

  public MockCallbackDispatcher(String callbackHost, int callbackPort) {
    this.callbackHost = callbackHost;
    this.callbackPort = callbackPort;
    root = new TopicNode();
  }

  @Override
  public <Message> CompletableFuture<Object> dispatch(Message message, String clientId, String... targets) {
    CompletableFuture<Object> future = new CompletableFuture<>();
    try {
      internalDispatch(message, clientId);
      future.complete(null);
    } catch (Exception e) {
      future.completeExceptionally(e);
    }
    return future;
  }

  @Override
  public <Message> CompletableFuture<Object> dispatch(Message message, String clientId, String kafkaTopic, int partition, String... targets) {
    return dispatch(message, clientId);
  }

  @Override
  public void keepAlive(String clientId) {

  }

  private <Message> void internalDispatch(Message message, String clientId) {
    if (message instanceof MqttMessage) {
      MqttMessage mqttMessage = (MqttMessage) message;
      switch (mqttMessage.getMessageType()) {
        case CONNECT:
          sendReply(new OutgoingMessageWrapper(new ConnAckMessage(ConnectReturnCode.ACCEPTED, false), clientId));
          break;
        case SUBSCRIBE:
          SubscribeMessage subscribeMessage = (SubscribeMessage) mqttMessage;
          subscribeMessage.getSubscriptions().forEach((topic, qos) -> {
            SubscribeTopicFilter topicFilter = new SubscribeTopicFilter(topic);
            MquteSubscription subscription = new MquteSubscription(clientId, topic, qos);
            root.locate(topicFilter).append(clientId, subscription);
          });
          break;
        case UNSUBSCRIBE:
          UnsubscribeMessage unsubscribeMessage = (UnsubscribeMessage) mqttMessage;
          unsubscribeMessage.getTopicFilters().forEach(topic -> {
            SubscribeTopicFilter topicFilter = new SubscribeTopicFilter(topic);
            root.locate(topicFilter).remove(clientId);
          });
          break;
        case PUBLISH:
          PublishMessage publishMessage = (PublishMessage) mqttMessage;
          PublishTopicName topicName = new PublishTopicName(publishMessage.getTopicName());
          root.filter(topicName).forEach((node) ->
              node.getSubscriptions().forEach(subscription ->
                  sendReply(new OutgoingMessageWrapper(publishMessage, subscription.getClientId()))
              )
          );
          break;
        case PUBACK:
          break;
        case PUBREC:
          break;
        case PUBCOMP:
          break;
        default:
          throw new RuntimeException(
              "Cannot dispatch message with type " + mqttMessage.getMessageType()
          );
      }
    } else {
      throw new RuntimeException("Cannot dispatch message " + message);
    }
  }

  private void sendReply(OutgoingMessageWrapper messageWrapper) {
    try (DatagramSocket socket = new DatagramSocket()){
      String clientId = messageWrapper.getClientId();
      int partition = messageWrapper.getPartition();
      ByteBuffer byteBuffer = MqttSerialization.getInstance()
          .serialize(messageWrapper.getOriginalMessage());
      byte[] bytes = OutgoingMessageWrapperOuterClass.OutgoingMessageWrapper.newBuilder()
          .setOriginalMessage(ByteString.copyFrom(byteBuffer))
          .setClientID(clientId)
          .setPartition(partition)
          .build()
          .toByteArray();
      InetAddress address = InetAddress.getByName(callbackHost);
      DatagramPacket packet = new DatagramPacket(bytes, bytes.length, address, callbackPort);
      socket.send(packet);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
