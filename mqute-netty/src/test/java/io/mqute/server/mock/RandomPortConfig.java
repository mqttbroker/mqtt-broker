package io.mqute.server.mock;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.Optional;

import io.mqute.config.MquteConfig;
import io.mqute.security.IAuthenticator;
import io.mqute.security.IAuthorizor;
import io.mqute.server.ISSLContextProvider;


public class RandomPortConfig implements MquteConfig {

  private final int m_tcpPort;
  private final int m_downstreamPort;
  private final int m_sslPort;
  private final int m_wsPort;
  private final int m_secureWSPort;
  private final Optional<IAuthenticator> m_authenticator;
  private final Optional<IAuthorizor> m_authorizor;
  private final Optional<ISSLContextProvider> m_sslContextProvider;

  public RandomPortConfig() {
    this(null, null);
  }

  public RandomPortConfig(IAuthenticator authenticator, IAuthorizor authorizor) {
    m_tcpPort = findFreePort();
    m_downstreamPort = findFreePort();
    m_sslPort = findFreePort();
    m_wsPort = findFreePort();
    m_secureWSPort = findFreePort();
    m_authenticator = Optional.ofNullable(authenticator);
    m_authorizor = Optional.ofNullable(authorizor);
    m_sslContextProvider = Optional.empty();
  }

  @Override
  public int tcpPort() {
    return m_tcpPort;
  }

  @Override
  public int downstreamPort() {
    return m_downstreamPort;
  }

  @Override
  public int sslPort() {
    return m_sslPort;
  }

  @Override
  public int webSocketPort() {
    return m_wsPort;
  }

  @Override
  public int secureWebSocketPort() {
    return m_secureWSPort;
  }

  @Override
  public Optional<IAuthenticator> authenticator() {
    return m_authenticator;
  }

  @Override
  public Optional<IAuthorizor> authorizor() {
    return m_authorizor;
  }

  @Override
  public Optional<ISSLContextProvider> sslContextProvider() {
    return m_sslContextProvider;
  }

  private int findFreePort() {
    ServerSocket socket = null;
    try {
      socket = new ServerSocket(0);
      socket.setReuseAddress(true);
      int port = socket.getLocalPort();
      try {
        socket.close();
      } catch (IOException e) {
        // Ignore IOException on close()
      }
      return port;
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (socket != null) {
        try {
          socket.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    throw new IllegalStateException("Could not find a free TCP/IP port to start embedded Jetty HTTP Server on");
  }
}
