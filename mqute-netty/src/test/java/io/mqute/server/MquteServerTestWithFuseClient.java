package io.mqute.server;


import io.mqute.IMessageDispatcher;
import io.mqute.config.MquteConfig;
import io.mqute.core.MessageDispatcherRegistry;
import io.mqute.msg.enums.QoSType;
import io.mqute.msg.serialization.util.ProtocolUtils;
import io.mqute.server.mock.MockCallbackDispatcher;
import io.mqute.server.mock.RandomPortConfig;
import io.mqute.server.netty.CallbackUdpServer;
import io.mqute.server.netty.MquteNettyServer;
import io.mqute.test.MockAuthenticator;
import io.mqute.test.MockAuthorizor;
import org.fusesource.mqtt.client.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.Optional;

import static org.junit.Assert.*;


public class MquteServerTestWithFuseClient {

  private static IMqttServer server;
  private static IMqttServer udpServer;
  private static MquteConfig mquteConfig;
  private MQTT fuseClient;

  private byte[] payload = "hello world".getBytes(ProtocolUtils.UTF_8);

  @BeforeClass
  public static void beforeAll() {
    mquteConfig = new RandomPortConfig(new MockAuthenticator(), new MockAuthorizor());
    udpServer = new CallbackUdpServer(mquteConfig.downstreamPort());
    udpServer.start();
    server = new MquteNettyServer(mquteConfig);
    server.start();
    IMessageDispatcher dispatcher = new MockCallbackDispatcher("localhost", mquteConfig.downstreamPort());
    MessageDispatcherRegistry.register(dispatcher);
  }

  @AfterClass
  public static void afterAll() throws Exception {
    Field field = MessageDispatcherRegistry.class.getDeclaredField("dispatcher");
    field.setAccessible(true);
    field.set(null, null);
    field.setAccessible(false);
    server.stop();
    udpServer.stop();
  }

  @Before
  public void beforeEach() throws Exception {
    fuseClient = new MQTT();
    fuseClient.setVersion("3.1.1");
    fuseClient.setHost("localhost", mquteConfig.tcpPort());
    fuseClient.setUserName("username");
    fuseClient.setPassword("password");
  }

  @Test(expected = MQTTException.class)
  public void testOpenConnectionWithUnsupportedProtocol() throws Exception {
    fuseClient.setVersion("3.1");
    BlockingConnection connection = fuseClient.blockingConnection();
    connection.connect();
    assertFalse(connection.isConnected());
  }

  @Test(expected = MQTTException.class)
  public void testOpenConnectionWithEmptyClientId() throws Exception {
    fuseClient.setClientId("");
    BlockingConnection connection = fuseClient.blockingConnection();
    connection.connect();
    assertFalse(connection.isConnected());
  }

  @Test(expected = MQTTException.class)
  public void testOpenConnectionWithBadCredential() throws Exception {
    fuseClient.setUserName("someone");
    BlockingConnection connection = fuseClient.blockingConnection();
    connection.connect();
    assertFalse(connection.isConnected());
  }

  @Test(expected = MQTTException.class)
  public void testOpenConnectionWithBlockedId() throws Exception {
    fuseClient.setClientId("blocked");
    BlockingConnection connection = fuseClient.blockingConnection();
    connection.connect();
    assertFalse(connection.isConnected());
  }

  @Test
  public void testOpenConnection() throws Exception {
    BlockingConnection connection = fuseClient.blockingConnection();
    connection.connect();
    assertTrue(connection.isConnected());
    connection.disconnect();
  }

  @Test
  public void testSubscribe() throws Exception {
    BlockingConnection subscriber = fuseClient.blockingConnection();
    subscriber.connect();
    Topic[] topics = new Topic[3];
    topics[0] = new Topic("topic/writeonly", QoS.AT_MOST_ONCE);
    topics[1] = new Topic("a/b/c", QoS.AT_LEAST_ONCE);
    topics[2] = new Topic("a/#/c", QoS.EXACTLY_ONCE);
    byte[] bytes = subscriber.subscribe(topics);
    assertEquals((byte) 0x80, bytes[0]);
    assertEquals((byte) 0x01, bytes[1]);
    assertEquals((byte) 0x80, bytes[2]);
    subscriber.disconnect();
  }

  @Test
  public void testPublishMostOnce() throws Exception {
    BlockingConnection subscriber = fuseClient.blockingConnection();
    subscriber.connect();
    Topic[] topics = new Topic[1];
    topics[0] = new Topic("a/b/c", QoS.AT_MOST_ONCE);
    byte[] bytes = subscriber.subscribe(topics);
    assertEquals(QoSType.MOST_ONCE.getByteValue(), bytes[0]);

    BlockingConnection publisher = fuseClient.blockingConnection();
    publisher.connect();
    publisher.publish("a/b/c", payload, QoS.AT_MOST_ONCE, false);

    Message message = subscriber.receive();
    assertEquals("a/b/c", message.getTopic());
    assertArrayEquals(payload, message.getPayload());

    subscriber.disconnect();
    publisher.disconnect();
  }

  @Test
  public void testPublishLeastOnce() throws Exception {
    BlockingConnection subscriber = fuseClient.blockingConnection();
    subscriber.connect();
    Topic[] topics = new Topic[1];
    topics[0] = new Topic("a/b/c", QoS.AT_LEAST_ONCE);
    byte[] bytes = subscriber.subscribe(topics);
    assertEquals(QoSType.LEAST_ONCE.getByteValue(), bytes[0]);

    BlockingConnection publisher = fuseClient.blockingConnection();
    publisher.connect();
    publisher.publish("a/b/c", payload, QoS.AT_LEAST_ONCE, false);

    Message message = subscriber.receive();
    assertEquals("a/b/c", message.getTopic());
    assertArrayEquals(payload, message.getPayload());

    subscriber.disconnect();
    publisher.disconnect();
  }

  @Test
  public void testPublishExactOnce() throws Exception {
    BlockingConnection subscriber = fuseClient.blockingConnection();
    subscriber.connect();
    Topic[] topics = new Topic[1];
    topics[0] = new Topic("a/b/c", QoS.EXACTLY_ONCE);
    byte[] bytes = subscriber.subscribe(topics);
    assertEquals(QoSType.EXACT_ONCE.getByteValue(), bytes[0]);

    BlockingConnection publisher = fuseClient.blockingConnection();
    publisher.connect();
    publisher.publish("a/b/c", payload, QoS.EXACTLY_ONCE, false);

    Message message = subscriber.receive();
    assertEquals("a/b/c", message.getTopic());
    assertArrayEquals(payload, message.getPayload());

    subscriber.disconnect();
    publisher.disconnect();
  }

}
