package io.mqute.server.netty;

import com.google.protobuf.ByteString;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

import io.mqute.IMqttChannel;
import io.mqute.core.MqttChannelRegistry;
import io.mqute.msg.ConnAckMessage;
import io.mqute.msg.DownstreamMessageOuterClass;
import io.mqute.msg.MqttMessage;
import io.mqute.msg.OutgoingMessageWrapperOuterClass;
import io.mqute.msg.enums.ConnectReturnCode;
import io.mqute.msg.serialization.MqttSerialization;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;

/**
 * Created by zhuwang on 8/17/16.
 */
public class CallbackUdpServerTest {

  private static final int PORT = 2171;
  private final CallbackUdpServer server = new CallbackUdpServer(PORT);

  private IMqttChannel<MqttMessage> channel;

  @Before
  @SuppressWarnings("unchecked")
  public void beforeEach() throws Exception {
    channel = (IMqttChannel<MqttMessage>) mock(IMqttChannel.class);
    MqttChannelRegistry.getInstance().registerChannel("abc", channel);
    server.start();
  }

  @After
  public void afterEach() throws Exception {
    server.stop();
  }

  @Test
  public void testReceive() throws Exception {
    BlockingQueue<MqttMessage> queue = new LinkedBlockingDeque<>();
    MqttMessage message = new ConnAckMessage(ConnectReturnCode.ACCEPTED, false);
    doAnswer(invocation -> queue.offer((MqttMessage) invocation.getArguments()[0])).when(channel).write(Matchers.any(MqttMessage.class));
    send("abc", message);
    MqttMessage received = queue.take();
    assertEquals(message, received);

    send("abc", message);
    received = queue.take();
    assertEquals(message, received);
  }

  private void send(String clientId, MqttMessage message) throws IOException {
    try (DatagramSocket socket = new DatagramSocket()) {
      ByteBuffer byteBuffer = MqttSerialization.getInstance().serialize(message);
      byte[] bytes = OutgoingMessageWrapperOuterClass.OutgoingMessageWrapper.newBuilder()
          .setClientID(clientId)
          .setOriginalMessage(ByteString.copyFrom(byteBuffer))
          .build()
          .toByteArray();
      InetAddress address = InetAddress.getLocalHost();
      DatagramPacket packet = new DatagramPacket(bytes, bytes.length, address, PORT);
      socket.send(packet);
    }
  }

}