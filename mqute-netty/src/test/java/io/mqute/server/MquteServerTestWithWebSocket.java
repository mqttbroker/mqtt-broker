package io.mqute.server;


import io.mqute.IMessageDispatcher;
import io.mqute.config.MquteConfig;
import io.mqute.core.MessageDispatcherRegistry;
import io.mqute.msg.ConnAckMessage;
import io.mqute.msg.ConnectMessage;
import io.mqute.msg.DisconnectMessage;
import io.mqute.msg.enums.ConnectReturnCode;
import io.mqute.msg.serialization.MqttSerialization;
import io.mqute.server.mock.MQTTWebSocket;
import io.mqute.server.mock.MockCallbackDispatcher;
import io.mqute.server.mock.RandomPortConfig;
import io.mqute.server.netty.CallbackUdpServer;
import io.mqute.server.netty.MquteNettyServer;
import org.eclipse.jetty.websocket.client.ClientUpgradeRequest;
import org.eclipse.jetty.websocket.client.WebSocketClient;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.lang.reflect.Field;
import java.net.URI;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class MquteServerTestWithWebSocket {

  private static IMqttServer server;
  private static IMqttServer udpServer;
  private static MquteConfig mquteConfig;

  @BeforeClass
  public static void beforeAll() {
    mquteConfig = new RandomPortConfig();
    udpServer = new CallbackUdpServer(mquteConfig.downstreamPort());
    udpServer.start();
    server = new MquteNettyServer(mquteConfig);
    server.start();
    IMessageDispatcher dispatcher = new MockCallbackDispatcher("localhost", mquteConfig.downstreamPort());
    MessageDispatcherRegistry.register(dispatcher);
  }

  @AfterClass
  public static void afterAll() throws Exception {
    Field field = MessageDispatcherRegistry.class.getDeclaredField("dispatcher");
    field.setAccessible(true);
    field.set(null, null);
    field.setAccessible(false);
    server.stop();
    udpServer.stop();
  }

  @Test
  public void testOpenConnection() throws Exception {
    URI destUri = new URI("ws://localhost:" + mquteConfig.webSocketPort() + "/mqtt");
    WebSocketClient client = new WebSocketClient();
    MQTTWebSocket socket = new MQTTWebSocket();
    client.start();
    ClientUpgradeRequest request = new ClientUpgradeRequest();
    client.connect(socket, destUri, request);
    assertTrue(socket.awaitConnected(4, TimeUnit.SECONDS));

    ConnectMessage message = new ConnectMessage("MQTT", (byte) 0x04, (short) 10, true, "TestClient" + System.nanoTime());
    socket.sendBytes(MqttSerialization.getInstance().serialize(message));
    ConnAckMessage ack = MqttSerialization.getInstance().deserialize(socket.receivedBytes());
    assertEquals(new ConnAckMessage(ConnectReturnCode.ACCEPTED, false), ack);

    socket.sendBytes(MqttSerialization.getInstance().serialize(new DisconnectMessage()));
    socket.close();
  }
}
