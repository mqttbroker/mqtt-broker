package io.mqute.server;

import io.mqute.core.BaseMquteChannel;
import io.mqute.msg.ConnectMessage;
import io.mqute.msg.MqttMessage;
import io.mqute.security.IAuthenticator;
import io.mqute.security.IAuthorizor;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.timeout.IdleStateHandler;


public class NettyMquteChannel extends BaseMquteChannel {

  private final ChannelHandlerContext context;

  public NettyMquteChannel(ChannelHandlerContext context,
                           ConnectMessage connectMsg,
                           IAuthenticator authenticator,
                           IAuthorizor authorizor) {

    super(authenticator, authorizor);
    this.context = context;
    openConnection(connectMsg);
  }

  @Override
  public void write(MqttMessage msg) {
    context.writeAndFlush(msg);
  }

  @Override
  public void close() {
    super.close();
    context.close();
  }
}
