package io.mqute.server;


import java.net.InetAddress;

import io.mqute.IMessageDispatcher;
import io.mqute.config.MquteConfig;
import io.mqute.config.impl.MquteConfigImpl;
import io.mqute.core.MessageDispatcherRegistry;
import io.mqute.kafka.producer.KafkaMessageDispatcher;
import io.mqute.server.netty.CallbackUdpServer;
import io.mqute.server.netty.MquteNettyServer;

public class ConnectionServerMain {

  public static void main(String[] args) {
    try {
      MquteConfig config = new MquteConfigImpl();
      IMessageDispatcher dispatcher = new KafkaMessageDispatcher(
          InetAddress.getLocalHost().getHostAddress(), config.downstreamPort());
      MessageDispatcherRegistry.register(dispatcher);
      final IMqttServer callbackUdpServer = new CallbackUdpServer(config.downstreamPort());
      callbackUdpServer.start();
      final IMqttServer server = new MquteNettyServer(config);
      server.start();
      Runtime.getRuntime().addShutdownHook(new Thread() {
        @Override
        public void run() {
          server.stop();
          callbackUdpServer.stop();
        }
      });
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
