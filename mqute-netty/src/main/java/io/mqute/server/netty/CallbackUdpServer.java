package io.mqute.server.netty;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.mqute.core.MqttChannelRegistry;
import io.mqute.msg.DownstreamMessageOuterClass;
import io.mqute.msg.MqttMessage;
import io.mqute.msg.OutgoingMessageWrapperOuterClass;
import io.mqute.msg.serialization.MqttSerialization;
import io.mqute.server.IMqttServer;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.DatagramPacket;
import io.netty.channel.socket.nio.NioDatagramChannel;


public class CallbackUdpServer implements IMqttServer {

  private static final Logger logger = LoggerFactory.getLogger(CallbackUdpServer.class);
  private static final long CLOSE_TIMEOUT_MILLIS = 10000;
  private final int port;
  private final NioEventLoopGroup eventLoopGroup;
  private ChannelFuture bindingChannel;

  public CallbackUdpServer(int port) {
    this.port = port;
    eventLoopGroup = new NioEventLoopGroup();
  }

  @Override
  public void start() {
    Bootstrap bootstrap = new Bootstrap()
        .group(new NioEventLoopGroup())
        .channel(NioDatagramChannel.class)
        .option(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT)
        .handler(new ChannelInitializer<NioDatagramChannel>() {
          @Override
          protected void initChannel(NioDatagramChannel ch) throws Exception {
            ch.pipeline().addLast(new CallbackMessageHandler());
          }
        });

    try {
      bindingChannel = bootstrap.bind(port).sync();
      logger.info("Bound to host:{}, port: {}", bindingChannel.channel().localAddress(), port);
    } catch (Exception e) {
      logger.info("Failed to bind to port:{}", port, e);
    }
  }

  @Override
  public void stop() {
    if (bindingChannel == null) {
      throw new IllegalStateException("Server must be start first");
    }
    try {
      bindingChannel.channel().close().sync();
      eventLoopGroup.shutdownGracefully().await(CLOSE_TIMEOUT_MILLIS);
    } catch (InterruptedException e) {
      logger.warn("Error encountered when closing the UDP server", e);
    }
  }

  private static class CallbackMessageHandler extends ChannelInboundHandlerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(CallbackMessageHandler.class);

    @Override
    public void channelRead(ChannelHandlerContext context, Object message) throws Exception {
      logger.info("[downstream] message received:{}", message);
      if (message instanceof DatagramPacket) {
        DatagramPacket packet = (DatagramPacket) message;
        ByteBuf byteBuf = packet.content();
        byte[] bytes = new byte[byteBuf.readableBytes()];
        byteBuf.getBytes(0, bytes);
        OutgoingMessageWrapperOuterClass.OutgoingMessageWrapper wrappedMessage =
            OutgoingMessageWrapperOuterClass.OutgoingMessageWrapper.parseFrom(bytes);
        MqttMessage mqttMessage = MqttSerialization.getInstance()
            .deserialize(wrappedMessage.getOriginalMessage().asReadOnlyByteBuffer());
        handleMessage(wrappedMessage.getClientID(), mqttMessage);
      } else {
        logger.error("Cannot recognize the message {}", message);
      }
    }

    private void handleMessage(String clientId, MqttMessage mqttMessage) {
      // for wrapped messages, simply get original channel and write it out
      MqttChannelRegistry.getInstance().getChannel(clientId)
          .ifPresent(channel -> {
            logger.info("[downstream] handler trying to write:{}", mqttMessage);
            channel.write(mqttMessage);
          });
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
      logger.error("Error in handling incoming message", cause);
      ctx.close();
    }
  }
}
