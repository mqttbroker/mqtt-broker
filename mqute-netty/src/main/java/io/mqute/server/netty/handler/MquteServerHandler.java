package io.mqute.server.netty.handler;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

import io.mqute.IMqttChannel;
import io.mqute.config.MquteConfig;
import io.mqute.core.MqttChannelRegistry;
import io.mqute.msg.ConnectMessage;
import io.mqute.msg.MqttMessage;
import io.mqute.msg.enums.MessageType;
import io.mqute.server.NettyMquteChannel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.AttributeKey;

@ChannelHandler.Sharable
public class MquteServerHandler extends ChannelInboundHandlerAdapter {

  private static final Logger LOGGER = LoggerFactory.getLogger(MquteServerHandler.class);

  private static final AttributeKey<String> ATTR_KEY_CLIENT_ID = AttributeKey.valueOf("client_id");

  private final MquteConfig config;

  public MquteServerHandler(MquteConfig config) {
    this.config = config;
  }

  @Override
  public void channelRead(ChannelHandlerContext context, Object message) throws Exception {
    if (message instanceof MqttMessage) {
      MqttMessage mqttMessage = (MqttMessage) message;
      if (mqttMessage.getMessageType() == MessageType.CONNECT) {
        handleConnectMessage(context, (ConnectMessage) mqttMessage);
      } else {
        handleMessagesAfterConnect(context, mqttMessage);
      }
    } else {
      LOGGER.error("Cannot accept message {}", message);
    }
  }

  private void handleConnectMessage(ChannelHandlerContext context, ConnectMessage message) {
    Optional<String> clientIdOpt = Optional
        .ofNullable(context.channel().attr(ATTR_KEY_CLIENT_ID).get());
    if (clientIdOpt.isPresent()) {
      // the MQTT connection was already opened. Ignore
      LOGGER.warn("Getting CONNECT message again in the same channel from {}, old id {}",
          message.getClientId(), clientIdOpt.get());
    } else {
      // Get CONNECT message. Will try to start a MQTT connection
      IMqttChannel<MqttMessage> channel = new NettyMquteChannel(context, message,
          config.authenticator().orElse(null), config.authorizor().orElse(null));
      context.channel().attr(ATTR_KEY_CLIENT_ID).set(channel.getClientId());
    }
  }

  private void handleMessagesAfterConnect(ChannelHandlerContext context, MqttMessage message) {
    Optional<IMqttChannel<MqttMessage>> channelOpt = Optional
        .ofNullable(context.channel().attr(ATTR_KEY_CLIENT_ID).get())
        .flatMap(MqttChannelRegistry.getInstance()::getChannel);
    if (channelOpt.isPresent()) {
      channelOpt.get().handleMessage(message);
    } else {
      // no MQTT channel created but get messages other than CONNECT, close the connection
      context.close();
    }
  }

  @Override
  public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
    LOGGER.error("Error in handling incoming message", cause);
    ctx.close();
  }
}
