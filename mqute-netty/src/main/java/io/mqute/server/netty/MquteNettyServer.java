package io.mqute.server.netty;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;

import io.mqute.config.MquteConfig;
import io.mqute.config.impl.MquteConfigImpl;
import io.mqute.server.IMqttServer;
import io.mqute.server.ISSLContextProvider;
import io.mqute.server.netty.handler.ByteBufToWebSocketFrameEncoder;
import io.mqute.server.netty.handler.MQTTDecoder;
import io.mqute.server.netty.handler.MQTTEncoder;
import io.mqute.server.netty.handler.MquteServerHandler;
import io.mqute.server.netty.handler.WebSocketFrameToByteBufDecoder;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.ssl.SslHandler;

public class MquteNettyServer implements IMqttServer {

  private static final long CLOSE_TIMEOUT_MILLIS = 10000;
  private static Logger logger = LoggerFactory.getLogger(MquteNettyServer.class);
  private final MquteConfig config;
  private EventLoopGroup bossGroup;
  private EventLoopGroup workerGroup;
  private List<ChannelFuture> bindingChannels;

  public MquteNettyServer() {
    this(null);
  }

  public MquteNettyServer(MquteConfig config) {
    this.config = Optional.ofNullable(config).orElseGet(MquteConfigImpl::new);
  }

  @Override
  public void start() {
    bossGroup = new NioEventLoopGroup();
    workerGroup = new NioEventLoopGroup();
    bindingChannels = new LinkedList<>();
    final MquteServerHandler handler = new MquteServerHandler(config);
    bind(config.tcpPort(), initTCPChannel(handler)).ifPresent(bindingChannels::add);
    bind(config.webSocketPort(), initWebSocketChannel(handler)).ifPresent(bindingChannels::add);
    config.sslContextProvider().map(ISSLContextProvider::getSSLContext).ifPresent(sslContext -> {
      bind(config.sslPort(), initSSLChannel(handler, sslContext)).ifPresent(bindingChannels::add);
      bind(config.secureWebSocketPort(), initSecureWebSocketChannel(handler, sslContext))
          .ifPresent(bindingChannels::add);
    });
  }

  private Optional<ChannelFuture> bind(int port, Consumer<SocketChannel> initChannel) {
    ChannelFuture channelFuture = null;
    ServerBootstrap b = new ServerBootstrap();
    b.group(bossGroup, workerGroup)
        .channel(NioServerSocketChannel.class)
        .childHandler(new ChannelInitializer<SocketChannel>() {
          @Override
          public void initChannel(SocketChannel ch) throws Exception {
            initChannel.accept(ch);
          }
        })
        .option(ChannelOption.SO_BACKLOG, 128)
        .option(ChannelOption.SO_REUSEADDR, true)
        .option(ChannelOption.TCP_NODELAY, true)
        .childOption(ChannelOption.SO_KEEPALIVE, true);
    // Bind and start to accept incoming connections.
    try {
      channelFuture = b.bind(port).sync();
      logger.info("Bound to host:{}, port: {}", channelFuture.channel().localAddress(), port);
    } catch (Exception e) {
      logger.info("Failed to bind to port:{}", port, e);
    }
    return Optional.ofNullable(channelFuture);
  }

  private Consumer<SocketChannel> initTCPChannel(final MquteServerHandler handler) {
    return (SocketChannel channel) -> {
      ChannelPipeline pipeline = channel.pipeline();
      pipeline.addLast("decoder", new MQTTDecoder());
      pipeline.addLast("encoder", new MQTTEncoder());
      pipeline.addLast("handler", handler);
    };
  }

  private Consumer<SocketChannel> initWebSocketChannel(final MquteServerHandler handler) {
    return (SocketChannel channel) -> {
      initTCPChannel(handler).accept(channel);
      ChannelPipeline pipeline = channel.pipeline();
      pipeline.addBefore("decoder", "httpCodec", new HttpServerCodec());
      pipeline.addBefore("decoder", "aggregator", new HttpObjectAggregator(65536));
      pipeline.addBefore("decoder", "webSocketHandler",
          new WebSocketServerProtocolHandler("/mqtt", "mqtt, mqttv3.1, mqttv3.1.1", true));
      pipeline.addBefore("decoder", "ws2bytebufDecoder", new WebSocketFrameToByteBufDecoder());
      pipeline.addBefore("decoder", "bytebuf2wsEncoder", new ByteBufToWebSocketFrameEncoder());
    };
  }

  private Consumer<SocketChannel> initSSLChannel(final MquteServerHandler handler,
                                                 SSLContext sslContext) {
    return (SocketChannel channel) -> {
      initTCPChannel(handler).accept(channel);
      ChannelPipeline pipeline = channel.pipeline();
      pipeline.addAfter("idleEventHandler", "sslHandler", sslHandler(sslContext));
    };
  }

  private Consumer<SocketChannel> initSecureWebSocketChannel(final MquteServerHandler handler,
                                                             SSLContext sslContext) {
    return (SocketChannel channel) -> {
      initWebSocketChannel(handler).accept(channel);
      ChannelPipeline pipeline = channel.pipeline();
      pipeline.addAfter("idleEventHandler", "sslHandler", sslHandler(sslContext));
    };
  }

  private ChannelHandler sslHandler(SSLContext sslContext) {
    SSLEngine sslEngine = sslContext.createSSLEngine();
    sslEngine.setUseClientMode(false);
    return new SslHandler(sslEngine);
  }

  @Override
  public void stop() {
    try {
      bindingChannels.forEach(channelFuture -> {
        try {
          channelFuture.channel().close().sync();
        } catch (InterruptedException e) {
          logger.warn("Error in closing channel");
        }
      });
      workerGroup.shutdownGracefully().await(CLOSE_TIMEOUT_MILLIS);
      bossGroup.shutdownGracefully().await(CLOSE_TIMEOUT_MILLIS);
    } catch (InterruptedException e) {
      logger.warn("Error encountered when closing the server", e);
    }
  }

}

