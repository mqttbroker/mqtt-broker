package io.mqute.server.netty.handler;

import io.mqute.msg.MqttMessage;
import io.mqute.msg.serialization.MqttSerialization;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;


public class MQTTEncoder extends MessageToByteEncoder<MqttMessage> {

  private static Logger logger = LoggerFactory.getLogger(MQTTEncoder.class);

  @Override
  protected void encode(ChannelHandlerContext context, MqttMessage message, ByteBuf out)
      throws Exception {
    try {
      ByteBuffer byteBuffer = MqttSerialization.getInstance().serialize(message);
      out.writeBytes(byteBuffer);
    } catch (Exception e) {
      logger.error("Error in encoding the MQTT message {}", message, e);
      context.close();
    }
  }
}
