package io.mqute.server.netty.handler;

import io.mqute.msg.MqttMessage;
import io.mqute.msg.serialization.MqttSerialization;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.List;


public class MQTTDecoder extends ByteToMessageDecoder {

  private static Logger logger = LoggerFactory.getLogger(MQTTDecoder.class);

  @Override
  protected void decode(ChannelHandlerContext context, ByteBuf in, List<Object> out) throws Exception {
    if (in.capacity() == 0) return;
    ByteBuffer byteBuffer = in.nioBuffer();
    try {
      MqttMessage message = MqttSerialization.getInstance().deserialize(byteBuffer);
      out.add(message);
    } catch (Exception e) {
      logger.error("Error in decoding the byte stream", e);
      context.close();
    } finally {
      in.skipBytes(byteBuffer.position());
    }
  }
}
