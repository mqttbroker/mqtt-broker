package io.mqute.security;


/**
 * An Authenticator contains the logic to validate the username and password field
 * in the CONNECT packet.
 */
public interface IAuthenticator {

  /**
   * Validate the username and password.
   *
   * @param username username field in CONNECT packet
   * @param password password field in CONNECT packet
   * @return if the username and password combination is valid.
   */
  boolean checkValid(String username, byte[] password);
}
