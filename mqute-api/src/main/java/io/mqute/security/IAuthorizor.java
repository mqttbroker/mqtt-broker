package io.mqute.security;


/**
 * An Authorizor determines the following.
 * <p/>
 * 1. If the broker should accept the connection from a given clientId
 * 2. If the broker should let the client publish to a given topic under a certain username
 * 3. If the broker should let the client subscribe to a give topic under a certain username
 */
public interface IAuthorizor {

  /**
   * Check if the client can publish to the given topic using a certain username.
   *
   * @param topic    the topic need to be published to
   * @param user     the username that is used to open the connection
   * @param clientId id of the client that connects to the broker
   * @return the write permission
   */
  boolean canWrite(String topic, String user, String clientId);

  /**
   * Check if the client can subscribe to the given topic using a certain username.
   *
   * @param topic    the topic need to subscribed to
   * @param user     the username that is used to open the connection
   * @param clientId id of the client that connects to the broker
   * @return the read permission
   */
  boolean canRead(String topic, String user, String clientId);

  /**
   * Check if the client is allowed to connect to the broker.
   *
   * @param clientId if of the client that wants to connection to the broker
   * @return the connect permission
   */
  boolean canConnect(String clientId);
}
