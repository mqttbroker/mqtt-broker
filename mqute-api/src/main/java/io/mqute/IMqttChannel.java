package io.mqute;

/**
 * An IMqttChannel represents the duplex channel over which messages are sent between the client and
 * broker.
 *
 * @param <T> the type of messages that is sent over this channel
 */
public interface IMqttChannel<T> {

  /**
   * Handle incoming messages.
   */
  void handleMessage(T msg);

  /**
   * Send message back to the client.
   *
   * @param msg the message needs to be sent
   */
  void write(T msg);

  /**
   * Keep alive update after connect.
   */
  void keepAlive();

  /**
   * Close the MQTT connection to the client.
   */
  void close();

  /**
   * Check if the channel is still active.
   *
   * @return if the channel is still active
   */
  boolean isActive();

  /**
   * Get the clientId that opens this channel.
   *
   * @return clientId
   */
  String getClientId();
}
