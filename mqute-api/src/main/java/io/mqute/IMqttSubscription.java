package io.mqute;

/**
 * An IMqttSubscription represents the subscription created by a SUBSCRIBE request.
 */
public interface IMqttSubscription {

  /**
   * Get the client id that sends SUBSCRIBE request.
   *
   * @return client id
   */
  String getClientId();

  /**
   * Get the topic filter on which the subscription is appended.
   *
   * @return the topic filter
   */
  String getTopicFilter();

  /**
   * Get the QoS of this subscription.
   *
   * @return QoS
   */
  byte getQoS();
}
