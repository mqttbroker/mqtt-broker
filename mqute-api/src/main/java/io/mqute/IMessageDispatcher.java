package io.mqute;

import java.util.concurrent.CompletableFuture;

/**
 * An IMessageDispatcher works as a dual agent by dispatching the message for publishers that
 * connect to this host as well as receiving the message for subscribers that connect to this host.
 */
public interface IMessageDispatcher {

  /**
   * Dispatch the message to subscribers.
   *
   * @param message   the message need to be dispatched
   * @param clientId  the client id from which the message is coming
   * @param <Message> the type of publishing message
   * @return a completable future resolved once the message is acknowledged
   */
  <Message> CompletableFuture<Object> dispatch(Message message, String clientId, String... targets);

  /**
   * Dispatch the message to subscribers.
   *
   * @param message   the message need to be dispatched
   * @param clientId  the client id from which the message is coming
   * @param partition the partition id explicitly assigned to the message
   * @return a completable future resolved once the message is acknowledged
   */
  <Message> CompletableFuture<Object> dispatch(Message message, String clientId, String kafkaTopic, int partition, String... targets);

  /**
   * Keep the connection alive by telling the downstream processor.
   *
   * @param clientId the client id for the connection
   */
  void keepAlive(String clientId);

}
