package io.mqute.server;


import javax.net.ssl.SSLContext;

/**
 * Provider of {@link SSLContext}.
 */
public interface ISSLContextProvider {

  /**
   * Get the provided {@link SSLContext}.
   * @return {@link SSLContext}
   */
  SSLContext getSSLContext();
}
