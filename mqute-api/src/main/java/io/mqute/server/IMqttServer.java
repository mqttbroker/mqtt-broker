package io.mqute.server;

/**
 * An IMqttServer will bind to TCP port and accept MQTT connections.
 */
public interface IMqttServer {

  /**
   * Start the server.
   */
  void start();

  /**
   * Stop the server.
   */
  void stop();

}
