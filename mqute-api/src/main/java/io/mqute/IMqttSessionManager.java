package io.mqute;

import java.util.Optional;


/**
 * An IMqttSessionManager manages all {@link IMqttSession}.
 *
 */
public interface IMqttSessionManager {

  /**
   * Load the session if it is persisted.
   *
   * @param clientId the id of the client who starts the session
   * @return optional of session
   */
  Optional<IMqttSession> loadSession(String clientId);

  /**
   * Persist the session
   *
   * @param session the session needs to be persisted.
   */
  void saveSession(IMqttSession session);

  /**
   * Remove the session from persistence service.
   *
   * @param clientId the id of the client who starts the session
   */
  void cleanSession(String clientId);
}
