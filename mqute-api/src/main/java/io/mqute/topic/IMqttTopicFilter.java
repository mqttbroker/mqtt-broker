package io.mqute.topic;


/**
 * An IMqttTopicFilter represents the hierarchy of topic names used in {@link IMqttTopicNode}.
 *
 * @param <T> Sub-type of the {@code IMqttTopicFilter}
 */
public interface IMqttTopicFilter<T extends IMqttTopicFilter<?>> {

  /**
   * Get the tail part of the topic filter expect the first section.
   *
   * @return the tail part of the topic filter expect the first section
   */
  T tail();

  /**
   * Get the top section of current topic filter.
   *
   * @return the top section of current topic filter
   */
  String head();

  /**
   * Check if the current topic filter has no more child sections.
   *
   * @return if the current topic filter has no more child sections
   */
  boolean isLeaf();

  /**
   * Get the last section of the topic filter.
   *
   * @return the last section of the topic filter
   */
  T leaf();
}
