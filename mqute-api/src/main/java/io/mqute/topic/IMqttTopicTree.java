package io.mqute.topic;

import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * This interface represents the tree structure constructed by {@link IMqttTopicNode}. It contains
 * the logic to publish the message to all matched nodes and append a subscription to a designated
 * node.
 *
 * @param <Message>      Type of the message to be published
 * @param <Subscription> Type of the subscription
 */
public interface IMqttTopicTree<Message, Subscription> {

  /**
   * Publish the message to a designated {@link IMqttTopicNode} in the tree
   * according to the topicFilter
   *
   * @param topic        the path to the leaf node from root
   * @param message      The message need to be publish to all subscriptions in the leaf node
   * @param shouldRetain If this message needs to be retained
   * @param <T>          Implementation of IMqttTopicFilter
   */
  <T extends IMqttTopicFilter<T>> List<CompletableFuture<Void>> publish(T topic,
                                                                        Message message,
                                                                        boolean shouldRetain);

  /**
   * Attach the subscription to a designated {@link IMqttTopicNode} in the tree
   * according to the topicFilter.
   *
   * @param topicFilter  the path to the leaf node from root
   * @param subscription The subscription that needs to be appended to the leaf node
   * @param <T>          Implementation of IMqttTopicFilter
   */
  <T extends IMqttTopicFilter<T>> void subscribe(T topicFilter, Subscription subscription);
}
