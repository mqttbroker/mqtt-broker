package io.mqute.topic;


import java.util.Collection;
import java.util.Map;

/**
 * An IMqttTopicNode is the node structure
 * that constructs a topic tree according to the topic sections.
 *
 * @param <Subscription> The type of subscriptions attached to the topic
 * @param <Node> The actual implementation class
 */
public interface IMqttTopicNode<Subscription, Node extends IMqttTopicNode<Subscription, Node>> {

  /**
   * Get the children of this node.
   *
   * @return children {@code IMqttTopicNode}
   */
  Map<String, Node> getChildren();

  /**
   * Get all the subscriptions appended to the topic.
   *
   * @return collection of subscriptions
   */
  Collection<Subscription> getSubscriptions();

  /**
   * Get or create a topic node for given topic filter
   *
   * @param topicFilter the path to the leaf node from current node
   * @param <T>         Implementation of IMqttTopicFilter
   * @return the {@code IMqttTopicNode} represents the given topic filter
   */
  <T extends IMqttTopicFilter<T>> Node locate(T topicFilter);

  /**
   * Get a collection of topic node matches the given topic
   *
   * @param topic the path to the leaf node from current node
   * @param <T>   Implementation of IMqttTopicFilter
   * @return collection of matched nodes
   */
  <T extends IMqttTopicFilter<T>> Collection<Node> filter(T topic);

  /**
   * Append a subscription to this topic node.
   *
   * @param clientId     the client that sends subscribe request
   * @param subscription the subscription to this topic
   */
  void append(String clientId, Subscription subscription);

  /**
   * Remove a subscription for a given client.
   *
   * @param clientId client that sends unsubscribe request
   * @return removed subscription
   */
  Subscription remove(final String clientId);

}
