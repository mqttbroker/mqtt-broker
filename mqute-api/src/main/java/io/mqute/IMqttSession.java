package io.mqute;

import java.util.List;


/**
 * An IMqttSession is mapped to an MQTT client connection. It needs to store every subscriptions for
 * SUBSCRIBE request.
 */
public interface IMqttSession {

  /**
   * If the clean session flag is set.
   *
   * @return if the session needs to be persisted
   */
  boolean cleanSession();

  /**
   * Get the client id associated with this session.
   *
   * @return id of the client for this session.
   */
  String getClientId();

  /**
   * Get the keep alive time of this session.
   *
   * @return the keep alive time of this session
   */
  short getKeepAlive();

  /**
   * Get all subscriptions requested in this session.
   *
   * @return all the subscriptions that are created in this session.
   */
  List<IMqttSubscription> getSubscriptions();

  /**
   * Add subscription to the session.
   *
   * @param subscription the subscription created in this session
   */
  void addSubscription(IMqttSubscription subscription);

  /**
   * Terminate the session.
   *
   * @param graceful whether the session is terminated by a DISCONNECT message or not
   */
  void terminate(boolean graceful);

}
