package io.mqute.core;

import io.mqute.IMqttSubscription;
import io.mqute.msg.enums.QoSType;


public class MquteSubscription implements IMqttSubscription {

  private final String clientId;
  private final String topicFilter;
  private final QoSType qos;

  public MquteSubscription(String clientId, String topicFilter, QoSType qos) {
    this.clientId = clientId;
    this.topicFilter = topicFilter;
    this.qos = qos;
  }

  @Override
  public String getClientId() {
    return clientId;
  }

  @Override
  public String getTopicFilter() {
    return topicFilter;
  }

  @Override
  public byte getQoS() {
    return qos.getByteValue();
  }
}
