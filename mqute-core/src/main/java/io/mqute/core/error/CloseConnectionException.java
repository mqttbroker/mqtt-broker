package io.mqute.core.error;

import java.io.IOException;

/**
 * The exception is thrown to close the existing TCP connection.
 * The thrower doesn't have the context to close the connection.
 * The caller should close the connection when catches this exception.
 */
public class CloseConnectionException extends IOException {

  private static final long serialVersionUID = -4274276298326136670L;

  public CloseConnectionException() {
    super("The connection will be closed according to the MQTT spec");
  }
}
