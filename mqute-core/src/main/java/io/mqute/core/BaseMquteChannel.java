package io.mqute.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import io.mqute.IMessageDispatcher;
import io.mqute.IMqttChannel;
import io.mqute.msg.ConnAckMessage;
import io.mqute.msg.ConnectMessage;
import io.mqute.msg.Credential;
import io.mqute.msg.MqttMessage;
import io.mqute.msg.PingRespMessage;
import io.mqute.msg.PubAckMessage;
import io.mqute.msg.PubCompMessage;
import io.mqute.msg.PubRecMessage;
import io.mqute.msg.PubRelMessage;
import io.mqute.msg.PublishMessage;
import io.mqute.msg.SubAckMessage;
import io.mqute.msg.SubscribeMessage;
import io.mqute.msg.UnsubAckMessage;
import io.mqute.msg.UnsubscribeMessage;
import io.mqute.msg.enums.ConnectReturnCode;
import io.mqute.msg.enums.QoSType;
import io.mqute.msg.serialization.util.ProtocolUtils;
import io.mqute.security.IAuthenticator;
import io.mqute.security.IAuthorizor;


public abstract class BaseMquteChannel implements IMqttChannel<MqttMessage> {

  private static final Logger LOGGER = LoggerFactory.getLogger(BaseMquteChannel.class);

  private IAuthenticator authenticator;
  private IAuthorizor authorizor;
  private boolean channelActive;

  private String user;
  private String clientId;
  private short keepAlive;
  private volatile short idleCounter;
  private final IMessageDispatcher dispatcher;

  public BaseMquteChannel(ConnectMessage connectMsg) {
    this(null, null);
  }

  public BaseMquteChannel(IAuthenticator authenticator, IAuthorizor authorizor) {
    this.channelActive = false;
    this.authenticator = authenticator;
    this.authorizor = authorizor;
    dispatcher = MessageDispatcherRegistry.getDispatcher();
  }

  public void openConnection(ConnectMessage connectMsg) {
    user = connectMsg.getCredentialOpt().map(Credential::getUsername).orElse(null);
    channelActive = true;
    clientId = connectMsg.getClientId();
    MqttChannelRegistry.getInstance().registerChannel(clientId, this);
    // check protocol
    if (!connectMsg.getProtocolName().equals(Constants.MQTT)
        || connectMsg.getProtocolLevel() != Constants.VERSION_3_1_1) {
      ConnAckMessage errorResp = new ConnAckMessage(ConnectReturnCode.BAD_PROTOCOL_VERSION, false);
      write(errorResp);
      close();
      return;
    }
    // check client id
    if (connectMsg.getClientId() == null || connectMsg.getClientId().length() == 0) {
      ConnAckMessage errorResp = new ConnAckMessage(ConnectReturnCode.ID_REJECTED, false);
      write(errorResp);
      close();
      return;
    }
    // client authorization
    boolean canConnect = Optional.ofNullable(authorizor)
        .map(auth -> auth.canConnect(connectMsg.getClientId())).orElse(true);
    if (!canConnect) {
      ConnAckMessage errorResp = new ConnAckMessage(ConnectReturnCode.NOT_AUTHORIZED, false);
      write(errorResp);
      close();
      return;
    }
    // user authentication
    boolean authenticated = Optional.ofNullable(authenticator).map(auth ->
        connectMsg.getCredentialOpt().map(credential ->
            auth.checkValid(credential.getUsername(), credential.getPassword())
        ).orElse(false)
    ).orElse(true);
    if (!authenticated) {
      ConnAckMessage errorResp =
          new ConnAckMessage(ConnectReturnCode.BAD_USERNAME_OR_PASSWORD, false);
      write(errorResp);
      close();
      return;
    }
    // dispatch the message
    dispatcher.dispatch(connectMsg, connectMsg.getClientId()).exceptionally(e -> {
      LOGGER.error("Failed to dispatch CONNECT message {}. Close the connection", connectMsg, e);
      close();
      return null;
    });
  }

  @Override
  public void handleMessage(MqttMessage message) {
    try {
      switch (message.getMessageType()) {
        case PUBLISH:
          processPublish((PublishMessage) message);
          break;
        case SUBSCRIBE:
          processSubscribe((SubscribeMessage) message);
          break;
        case UNSUBSCRIBE:
          dispatcher.dispatch(message, clientId).thenAccept(v ->
              write(new UnsubAckMessage(((UnsubscribeMessage) message).getPacketId()))
          );
          break;
        case DISCONNECT:
          dispatcher.dispatch(message, clientId);
          close();
          break;
        case PUBACK:
          dispatcher.dispatch(message, clientId);
          break;
        case PUBREL:
          write(new PubCompMessage(((PubRelMessage) message).getPacketId()));
          break;
        case PUBREC:
          dispatcher.dispatch(message, clientId).thenAccept(v ->
              write(new PubRelMessage(((PubRecMessage) message).getPacketId()))
          );
          break;
        case PUBCOMP:
          dispatcher.dispatch(message, clientId);
          break;
        case PINGREQ:
          write(new PingRespMessage());
          break;
        default:
          LOGGER.error("Got unknown message type {}. Close the connection", message.getMessageType());
          close();
          return;
      }
      //send activity notices to keep alive processor
      keepAlive();
    } catch (Exception e) {
      LOGGER.error("Error when handling message {}", message, e);
    }
  }

  private void processSubscribe(SubscribeMessage message) throws IOException {
    LinkedHashMap<String, QoSType> allowedSubscriptions = new LinkedHashMap<>();
    List<QoSType> qoses = new LinkedList<>();
    for (Map.Entry<String, QoSType> entry : message.getSubscriptions().entrySet()) {
      String topicFilter = entry.getKey();
      QoSType qos = entry.getValue();
      if (!ProtocolUtils.validateTopicFilter(topicFilter)) {
        qoses.add(QoSType.FAILURE);
        LOGGER.warn("Topic filter {} in message {} is invalid", topicFilter, message);
        continue;
      }
      boolean canRead = Optional.ofNullable(authorizor)
          .map(auth -> auth.canRead(topicFilter, user, clientId)).orElse(true);
      if (!canRead) {
        qoses.add(QoSType.FAILURE);
        LOGGER.error("Subscribe to the topic {} is not authorized to user {} clientId {}. ",
            topicFilter, user, clientId);
        continue;
      }
      allowedSubscriptions.put(topicFilter, qos);
      qoses.add(qos);
    }
    QoSType[] qoSTypes = qoses.toArray(new QoSType[qoses.size()]);
    dispatcher.dispatch(new SubscribeMessage(message.getPacketId(), allowedSubscriptions), clientId)
        .thenAccept(v -> write(new SubAckMessage(message.getPacketId(), qoSTypes)));
  }

  private void processPublish(PublishMessage message) throws IOException {
    // write permission check
    String topicName = message.getTopicName();
    if (!ProtocolUtils.validateTopicName(topicName)) {
      LOGGER.error("Topic name {} in message {} is invalid. Close connection", topicName, message);
      close();
      return;
    }
    boolean canWrite = Optional.ofNullable(authorizor)
        .map(auth -> auth.canWrite(topicName, user, clientId)).orElse(true);
    if (!canWrite) {
      LOGGER.error("Publish to the topic {} is not authorized to user {} clientId {}. "
          + "Close connection", topicName, user, clientId);
      close();
      return;
    }
    dispatcher.dispatch(message, clientId).thenAccept(v -> {
      if (message.getQos() == QoSType.LEAST_ONCE) {
        write(new PubAckMessage(message.getPacketId()));
      } else if (message.getQos() == QoSType.EXACT_ONCE) {
        write(new PubRecMessage(message.getPacketId()));
      }
    });
  }

  @Override
  public void keepAlive() {
    dispatcher.keepAlive(clientId);
  }

  @Override
  public boolean isActive() {
    return channelActive;
  }

  @Override
  public String getClientId() {
    return clientId;
  }

  @Override
  public void close() {
    channelActive = false;
    MqttChannelRegistry.getInstance().removeChannel(clientId);
  }
}
