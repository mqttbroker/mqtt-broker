package io.mqute.core;


import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import io.mqute.IMqttChannel;
import io.mqute.msg.MqttMessage;

public class MqttChannelRegistry {

  private static MqttChannelRegistry instance;

  private Map<String, IMqttChannel<MqttMessage>> channelMap;

  private MqttChannelRegistry() {
    channelMap = new ConcurrentHashMap<>();
  }

  public static MqttChannelRegistry getInstance() {
    return Optional.ofNullable(instance).orElseGet(() -> {
      synchronized (MqttChannelRegistry.class) {
        if (!Optional.ofNullable(instance).isPresent()) {
          instance = new MqttChannelRegistry();
        }
        return instance;
      }
    });
  }

  public Optional<IMqttChannel<MqttMessage>> getChannel(String clientId) {
    return Optional.ofNullable(channelMap.get(clientId));
  }

  public void registerChannel(String clientId, IMqttChannel<MqttMessage> channel) {
    Optional.ofNullable(channelMap.remove(clientId)).ifPresent(IMqttChannel::close);
    channelMap.putIfAbsent(clientId, channel);
  }

  public void removeChannel(String clientId) {
    channelMap.remove(clientId);
  }

}
