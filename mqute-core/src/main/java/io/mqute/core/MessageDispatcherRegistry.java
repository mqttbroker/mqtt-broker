package io.mqute.core;

import java.util.Optional;

import io.mqute.IMessageDispatcher;

public class MessageDispatcherRegistry {

  private static IMessageDispatcher dispatcher;

  /**
   * Get the registered {@link IMessageDispatcher} instance
   *
   * @return registered dispatcher
   * @throws IllegalStateException if there is not dispatcher registered
   */
  public static IMessageDispatcher getDispatcher() {
    return Optional.ofNullable(dispatcher).orElseThrow(() ->
        new IllegalStateException("MessageDispatcher has not been registered yet"));
  }

  /**
   * Register an instance of {@link IMessageDispatcher}
   *
   * @param dispatcher the dispatcher to be registered
   * @throws IllegalStateException if dispatcher has already been registered
   */
  public static void register(IMessageDispatcher dispatcher) {
    if (Optional.ofNullable(MessageDispatcherRegistry.dispatcher).isPresent()) {
      throw new IllegalStateException("MessageDispatcher has already been registered");
    }
    MessageDispatcherRegistry.dispatcher = dispatcher;
  }

}
