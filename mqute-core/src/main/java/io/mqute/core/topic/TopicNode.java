package io.mqute.core.topic;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import io.mqute.IMqttSubscription;
import io.mqute.core.MquteSubscription;
import io.mqute.topic.IMqttTopicFilter;
import io.mqute.topic.IMqttTopicNode;

public class TopicNode implements IMqttTopicNode<IMqttSubscription, TopicNode> {

  private static final String PLUS_SIGN = "+";
  private static final String HASH_SIGN = "#";

  private final Map<String, IMqttSubscription> subscriptions;
  private final Map<String, TopicNode> children;

  public TopicNode() {
    this(null, null);
  }

  public TopicNode(Map<String, TopicNode> children, Collection<IMqttSubscription> subscriptions) {
    this.children = children == null ? new HashMap<>() : children;
    this.subscriptions = new HashMap<>();
    Optional.ofNullable(subscriptions).ifPresent(subs ->
      subs.forEach(sub -> this.subscriptions.put(sub.getClientId(), sub))
    );
  }

  @Override
  public Map<String, TopicNode> getChildren() {
    return Collections.unmodifiableMap(children);
  }

  @Override
  public Collection<IMqttSubscription> getSubscriptions() {
    return Collections.unmodifiableCollection(subscriptions.values());
  }

  @Override
  public <T extends IMqttTopicFilter<T>> TopicNode locate(
      final T topicFilter) {
    if (topicFilter.isLeaf()) {
      return this;
    }

    String head = topicFilter.head();
    if (!children.containsKey(head)) {
      children.put(head, new TopicNode());
    }

    return children.get(head).locate(topicFilter.tail());
  }

  @Override
  public <T extends IMqttTopicFilter<T>> Collection<TopicNode> filter(
      final T topic) {
    if (topic.isLeaf()) {
      return Collections.singletonList(this);
    }

    List<TopicNode> aggregates = new LinkedList<>();

    if (children.containsKey(HASH_SIGN)) {
      //there cannot be any further children after *
      aggregates.add(children.get(HASH_SIGN));
    }
    if (children.containsKey(PLUS_SIGN)) {
      aggregates.addAll(children.get(PLUS_SIGN).filter(topic.tail()));
    }

    String head = topic.head();
    if (children.containsKey(head)) {
      aggregates.addAll(children.get(head).filter(topic.tail()));
    }

    return Collections.unmodifiableCollection(aggregates);
  }

  @Override
  public void append(String clientId, IMqttSubscription subscription) {
    subscriptions.put(clientId, subscription);
  }

  @Override
  public IMqttSubscription remove(String clientId) {
    return subscriptions.remove(clientId);
  }
}
