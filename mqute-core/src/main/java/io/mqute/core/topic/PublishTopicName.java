package io.mqute.core.topic;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import io.mqute.msg.serialization.util.ProtocolUtils;
import io.mqute.topic.IMqttTopicFilter;

public class PublishTopicName implements IMqttTopicFilter<PublishTopicName> {

  private List<String> sections;

  /**
   * Construct the topic hierarchy from topic name in PUBLISH message.
   *
   * @param topicName topic name in PUBLISH message
   * @throws IllegalArgumentException if the topic name is invalid
   */
  public PublishTopicName(String topicName) {
    if (ProtocolUtils.validateTopicName(topicName)) {
      sections = Collections.unmodifiableList(Arrays.asList(topicName.split("/")));
    } else {
      throw new IllegalArgumentException("Topic name " + topicName + " is invalid");
    }
  }

  private PublishTopicName(List<String> sections) {
    this.sections = sections;
  }

  @Override
  public PublishTopicName tail() {
    return new PublishTopicName(sections.subList(1, sections.size()));
  }

  @Override
  public String head() {
    return sections.get(0);
  }

  @Override
  public boolean isLeaf() {
    return sections.isEmpty();
  }

  @Override
  public PublishTopicName leaf() {
    return new PublishTopicName(Collections.emptyList());
  }
}
