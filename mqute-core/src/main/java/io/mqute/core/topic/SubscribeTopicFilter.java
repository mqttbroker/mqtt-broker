package io.mqute.core.topic;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import io.mqute.msg.serialization.util.ProtocolUtils;
import io.mqute.topic.IMqttTopicFilter;

public class SubscribeTopicFilter implements IMqttTopicFilter<SubscribeTopicFilter> {

  private List<String> sections;

  /**
   * Construct the topic hierarchy from topic filter in SUBSCRIBE message.
   *
   * @param topicFilter topic filter in SUBSCRIBE message
   * @throws IllegalArgumentException if the topic filter is invalid
   */
  public SubscribeTopicFilter(String topicFilter) {
    if (ProtocolUtils.validateTopicFilter(topicFilter)) {
      sections = Collections.unmodifiableList(Arrays.asList(topicFilter.split("/")));
    } else {
      throw new IllegalArgumentException("Topic filter " + topicFilter + " is invalid");
    }
  }

  private SubscribeTopicFilter(List<String> sections) {
    this.sections = sections;
  }

  @Override
  public SubscribeTopicFilter tail() {
    return new SubscribeTopicFilter(sections.subList(1, sections.size()));
  }

  @Override
  public String head() {
    return sections.get(0);
  }

  @Override
  public boolean isLeaf() {
    return sections.isEmpty();
  }

  @Override
  public SubscribeTopicFilter leaf() {
    return new SubscribeTopicFilter(Collections.emptyList());
  }
}
