package io.mqute.core;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.concurrent.CompletableFuture;

import io.mqute.IMessageDispatcher;

import static org.junit.Assert.assertNotNull;


public class MessageDispatcherRegistryTest {

  @Before
  public void beforeEach() throws Exception {
    Field field = MessageDispatcherRegistry.class.getDeclaredField("dispatcher");
    field.setAccessible(true);
    field.set(null, null);
    field.setAccessible(false);
  }

  @After
  public void afterEach() throws Exception {
    Field field = MessageDispatcherRegistry.class.getDeclaredField("dispatcher");
    field.setAccessible(true);
    field.set(null, null);
    field.setAccessible(false);
  }

  @Test
  public void testRegister() throws Exception {
    MessageDispatcherRegistry.register(new DummyDispatcher());
    assertNotNull(MessageDispatcherRegistry.getDispatcher());
  }

  @Test(expected = IllegalStateException.class)
  public void testRegisterAgain() throws Exception {
    MessageDispatcherRegistry.register(new DummyDispatcher());
    MessageDispatcherRegistry.register(new DummyDispatcher());
  }

  @Test(expected = IllegalStateException.class)
  public void testGetDispatcherBeforeRegister() throws Exception {
    MessageDispatcherRegistry.getDispatcher();
  }

  @Test
  public void testGetDispatcher() throws Exception {
    MessageDispatcherRegistry.register(new DummyDispatcher());
    assertNotNull(MessageDispatcherRegistry.getDispatcher());
  }

  private static class DummyDispatcher implements IMessageDispatcher {

    @Override
    public <Message> CompletableFuture<Object> dispatch(Message message, String clientId, String... targets) {
      return null;
    }

    @Override
    public <Message> CompletableFuture<Object> dispatch(Message message, String clientId, String kafkaTopic, int partition, String ... targets) {
      return null;
    }

    @Override
    public void keepAlive(String clientId) {

    }
  }
}