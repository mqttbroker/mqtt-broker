package io.mqute.core.topic;

import org.junit.Test;

import static org.junit.Assert.*;

public class SubscribeTopicFilterTest {
  @Test
  public void testConstructor() {
    new SubscribeTopicFilter("abc");
    new SubscribeTopicFilter("a/b/c");
    new SubscribeTopicFilter("a/+/c");
    new SubscribeTopicFilter("a/+/+");
    new SubscribeTopicFilter("a/+/#");
    new SubscribeTopicFilter("a/#");
  }

  @Test(expected = IllegalArgumentException.class)
  public void testNullTopicString() {
    new SubscribeTopicFilter(null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testEmptyTopicString() {
    new SubscribeTopicFilter("");
  }

  @Test(expected = IllegalArgumentException.class)
  public void testTopicStringWithInvalidHash() {
    new SubscribeTopicFilter("a/#/c");
  }

  @Test(expected = IllegalArgumentException.class)
  public void testTopicStringWithLeadingSlash() {
    new SubscribeTopicFilter("/a/b/c");
  }

  @Test(expected = IllegalArgumentException.class)
  public void testTopicStringWithTrailingSlash() {
    new SubscribeTopicFilter("a/b/c/");
  }


  @Test
  public void testGetHead() {
    SubscribeTopicFilter filter = new SubscribeTopicFilter("a/b/c");
    assertEquals("a", filter.head());
  }

  @Test
  public void testGetTail() {
    SubscribeTopicFilter filter = new SubscribeTopicFilter("a/b/c");
    SubscribeTopicFilter tail = filter.tail();
    assertEquals("b", tail.head());
  }

  @Test
  public void testIsLeaf() {
    SubscribeTopicFilter filter = new SubscribeTopicFilter("a");
    assertFalse(filter.isLeaf());
    SubscribeTopicFilter tail = filter.tail();
    assertTrue(tail.isLeaf());
  }

  @Test
  public void testLeaf() {
    SubscribeTopicFilter filter = new SubscribeTopicFilter("a/b/c");
    SubscribeTopicFilter leaf = filter.leaf();
    assertTrue(leaf.isLeaf());
  }
}
