package io.mqute.core.topic;

import org.junit.Before;
import org.junit.Test;

import java.util.Collections;

import io.mqute.IMqttSubscription;
import io.mqute.core.MquteSubscription;
import io.mqute.msg.enums.QoSType;
import io.mqute.topic.IMqttTopicFilter;
import io.mqute.topic.IMqttTopicNode;

import static org.junit.Assert.*;


public class TopicNodeTest {

  private TopicNode node;

  @Before
  public void beforeEach() throws Exception {
    node = new TopicNode();
  }

  @Test
  public void testGetChildren() throws Exception {
    assertEquals(0, node.getChildren().size());
    TopicNode child = new TopicNode();
    node = new TopicNode(Collections.singletonMap("key", child), null);
    assertEquals(1, node.getChildren().size());
  }

  @Test
  public void testGetSubscriptions() throws Exception {
    assertEquals(0, node.getSubscriptions().size());
    MquteSubscription subscription = new MquteSubscription("123", "a/b/c", QoSType.LEAST_ONCE);
    node = new TopicNode(null, Collections.singleton(subscription));
    assertEquals(1, node.getSubscriptions().size());
  }

  @Test
  public void testLocate() throws Exception {
    SubscribeTopicFilter topicFilter = new SubscribeTopicFilter("a/b/c");
    TopicNode child = node.locate(topicFilter);
    assertEquals(child, node.locate(topicFilter));
  }

  @Test
  public void testLocateWildcard() throws Exception {
    SubscribeTopicFilter topicFilter = new SubscribeTopicFilter("a/+/c");
    TopicNode child = node.locate(topicFilter);
    assertEquals(child, node.locate(topicFilter));

    topicFilter = new SubscribeTopicFilter("a/b/+");
    child = node.locate(topicFilter);
    assertEquals(child, node.locate(topicFilter));

    topicFilter = new SubscribeTopicFilter("a/#");
    child = node.locate(topicFilter);
    assertEquals(child, node.locate(topicFilter));
  }

  @Test
  public void testFilter() throws Exception {
    SubscribeTopicFilter topicFilter = new SubscribeTopicFilter("a/b/c");
    node.locate(topicFilter);
    PublishTopicName topicName = new PublishTopicName("a/b/c");
    assertEquals(1, node.filter(topicName).size());

    topicName = new PublishTopicName("a/b/d");
    assertEquals(0, node.filter(topicName).size());
  }

  @Test
  public void testFilterWithWildcard() throws Exception {
    SubscribeTopicFilter topicFilter = new SubscribeTopicFilter("a/+/c");
    node.locate(topicFilter);
    PublishTopicName topicName = new PublishTopicName("a/b/c");
    assertEquals(1, node.filter(topicName).size());
    topicName = new PublishTopicName("a/d/c");
    assertEquals(1, node.filter(topicName).size());


    topicFilter = new SubscribeTopicFilter("a/b/+");
    node.locate(topicFilter);
    topicName = new PublishTopicName("a/b/c");
    assertEquals(2, node.filter(topicName).size());
    topicName = new PublishTopicName("a/d/c");
    assertEquals(1, node.filter(topicName).size());

    topicFilter = new SubscribeTopicFilter("a/#");
    node.locate(topicFilter);
    topicName = new PublishTopicName("a/b/c");
    assertEquals(3, node.filter(topicName).size());
    topicName = new PublishTopicName("a/d/c");
    assertEquals(2, node.filter(topicName).size());
    topicName = new PublishTopicName("a/c");
    assertEquals(2, node.filter(topicName).size());
  }

  @Test
  public void testAppendRemove() throws Exception {
    MquteSubscription subscription = new MquteSubscription("123", "a/b/c", QoSType.LEAST_ONCE);
    node.append(subscription.getClientId(), subscription);
    assertEquals(1, node.getSubscriptions().size());
    node.remove("123");
    assertEquals(0, node.getSubscriptions().size());
  }

}