package io.mqute.core.topic;


import org.junit.Test;

import static org.junit.Assert.*;

public class PublishTopicNameTest {

  @Test
  public void testConstructor() {
    new PublishTopicName("abc");
    new PublishTopicName("a/b/c");
  }

  @Test(expected = IllegalArgumentException.class)
  public void testNullTopicString() {
    new PublishTopicName(null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testEmptyTopicString() {
    new PublishTopicName("");
  }

  @Test(expected = IllegalArgumentException.class)
  public void testTopicStringWithWildcardPlus() {
    new PublishTopicName("a/+/c");
  }

  @Test(expected = IllegalArgumentException.class)
  public void testTopicStringWithWildcardHash() {
    new PublishTopicName("a/#");
  }

  @Test(expected = IllegalArgumentException.class)
  public void testTopicStringWithLeadingSlash() {
    new PublishTopicName("/a/b/c");
  }

  @Test(expected = IllegalArgumentException.class)
  public void testTopicStringWithTrailingSlash() {
    new PublishTopicName("a/b/c/");
  }


  @Test
  public void testGetHead() {
    PublishTopicName filter = new PublishTopicName("a/b/c");
    assertEquals("a", filter.head());
  }

  @Test
  public void testGetTail() {
    PublishTopicName filter = new PublishTopicName("a/b/c");
    PublishTopicName tail = filter.tail();
    assertEquals("b", tail.head());
  }

  @Test
  public void testIsLeaf() {
    PublishTopicName filter = new PublishTopicName("a");
    assertFalse(filter.isLeaf());
    PublishTopicName tail = filter.tail();
    assertTrue(tail.isLeaf());
  }

  @Test
  public void testLeaf() {
    PublishTopicName filter = new PublishTopicName("a/b/c");
    PublishTopicName leaf = filter.leaf();
    assertTrue(leaf.isLeaf());
  }
}
