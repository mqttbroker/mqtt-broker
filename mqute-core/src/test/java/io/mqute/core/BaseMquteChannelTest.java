package io.mqute.core;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Optional;
import java.util.Queue;

import io.mqute.IMqttChannel;
import io.mqute.msg.ConnAckMessage;
import io.mqute.msg.ConnectMessage;
import io.mqute.msg.Credential;
import io.mqute.msg.DisconnectMessage;
import io.mqute.msg.MqttMessage;
import io.mqute.msg.PingReqMessage;
import io.mqute.msg.PingRespMessage;
import io.mqute.msg.PubAckMessage;
import io.mqute.msg.PubCompMessage;
import io.mqute.msg.PubRecMessage;
import io.mqute.msg.PubRelMessage;
import io.mqute.msg.PublishMessage;
import io.mqute.msg.SubAckMessage;
import io.mqute.msg.SubscribeMessage;
import io.mqute.msg.UnsubAckMessage;
import io.mqute.msg.UnsubscribeMessage;
import io.mqute.msg.enums.ConnectReturnCode;
import io.mqute.msg.enums.QoSType;
import io.mqute.test.DispatcherProbe;
import io.mqute.test.MockAuthenticator;
import io.mqute.test.MockAuthorizor;
import io.mqute.test.TestMquteChannelImpl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


public class BaseMquteChannelTest {

  private static DispatcherProbe dispatcherProbe = new DispatcherProbe();
  private Queue<MqttMessage> output;

  @BeforeClass
  public static void beforeAll() throws Exception {
    MessageDispatcherRegistry.register(dispatcherProbe);
  }

  @AfterClass
  public static void afterAll() throws Exception {
    Field field = MessageDispatcherRegistry.class.getDeclaredField("dispatcher");
    field.setAccessible(true);
    field.set(null, null);
    field.setAccessible(false);
  }

  @Before
  public void beforeEach() {
    dispatcherProbe.clear();
    output = new LinkedList<>();
  }

  @Test
  public void testOpenConnectionWithBadProtocolName() throws Exception {
    ConnectMessage message = new ConnectMessage("Bad", Constants.VERSION_3_1_1, (short) 100, true, "abc");
    IMqttChannel<MqttMessage> channel = new TestMquteChannelImpl(output, message);
    assertFalse(channel.isActive());
    assertEquals(new ConnAckMessage(ConnectReturnCode.BAD_PROTOCOL_VERSION, false), output.poll());
    assertTrue(output.isEmpty());
  }

  @Test
  public void testOpenConnectionWithBadProtocolLevel() throws Exception {
    ConnectMessage message = new ConnectMessage(Constants.MQTT, (byte) 0x01, (short) 100, true, "abc");
    IMqttChannel<MqttMessage> channel = new TestMquteChannelImpl(output, message);
    assertFalse(channel.isActive());
    assertEquals(new ConnAckMessage(ConnectReturnCode.BAD_PROTOCOL_VERSION, false), output.poll());
    assertTrue(output.isEmpty());
  }

  @Test
  public void testOpenConnectionWithEmptyId() throws Exception {
    ConnectMessage message = new ConnectMessage(Constants.MQTT, Constants.VERSION_3_1_1, (short) 100, true, "");
    IMqttChannel<MqttMessage> channel = new TestMquteChannelImpl(output, message);
    assertFalse(channel.isActive());
    assertEquals(new ConnAckMessage(ConnectReturnCode.ID_REJECTED, false), output.poll());
    assertTrue(output.isEmpty());
  }

  @Test
  public void testOpenConnectionWithNotAuthorizedId() throws Exception {
    ConnectMessage message = new ConnectMessage(Constants.MQTT, Constants.VERSION_3_1_1, (short) 100, true, "blocked");
    IMqttChannel<MqttMessage> channel = new TestMquteChannelImpl(
        output, message, null, new MockAuthorizor()
    );
    assertFalse(channel.isActive());
    assertEquals(new ConnAckMessage(ConnectReturnCode.NOT_AUTHORIZED, false), output.poll());
    assertTrue(output.isEmpty());
  }

  @Test
  public void testOpenConnectionWithBadCredential() throws Exception {
    Credential credential = new Credential("username", "badPass".getBytes());
    ConnectMessage message = new ConnectMessage(
        Constants.MQTT, Constants.VERSION_3_1_1, (short) 100, true, "abc", credential
    );
    IMqttChannel<MqttMessage> channel = new TestMquteChannelImpl(
        output, message, new MockAuthenticator(), null
    );
    assertFalse(channel.isActive());
    assertEquals(new ConnAckMessage(ConnectReturnCode.BAD_USERNAME_OR_PASSWORD, false), output.poll());
    assertTrue(output.isEmpty());
  }

  @Test
  public void testOpenConnection() throws Exception {
    createActiveChannel();
    assertTrue(output.isEmpty());
  }

  @Test
  public void testHandleMessageSubscribe() throws Exception {
    IMqttChannel<MqttMessage> channel = createActiveChannel();
    LinkedHashMap<String, QoSType> subscriptions = new LinkedHashMap<>();
    subscriptions.put("topic/writeonly", QoSType.MOST_ONCE);
    subscriptions.put("a/b/c", QoSType.LEAST_ONCE);
    subscriptions.put("a/#/c", QoSType.EXACT_ONCE);
    SubscribeMessage message = new SubscribeMessage((short) 123, subscriptions);
    channel.handleMessage(message);
    QoSType[] qoses = {QoSType.FAILURE, QoSType.LEAST_ONCE, QoSType.FAILURE};
    assertEquals(new SubAckMessage((short) 123, qoses), output.poll());
    assertTrue(output.isEmpty());
    subscriptions.remove("topic/writeonly");
    subscriptions.remove("a/#/c");
    assertEquals(new SubscribeMessage((short) 123, subscriptions), dispatcherProbe.nextMessage());
    assertEquals(channel.getClientId(), dispatcherProbe.nextClientId());
  }

  @Test
  public void testHandleMessagePublishMostOnce() throws Exception {
    IMqttChannel<MqttMessage> channel = createActiveChannel();
    PublishMessage message = new PublishMessage(
        false, QoSType.MOST_ONCE, false, (short) 123, "a/b/c", new byte[1]
    );
    channel.handleMessage(message);
    assertEquals(message, dispatcherProbe.nextMessage());
    assertEquals(channel.getClientId(), dispatcherProbe.nextClientId());
  }

  @Test
  public void testHandleMessagePublishLeastOnce() throws Exception {
    IMqttChannel<MqttMessage> channel = createActiveChannel();
    PublishMessage message = new PublishMessage(
        false, QoSType.LEAST_ONCE, false, (short) 123, "a/b/c", new byte[1]
    );
    channel.handleMessage(message);
    assertEquals(message, dispatcherProbe.nextMessage());
    assertEquals(channel.getClientId(), dispatcherProbe.nextClientId());
    assertEquals(new PubAckMessage((short) 123), output.poll());
    assertTrue(output.isEmpty());
  }

  @Test
  public void testHandleMessagePublishExactOnce() throws Exception {
    IMqttChannel<MqttMessage> channel = createActiveChannel();
    PublishMessage message = new PublishMessage(
        false, QoSType.EXACT_ONCE, false, (short) 123, "a/b/c", new byte[1]
    );
    channel.handleMessage(message);
    assertEquals(message, dispatcherProbe.nextMessage());
    assertEquals(channel.getClientId(), dispatcherProbe.nextClientId());
    assertEquals(new PubRecMessage((short) 123), output.poll());
    assertTrue(output.isEmpty());
  }

  @Test
  public void testHandleMessagePublishInvalidTopic() throws Exception {
    IMqttChannel<MqttMessage> channel = createActiveChannel();
    PublishMessage message = new PublishMessage(
        false, QoSType.LEAST_ONCE, false, (short) 123, "a/+/c", new byte[1]
    );
    channel.handleMessage(message);
    assertNull(dispatcherProbe.nextMessage());
    assertTrue(output.isEmpty());
    assertFalse(channel.isActive());
  }

  @Test
  public void testHandleMessagePublishUnauthorizedTopic() throws Exception {
    IMqttChannel<MqttMessage> channel = createActiveChannel();
    PublishMessage message = new PublishMessage(
        false, QoSType.LEAST_ONCE, false, (short) 123, "topic/readonly", new byte[1]
    );
    channel.handleMessage(message);
    assertNull(dispatcherProbe.nextMessage());
    assertTrue(output.isEmpty());
    assertFalse(channel.isActive());
  }

  @Test
  public void testHandleMessageUnsubscribe() throws Exception {
    IMqttChannel<MqttMessage> channel = createActiveChannel();
    UnsubscribeMessage message = new UnsubscribeMessage((short) 123, Collections.singletonList("a/b/c"));
    channel.handleMessage(message);
    assertEquals(new UnsubAckMessage((short) 123), output.poll());
    assertTrue(output.isEmpty());
    assertEquals(message, dispatcherProbe.nextMessage());
    assertEquals(channel.getClientId(), dispatcherProbe.nextClientId());
  }

  @Test
  public void testHandleMessagePingReq() throws Exception {
    IMqttChannel<MqttMessage> channel = createActiveChannel();
    channel.handleMessage(new PingReqMessage());
    assertEquals(new PingRespMessage(), output.poll());
    assertEquals("abc", dispatcherProbe.nextKeepAlive());
  }

  @Test
  public void testHandleMessageDisconnect() throws Exception {
    IMqttChannel<MqttMessage> channel = createActiveChannel();
    channel.handleMessage(new DisconnectMessage());
    assertFalse(channel.isActive());
  }

  @Test
  public void testHandleMessagePubAck() throws Exception {
    IMqttChannel<MqttMessage> channel = createActiveChannel();
    PubAckMessage message = new PubAckMessage((short) 123);
    channel.handleMessage(message);
    assertEquals(message, dispatcherProbe.nextMessage());
    assertEquals(channel.getClientId(), dispatcherProbe.nextClientId());
  }

  @Test
  public void testHandleMessagePubRec() throws Exception {
    IMqttChannel<MqttMessage> channel = createActiveChannel();
    PubRecMessage message = new PubRecMessage((short) 123);
    channel.handleMessage(message);
    assertEquals(new PubRelMessage((short) 123), output.poll());
    assertTrue(output.isEmpty());
    assertEquals(message, dispatcherProbe.nextMessage());
    assertEquals(channel.getClientId(), dispatcherProbe.nextClientId());
  }

  @Test
  public void testHandleMessagePubRel() throws Exception {
    IMqttChannel<MqttMessage> channel = createActiveChannel();
    PubRelMessage message = new PubRelMessage((short) 123);
    channel.handleMessage(message);
    assertEquals(new PubCompMessage((short) 123), output.poll());
    assertTrue(output.isEmpty());
  }

  @Test
  public void testHandleMessagePubComp() throws Exception {
    IMqttChannel<MqttMessage> channel = createActiveChannel();
    PubCompMessage message = new PubCompMessage((short) 123);
    channel.handleMessage(message);
    assertEquals(message, dispatcherProbe.nextMessage());
    assertEquals(channel.getClientId(), dispatcherProbe.nextClientId());
  }

  @Test
  public void testKeepAlive() throws Exception {
    IMqttChannel<MqttMessage> channel = createActiveChannel();
    channel.keepAlive();
    assertEquals("abc", dispatcherProbe.nextKeepAlive());
  }

  @Test
  public void testGetClientId() throws Exception {
    IMqttChannel<MqttMessage> channel = createActiveChannel();
    assertEquals("abc", channel.getClientId());
  }

  @Test
  public void testClose() throws Exception {
    IMqttChannel<MqttMessage> channel = createActiveChannel();
    channel.close();
    assertFalse(channel.isActive());
  }

  private IMqttChannel<MqttMessage> createActiveChannel() {
    PublishMessage willMessage = new PublishMessage(
        false, QoSType.MOST_ONCE, false, (short) 123, "a/b/c", new byte[1]
    );
    Credential credential = new Credential("username", "password".getBytes());
    ConnectMessage message = new ConnectMessage(
        Constants.MQTT, Constants.VERSION_3_1_1, (short) 100, true, "abc", credential, willMessage
    );
    IMqttChannel<MqttMessage> channel = new TestMquteChannelImpl(
        output, message, new MockAuthenticator(), new MockAuthorizor()
    );
    assertTrue(channel.isActive());
    assertEquals(message, dispatcherProbe.nextMessage());
    assertEquals("abc", dispatcherProbe.nextClientId());
    return channel;
  }
}