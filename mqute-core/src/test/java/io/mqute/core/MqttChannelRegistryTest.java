package io.mqute.core;

import io.mqute.IMqttChannel;
import io.mqute.msg.MqttMessage;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class MqttChannelRegistryTest {

  @Before
  public void beforeEach() throws Exception {
    MqttChannelRegistry registry = MqttChannelRegistry.getInstance();
    Field field = registry.getClass().getDeclaredField("channelMap");
    field.setAccessible(true);
    field.set(registry, new ConcurrentHashMap<String, IMqttChannel<MqttMessage>>());
    field.setAccessible(false);
  }

  @Test
  public void testGetChannel() throws Exception {
    MqttChannelRegistry registry = MqttChannelRegistry.getInstance();
    assertFalse(registry.getChannel("abc").isPresent());
    registry.registerChannel("abc", new DummyChannel("abc", true));
    assertTrue(registry.getChannel("abc").isPresent());
  }

  @Test
  public void testRegisterChannel() throws Exception {
    MqttChannelRegistry registry = MqttChannelRegistry.getInstance();
    registry.registerChannel("123", new DummyChannel("123", true));
    assertTrue(registry.getChannel("123").isPresent());
  }

  @Test
  public void testRemoveChannel() throws Exception {
    MqttChannelRegistry registry = MqttChannelRegistry.getInstance();
    registry.registerChannel("123", new DummyChannel("123", true));
    assertTrue(registry.getChannel("123").isPresent());
    registry.removeChannel("123");
    assertFalse(registry.getChannel("123").isPresent());
  }

  private static class DummyChannel implements IMqttChannel<MqttMessage> {

    private final boolean active;
    private final String clientId;

    public DummyChannel(String clientId, boolean active) {
      this.clientId = clientId;
      this.active = active;
    }

    @Override
    public void handleMessage(MqttMessage msg) {

    }

    @Override
    public void keepAlive() {

    }

    @Override
    public void write(MqttMessage msg) {

    }

    @Override
    public void close() {

    }

    @Override
    public boolean isActive() {
      return active;
    }

    @Override
    public String getClientId() {
      return clientId;
    }
  }
}