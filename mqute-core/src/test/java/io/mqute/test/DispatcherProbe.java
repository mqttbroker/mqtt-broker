package io.mqute.test;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.CompletableFuture;

import io.mqute.IMessageDispatcher;


public class DispatcherProbe implements IMessageDispatcher {

  private Queue<Object> outgoingMessages;
  private Queue<String> clientIds;
  private Queue<String> keepAlives;

  public DispatcherProbe() {
    this.outgoingMessages = new LinkedList<>();
    this.clientIds = new LinkedList<>();
    this.keepAlives = new LinkedList<>();
  }

  @Override
  public <Message> CompletableFuture<Object> dispatch(Message message, String clientId, String... targets) {
    CompletableFuture<Object> future = new CompletableFuture<>();
    try {
      outgoingMessages.offer(message);
      clientIds.offer(clientId);
      future.complete(null);
    } catch (Exception e) {
      future.completeExceptionally(e);
    }
    return future;
  }

  @Override
  public <Message> CompletableFuture<Object> dispatch(Message message, String clientId, String kafkaTopic, int partition, String... targets) {
    return dispatch(message, clientId);
  }

  @Override
  public void keepAlive(String clientId) {
    keepAlives.offer(clientId);
  }

  public void clear() {
    clientIds.clear();
    outgoingMessages.clear();
    keepAlives.clear();
  }

  public Object nextMessage() {
    return outgoingMessages.poll();
  }

  public String nextClientId() {
    return clientIds.poll();
  }

  public String nextKeepAlive() {
    return keepAlives.poll();
  }
}
