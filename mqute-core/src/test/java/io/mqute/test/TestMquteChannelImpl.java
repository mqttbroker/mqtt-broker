package io.mqute.test;


import java.util.Queue;

import io.mqute.core.BaseMquteChannel;
import io.mqute.msg.ConnectMessage;
import io.mqute.msg.MqttMessage;
import io.mqute.security.IAuthenticator;
import io.mqute.security.IAuthorizor;

public class TestMquteChannelImpl extends BaseMquteChannel {

  private Queue<MqttMessage> output;

  public TestMquteChannelImpl(Queue<MqttMessage> output,
                              ConnectMessage connectMsg) {
    super(connectMsg);
    this.output = output;
    openConnection(connectMsg);
  }

  public TestMquteChannelImpl(Queue<MqttMessage> output,
                              ConnectMessage connectMsg,
                              IAuthenticator authenticator,
                              IAuthorizor authorizor) {
    super(authenticator, authorizor);
    this.output = output;
    openConnection(connectMsg);
  }

  @Override
  public void write(MqttMessage msg) {
    output.offer(msg);
  }
}
