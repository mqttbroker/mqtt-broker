package io.mqute.test;


import io.mqute.security.IAuthorizor;

import java.util.Optional;

public class MockAuthorizor implements IAuthorizor {

  private static final String BLOCKED_CLIENT = "blocked";
  private static final String READONLY_TOPIC = "topic/readonly";
  private static final String WRITEONLY_TOPIC = "topic/writeonly";

  @Override
  public boolean canWrite(String topic, String user, String clientId) {
    return !READONLY_TOPIC.equals(topic);
  }

  @Override
  public boolean canRead(String topic, String user, String clientId) {
    return !WRITEONLY_TOPIC.endsWith(topic);
  }

  @Override
  public boolean canConnect(String clientId) {
    return !BLOCKED_CLIENT.equals(clientId);
  }
}
