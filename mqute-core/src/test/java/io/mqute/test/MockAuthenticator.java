package io.mqute.test;

import io.mqute.security.IAuthenticator;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;


public class MockAuthenticator implements IAuthenticator {

  private static final String CORRECT_USERNAME = "username";
  private static byte[] CORRECT_PASSWORD_HASH;

  static {
    try {
      CORRECT_PASSWORD_HASH = MessageDigest.getInstance("MD5").digest("password".getBytes());
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
  }

  @Override
  public boolean checkValid(String username, byte[] password) {
    try {
      byte[] hash = MessageDigest.getInstance("MD5").digest(password);
      return CORRECT_USERNAME.equals(username) && Arrays.equals(CORRECT_PASSWORD_HASH, hash);
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    return false;
  }
}
