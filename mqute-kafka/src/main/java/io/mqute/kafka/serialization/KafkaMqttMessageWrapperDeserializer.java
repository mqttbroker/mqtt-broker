package io.mqute.kafka.serialization;


import com.google.protobuf.InvalidProtocolBufferException;

import org.apache.kafka.common.serialization.Deserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import io.mqute.kafka.msg.KafkaMqttEnvelopOuterClass;
import io.mqute.kafka.msg.KafkaMqttMessageWrapper;

public class KafkaMqttMessageWrapperDeserializer implements Deserializer<KafkaMqttMessageWrapper> {

  private static final Logger LOGGER =
      LoggerFactory.getLogger(KafkaMqttMessageWrapperDeserializer.class);

  @Override
  public void configure(Map<String, ?> configs, boolean isKey) {
    // no configure
  }

  @Override
  public KafkaMqttMessageWrapper deserialize(final String topic, final byte[] data) {

    try {
      return new KafkaMqttMessageWrapper(
          KafkaMqttEnvelopOuterClass.KafkaMqttEnvelop.parseFrom(data));
    } catch (InvalidProtocolBufferException e) {
      LOGGER.error("[kafka] deserialize message failed due to: {}", e);
      return null;
    }
  }

  @Override
  public void close() {
    // nothing to do
  }
}
