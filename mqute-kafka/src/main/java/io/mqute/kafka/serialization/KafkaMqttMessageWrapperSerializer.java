package io.mqute.kafka.serialization;

import com.google.protobuf.ByteString;

import org.apache.kafka.common.serialization.Serializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

import io.mqute.kafka.msg.KafkaMqttEnvelopOuterClass;
import io.mqute.kafka.msg.KafkaMqttMessageWrapper;
import io.mqute.msg.serialization.MqttSerialization;


public class KafkaMqttMessageWrapperSerializer implements Serializer<KafkaMqttMessageWrapper> {

  private static final Logger LOGGER = LoggerFactory.getLogger(KafkaMqttMessageWrapperSerializer.class);

  @Override
  public void configure(Map<String, ?> configs, boolean isKey) {
    // no configure
  }

  @Override
  public byte[] serialize(final String topic, final KafkaMqttMessageWrapper data) {

    final KafkaMqttEnvelopOuterClass.KafkaMqttEnvelop envelop = KafkaMqttEnvelopOuterClass.KafkaMqttEnvelop.newBuilder()
        .setClientID(data.getClientId())
        .setOriginal(ByteString.copyFrom(MqttSerialization.getInstance().serialize(data.getOriginalMessage())))
        .addAllTargets(Arrays.asList(data.getTargets()))
        .setSenderHost(data.getSenderHost())
        .setSenderPort(data.getSenderPort()).build();

    final ByteArrayOutputStream bos = new ByteArrayOutputStream(envelop.getOriginal().size() + 128);
    try {
      envelop.writeTo(bos);
    } catch (IOException e) {
      LOGGER.error("[kafka] serialize message failed due to: {}", e);
    }

    return bos.toByteArray();
  }

  @Override
  public void close() {
    // nothing to do
  }
}
