package io.mqute.kafka.producer;

import io.mqute.IMessageDispatcher;
import io.mqute.kafka.msg.KafkaMqttMessageWrapper;
import io.mqute.kafka.serialization.KafkaMqttMessageWrapperSerializer;
import io.mqute.msg.MqttMessage;
import io.mqute.msg.PingRespMessage;
import io.mqute.msg.PublishMessage;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.CompletableFuture;

import static io.mqute.kafka.KafkaTopics.*;


public class KafkaMessageDispatcher implements IMessageDispatcher {

  private static final Logger LOGGER = LoggerFactory.getLogger(KafkaMessageDispatcher.class);

  private static final String PROP_FILE_PATH = "META-INF/kafka-producer.properties";

  // Kafka producer keyed by client id
  private final Producer<String, KafkaMqttMessageWrapper> producer;

  private final String callbackHost;
  private final int callbackPort;

  public KafkaMessageDispatcher(String callbackHost, int callbackPort) {
    this.callbackHost = callbackHost;
    this.callbackPort = callbackPort;
    Properties configProps = new Properties();
    try {
      configProps.load(getClass().getClassLoader().getResourceAsStream(PROP_FILE_PATH));
    } catch (IOException ex) {
      LOGGER.warn("Failed to load kafka configuration from {}", PROP_FILE_PATH, ex);
    }
    producer = new KafkaProducer<>(
        configProps,
        new StringSerializer(),
        new KafkaMqttMessageWrapperSerializer()
    );
  }

  @Override
  public <Message> CompletableFuture<Object> dispatch(Message message, String clientId, String... targets) {
    return dispatch(message, clientId, null, -1, targets);
  }

  @Override
  public <Message> CompletableFuture<Object> dispatch(Message message, String clientId, String kafkaTopic, int partition, String... targets) {

    LOGGER.info("[dispatch] client:{}, partition:{}, endpoint:{}:{}", clientId, partition, callbackHost, callbackPort);
    CompletableFuture<Object> future = new CompletableFuture<>();
    if (message instanceof MqttMessage) {
      MqttMessage mqttMessage = (MqttMessage) message;
      KafkaMqttMessageWrapper messageWrapper = new KafkaMqttMessageWrapper(
          mqttMessage, clientId, targets, callbackHost, callbackPort
      );

      String partitionKey = clientId;
      switch (mqttMessage.getMessageType()) {
        case CONNECT:
        case DISCONNECT:
        case SUBSCRIBE:
        case UNSUBSCRIBE:
          kafkaTopic = GENERAL_TOPIC;
          break;
        case PUBLISH:
          partitionKey = ((PublishMessage) mqttMessage).getTopicName();
          kafkaTopic = Optional.ofNullable(kafkaTopic).orElse(PUBLISH_TOPIC);
          break;
        case PUBACK:
        case PUBREC:
        case PUBCOMP:
          kafkaTopic = Optional.ofNullable(kafkaTopic).orElse(ACK_TOPIC);
          break;
        default:
          break;
      }
      if (kafkaTopic != null) {
        final ProducerRecord<String, KafkaMqttMessageWrapper> record = partition >= 0
            ? new ProducerRecord<>(kafkaTopic, partition, partitionKey, messageWrapper)
            : new ProducerRecord<>(kafkaTopic, partitionKey, messageWrapper);
        producer.send(record, (metadata, exception) -> {
              if (exception != null) {
                future.completeExceptionally(exception);
              } else {
                future.complete(metadata);
              }
            }
        );
      } else {
        future.completeExceptionally(
            new IllegalArgumentException("Message " + mqttMessage.getMessageType()
                + " should not appear here."));
      }
    } else {
      future.completeExceptionally(
          new IllegalArgumentException("Cannot dispatch message " + message));
    }
    return future.whenComplete((result, exception) -> {
      if (exception != null) {
        LOGGER.error(exception.getMessage(), exception);
      }
    });
  }

  @Override
  public void keepAlive(String clientId) {

    producer.send(new ProducerRecord<>(KEEP_ALIVE_TOPIC, clientId, new KafkaMqttMessageWrapper(new PingRespMessage(), clientId, null, callbackHost, callbackPort)));
  }
}
