package io.mqute.kafka.consumer;

import com.google.common.base.Strings;

import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import io.mqute.kafka.msg.KafkaMqttMessageWrapper;
import io.mqute.kafka.serialization.KafkaMqttMessageWrapperDeserializer;


public class MquteKafkaConsumerFactory {

  private static final Logger logger = LoggerFactory.getLogger(MquteKafkaConsumerFactory.class);

  private static final String PROP_FILE_PATH = "META-INF/kafka-consumer.properties";

  public KafkaConsumer<String, KafkaMqttMessageWrapper> create() {
    return create(null);
  }

  public KafkaConsumer<String, KafkaMqttMessageWrapper> create(String name) {
    return create(name, null);
  }

  public KafkaConsumer<String, KafkaMqttMessageWrapper> create(final String name, final String consumerGroup) {
    Properties configProps = new Properties();
    try {
      configProps = getPropertiesForConsumer(name);
      if(!Strings.isNullOrEmpty(consumerGroup)){
        configProps.put("group.id", consumerGroup);
      }
    } catch (IOException e) {
      logger.warn("Failed to load kafka configuration from {}", PROP_FILE_PATH, e);
    }
    return new KafkaConsumer<>(
        configProps,
        new StringDeserializer(),
        new KafkaMqttMessageWrapperDeserializer()
    );
  }

  private Properties getPropertiesForConsumer(String name) throws IOException {
    Properties properties = new Properties();
    properties.load(getClass().getClassLoader().getResourceAsStream(PROP_FILE_PATH));
    if (name == null || name.isEmpty()) {
      return properties;
    }
    // properties for a specific consumer is prefixed by {consumer name}.
    Map<String, Object> consumerOverriddenProperties = new HashMap<>();
    for (Object object : properties.keySet()) {
      String key = (String) object;
      if (key.startsWith(name + ".")) {
        consumerOverriddenProperties.put(key.substring(name.length() + 1), properties.get(key));
      }
    }
    properties.putAll(consumerOverriddenProperties);
    return properties;
  }

}
