package io.mqute.kafka.msg;

import com.google.protobuf.ByteString;

import java.util.Arrays;

import io.mqute.msg.MqttMessage;
import io.mqute.msg.serialization.MqttSerialization;


public class KafkaMqttMessageWrapper {

  private final KafkaMqttEnvelopOuterClass.KafkaMqttEnvelop envelop;

  public KafkaMqttMessageWrapper(final MqttMessage mqttMessage,
                                 final String clientID,
                                 final String[] targets,
                                 final String senderHost,
                                 final int senderPort) {

    this(KafkaMqttEnvelopOuterClass.KafkaMqttEnvelop.newBuilder()
        .setOriginal(ByteString.copyFrom(MqttSerialization.getInstance().serialize(mqttMessage)))
        .setClientID(clientID)
        .addAllTargets(Arrays.asList(targets))
        .setSenderHost(senderHost)
        .setSenderPort(senderPort).build());
  }

  public KafkaMqttMessageWrapper(final KafkaMqttEnvelopOuterClass.KafkaMqttEnvelop envelop) {
    this.envelop = envelop;
  }

  public MqttMessage getOriginalMessage() {
    return MqttSerialization.getInstance().deserialize(envelop.getOriginal().asReadOnlyByteBuffer());
  }

  public String getClientId() {
    return envelop.getClientID();
  }

  public String[] getTargets() {
    return envelop.getTargetsList().toArray(new String[envelop.getTargetsCount()]);
  }

  public String getSenderHost() {
    return envelop.getSenderHost();
  }

  public int getSenderPort() {
    return envelop.getSenderPort();
  }

  public KafkaMqttEnvelopOuterClass.KafkaMqttEnvelop getEnvelop() {
    return envelop;
  }

  @Override
  public String toString() {
    return "KafkaMqttMessageWrapper{" +
        "type=" + getOriginalMessage().getMessageType() +
        "\nsenderHost=" + getSenderHost() +
        "\nsenderPort=" + getSenderPort() +
        "\nclientID=" + getClientId() +
        '}';
  }
}
