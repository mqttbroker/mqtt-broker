package io.mqute.kafka;


public class KafkaTopics {

  public static final String GENERAL_TOPIC = "CONNECT|DISCONNECT|SUBSCRIBE|UNSUBSCRIBE";
  public static final String PUBLISH_TOPIC = "PUBLISH";
  //this is what publication processor route/filter and then pass on to subscription processor
  public static final String ROUTED_PUBLISH_TOPIC = "ROUTED_PUBLISH";
  //this is for edge to directly publish its aliveness, only those edges with heartbeat requires to publish this keepalive topic, and could be filtered even by last published time
  public static final String KEEP_ALIVE_TOPIC = "KEEP_ALIVE";
  public static final String ACK_TOPIC = "PUBACK|PUBREC|PUBCOMP";

  private KafkaTopics() {

  }
}
