package io.mqute.config.test.mock;

import io.mqute.security.IAuthenticator;


public class MockAuthenticator implements IAuthenticator {
  @Override
  public boolean checkValid(String username, byte[] password) {
    return false;
  }
}
