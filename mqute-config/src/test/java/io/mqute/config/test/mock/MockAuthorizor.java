package io.mqute.config.test.mock;

import io.mqute.security.IAuthorizor;

import java.util.Optional;


public class MockAuthorizor implements IAuthorizor {
  @Override
  public boolean canWrite(String topic, String user, String clientId) {
    return false;
  }

  @Override
  public boolean canRead(String topic, String user, String clientId) {
    return false;
  }

  @Override
  public boolean canConnect(String clientId) {
    return false;
  }
}
