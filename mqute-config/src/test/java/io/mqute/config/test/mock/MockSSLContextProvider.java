package io.mqute.config.test.mock;

import io.mqute.server.ISSLContextProvider;

import javax.net.ssl.SSLContext;


public class MockSSLContextProvider implements ISSLContextProvider {
  @Override
  public SSLContext getSSLContext() {
    return null;
  }
}
