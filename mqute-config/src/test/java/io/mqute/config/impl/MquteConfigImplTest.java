package io.mqute.config.impl;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import io.mqute.config.MquteConfig;
import io.mqute.config.test.mock.MockAuthenticator;
import io.mqute.config.test.mock.MockAuthorizor;
import io.mqute.config.test.mock.MockSSLContextProvider;


public class MquteConfigImplTest {

  @Test
  public void testTcpPortDefault() throws Exception {
    MquteConfig mquteConfig = new MquteConfigImpl();
    Assert.assertEquals(2170, mquteConfig.tcpPort());
  }

  @Test
  public void testTcpPortOverride() throws Exception {
    Map<String, Integer> configMap = new HashMap<>();
    configMap.put("mqute.server.tcp_port", 1234);
    Config config = ConfigFactory.parseMap(configMap);
    MquteConfig mquteConfig = new MquteConfigImpl(config);
    Assert.assertEquals(1234, mquteConfig.tcpPort());
  }

  @Test
  public void testDownstreamPortDefault() throws Exception {
    MquteConfig mquteConfig = new MquteConfigImpl();
    Assert.assertEquals(2172, mquteConfig.downstreamPort());
  }

  @Test
  public void testDownstreamPortOverride() throws Exception {
    Map<String, Integer> configMap = new HashMap<>();
    configMap.put("mqute.server.downstream_port", 3456);
    Config config = ConfigFactory.parseMap(configMap);
    MquteConfig mquteConfig = new MquteConfigImpl(config);
    Assert.assertEquals(3456, mquteConfig.downstreamPort());
  }


  @Test
  public void testSslPortDefault() throws Exception {
    MquteConfig mquteConfig = new MquteConfigImpl();
    Assert.assertEquals(2171, mquteConfig.sslPort());
  }

  @Test
  public void testSslPortOverride() throws Exception {
    Map<String, Integer> configMap = new HashMap<>();
    configMap.put("mqute.server.security.ssl_port", 5678);
    Config config = ConfigFactory.parseMap(configMap);
    MquteConfig mquteConfig = new MquteConfigImpl(config);
    Assert.assertEquals(5678, mquteConfig.sslPort());
  }

  @Test
  public void testAuthenticatorDefault() throws Exception {
    MquteConfig mquteConfig = new MquteConfigImpl();
    Assert.assertFalse(mquteConfig.authenticator().isPresent());
  }

  @Test
  public void testAuthenticatorOverride() throws Exception {
    Map<String, String> configMap = new HashMap<>();
    configMap.put("mqute.authenticator", MockAuthenticator.class.getCanonicalName());
    Config config = ConfigFactory.parseMap(configMap);
    MquteConfig mquteConfig = new MquteConfigImpl(config);
    Assert.assertTrue(mquteConfig.authenticator().isPresent());
  }

  @Test
  public void testAuthorizorDefault() throws Exception {
    MquteConfig mquteConfig = new MquteConfigImpl();
    Assert.assertFalse(mquteConfig.authorizor().isPresent());
  }

  @Test
  public void testAuthorizorOverride() throws Exception {
    Map<String, String> configMap = new HashMap<>();
    configMap.put("mqute.authorizor", MockAuthorizor.class.getCanonicalName());
    Config config = ConfigFactory.parseMap(configMap);
    MquteConfig mquteConfig = new MquteConfigImpl(config);
    Assert.assertTrue(mquteConfig.authorizor().isPresent());
  }

  @Test
  public void testWebSocketPortDefault() throws Exception {
    MquteConfig mquteConfig = new MquteConfigImpl();
    Assert.assertEquals(8080, mquteConfig.webSocketPort());
  }

  @Test
  public void testWebSocketOverride() throws Exception {
    Map<String, Integer> configMap = new HashMap<>();
    configMap.put("mqute.server.ws_port", 8086);
    Config config = ConfigFactory.parseMap(configMap);
    MquteConfig mquteConfig = new MquteConfigImpl(config);
    Assert.assertEquals(8086, mquteConfig.webSocketPort());
  }

  @Test
  public void testSslContextProviderDefault() throws Exception {
    MquteConfig mquteConfig = new MquteConfigImpl();
    Assert.assertFalse(mquteConfig.sslContextProvider().isPresent());
  }

  @Test
  public void testSslContextProviderOverride() throws Exception {
    Map<String, String> configMap = new HashMap<>();
    configMap.put("mqute.server.security.ssl_context_provider", MockSSLContextProvider.class.getCanonicalName());
    Config config = ConfigFactory.parseMap(configMap);
    MquteConfig mquteConfig = new MquteConfigImpl(config);
    Assert.assertTrue(mquteConfig.sslContextProvider().isPresent());
  }

  @Test
  public void testSecureWebSocketPortDefault() throws Exception {
    MquteConfig mquteConfig = new MquteConfigImpl();
    Assert.assertEquals(8082, mquteConfig.secureWebSocketPort());
  }

  @Test
  public void testSecureWebSocketOverride() throws Exception {
    Map<String, Integer> configMap = new HashMap<>();
    configMap.put("mqute.server.security.secure_ws_port", 8086);
    Config config = ConfigFactory.parseMap(configMap);
    MquteConfig mquteConfig = new MquteConfigImpl(config);
    Assert.assertEquals(8086, mquteConfig.secureWebSocketPort());
  }
}