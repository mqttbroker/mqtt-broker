package io.mqute.config.impl;


import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

import io.mqute.config.MquteConfig;
import io.mqute.security.IAuthenticator;
import io.mqute.security.IAuthorizor;
import io.mqute.server.ISSLContextProvider;

public class MquteConfigImpl implements MquteConfig {

  private static Logger logger = LoggerFactory.getLogger(MquteConfigImpl.class);

  private final int m_tcpPort;
  private final int m_downstreamPort;
  private final int m_sslPort;
  private final int m_wsPort;
  private final int m_secureWSPort;
  private final Optional<IAuthenticator> m_authenticator;
  private final Optional<IAuthorizor> m_authorizor;
  private final Optional<ISSLContextProvider> m_sslContextProvider;


  public MquteConfigImpl() {
    this(null);
  }

  public MquteConfigImpl(final Config overrides) {
    Config config = Optional.ofNullable(overrides)
        .orElseGet(ConfigFactory::empty).withFallback(ConfigFactory.load(getClass().getClassLoader()))
        .getConfig("mqute");

    m_tcpPort = config.getInt("server.tcp_port");
    m_downstreamPort = config.getInt("server.downstream_port");
    m_sslPort = config.getInt("server.security.ssl_port");
    m_wsPort = config.getInt("server.ws_port");
    m_secureWSPort = config.getInt("server.security.secure_ws_port");
    m_authenticator = tryToOptional(() -> config.getString("authenticator"))
        .flatMap(instanceCreator(IAuthenticator.class));
    m_authorizor = tryToOptional(() -> config.getString("authorizor"))
        .flatMap(instanceCreator(IAuthorizor.class));
    m_sslContextProvider = tryToOptional(() -> config.getString("server.security.ssl_context_provider"))
        .flatMap(instanceCreator(ISSLContextProvider.class));
  }

  private <T> Optional<T> tryToOptional(Supplier<T> supplier) {
    try {
      return Optional.ofNullable(supplier.get());
    } catch (Exception e) {
      return Optional.empty();
    }
  }

  private <T> Function<String, Optional<T>> instanceCreator(Class<T> clazz) {
    return (String className) -> {
      try {
        return Optional.ofNullable(Class.forName(className).asSubclass(clazz).newInstance());
      } catch (Exception e) {
        logger.warn("Failed to load {}: {}", className, e.getMessage());
        return Optional.empty();
      }
    };
  }

  @Override
  public int tcpPort() {
    return m_tcpPort;
  }

  @Override
  public int downstreamPort() {
    return m_downstreamPort;
  }

  @Override
  public int sslPort() {
    return m_sslPort;
  }

  @Override
  public int webSocketPort() {
    return m_wsPort;
  }

  @Override
  public int secureWebSocketPort() {
    return m_secureWSPort;
  }

  @Override
  public Optional<IAuthenticator> authenticator() {
    return m_authenticator;
  }

  @Override
  public Optional<IAuthorizor> authorizor() {
    return m_authorizor;
  }

  @Override
  public Optional<ISSLContextProvider> sslContextProvider() {
    return m_sslContextProvider;
  }
}
