package io.mqute.config;


import java.util.Optional;

import io.mqute.security.IAuthenticator;
import io.mqute.security.IAuthorizor;
import io.mqute.server.ISSLContextProvider;

public interface MquteConfig {

  int tcpPort();

  int downstreamPort();

  int sslPort();

  int webSocketPort();

  int secureWebSocketPort();

  Optional<IAuthenticator> authenticator();

  Optional<IAuthorizor> authorizor();

  Optional<ISSLContextProvider> sslContextProvider();

}
