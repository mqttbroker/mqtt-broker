package io.mqute.persistence.redis;

import com.lambdaworks.redis.api.async.RedisAsyncCommands;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import io.mqute.persistence.Codec;
import io.mqute.persistence.PersistenceService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


public class RedisServiceTest {

  private final Codec<String> codec = new StringCodec();
  private static PersistenceService redisService;
  private static Process process;

  @BeforeClass
  public static void beforeAll() throws Exception {
    String dir = System.getProperty("user.dir");
    if (dir.endsWith("mqute-persistence")) {
      dir += "/..";
    }
    process = Runtime.getRuntime().exec(dir + "/redis-3.2.3/src/redis-server");
    String redisUri = "redis://127.0.0.1:6379/0";
    redisService = new RedisService(redisUri);
  }

  @AfterClass
  public static void afterAll() throws Exception {
    Field field = RedisService.class.getDeclaredField("redisAsync");
    field.setAccessible(true);
    RedisAsyncCommands redisAsync = (RedisAsyncCommands) field.get(redisService);
    redisAsync.shutdown(false);
    process.waitFor();
  }

  @Test
  public void testCRUD() throws Exception {
    String key = "key:field";
    CompletableFuture<String> future = redisService.get(key, codec);
    assertNull(future.exceptionally(t -> null).get());

    assertTrue(redisService.update(key, codec, "value").get());
    future = redisService.get(key, codec);
    assertEquals("value", future.get());

    assertFalse(redisService.update(key, codec, "value2").get());
    future = redisService.get(key, codec);
    assertEquals("value2", future.get());

    assertTrue(redisService.remove(key).get());
    future = redisService.get(key, codec);
    assertNull(future.exceptionally(t -> null).get());

    assertFalse(redisService.remove(key).get());
  }

  @Test
  public void testOfferTake() throws Exception {
    String key = "queue";
    assertEquals(2, redisService.offer(key, codec, "value1", "value2").get().longValue());
    assertEquals(3, redisService.offer(key, codec, "value3").get().longValue());

    List<String> values = redisService.take(key, codec, 3).get();
    assertEquals("value1", values.get(0));
    assertEquals("value2", values.get(1));
    assertEquals("value3", values.get(2));
  }

  private static class StringCodec implements Codec<String> {
    @Override
    public byte[] encode(String data) {
      return data.getBytes(Charset.forName("UTF-8"));
    }

    @Override
    public String decode(byte[] bytes) {
      return new String(bytes, Charset.forName("UTF-8"));
    }
  }

}