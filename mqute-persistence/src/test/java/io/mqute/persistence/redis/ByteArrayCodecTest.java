package io.mqute.persistence.redis;

import com.lambdaworks.redis.codec.RedisCodec;

import org.junit.Test;

import java.nio.ByteBuffer;

import static org.junit.Assert.*;


public class ByteArrayCodecTest {

  @Test
  public void testEncodeDecodeKey() throws Exception {
    RedisCodec<String, byte[]> codec = new ByteArrayCodec();
    ByteBuffer byteBuffer = codec.encodeKey("key");
    assertEquals("key", codec.decodeKey(byteBuffer));

    byteBuffer = ByteBuffer.allocateDirect(3);
    byteBuffer.mark();
    byteBuffer.put("key".getBytes());
    byteBuffer.reset();
    assertEquals("key", codec.decodeKey(byteBuffer));
  }

  @Test
  public void testEncodeDecodeValue() throws Exception {
    RedisCodec<String, byte[]> codec = new ByteArrayCodec();
    byte [] value = "value".getBytes();
    ByteBuffer byteBuffer = codec.encodeValue(value);
    assertArrayEquals(value, codec.decodeValue(byteBuffer));

    byteBuffer = ByteBuffer.allocateDirect(5);
    byteBuffer.mark();
    byteBuffer.put(value);
    byteBuffer.reset();
    assertArrayEquals(value, codec.decodeValue(byteBuffer));
  }

}