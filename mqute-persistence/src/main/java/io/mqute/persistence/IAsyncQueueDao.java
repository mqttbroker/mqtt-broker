package io.mqute.persistence;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * This interface represents a special key value store. However, the value acts like a queue.
 */
public interface IAsyncQueueDao<T> {

  /**
   * Put a sequence of data to the queue associated with given key.
   *
   * @param key  the key of the queue
   * @param data sequence of data to be enqueued
   * @return length of the queue after the operation
   */
  CompletableFuture<Long> offer(String key, T... data);

  CompletableFuture<Void> offer(Map<String, T> data);

  /**
   * Remove the queue with the given key.
   *
   * @param key the key of the queue
   * @return {@literal true} if the data is removed {@literal false} if the key doesn't exist
   */
  CompletableFuture<Boolean> remove(String key);

  /**
   * Get a sequence of data from beginning of the queue.
   *
   * @param key    the key of the queue
   * @param length length of data to be loaded
   * @return a list of data loaded from the queue
   */
  CompletableFuture<List<T>> take(String key, int length);
}
