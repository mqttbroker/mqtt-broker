package io.mqute.persistence.redis;

import com.google.common.collect.*;
import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.api.async.RedisAsyncCommands;

import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import io.mqute.persistence.Codec;
import io.mqute.persistence.PersistenceService;


public class RedisService implements PersistenceService {

  private static final Void VOID = null;
  private final RedisClient client;
  private final RedisAsyncCommands<String, byte[]> redisAsync;

  public RedisService(String redisUri) {
    client = RedisClient.create(redisUri);
    redisAsync = client.connect(new ByteArrayCodec()).async();
  }

  @Override
  public <T> CompletableFuture<Boolean> update(String key, Codec<T> codec, T data) {
    Map.Entry<String, String> pair = parseKey(key);
    return redisAsync.hset(pair.getKey(), pair.getValue(), codec.encode(data))
        .toCompletableFuture();
  }

  @Override
  public CompletableFuture<Boolean> remove(String key) {
    if (key.contains(":")) {
      Map.Entry<String, String> pair = parseKey(key);
      return redisAsync.hdel(pair.getKey(), pair.getValue())
          .thenApply(l -> l == 1L)
          .toCompletableFuture();
    } else {
      return redisAsync.ltrim(key, 1L, 0L).thenApply(String::isEmpty).toCompletableFuture();
    }
  }

  @Override
  public <T> CompletableFuture<T> get(String key, Codec<T> codec) {
    Map.Entry<String, String> pair = parseKey(key);
    return redisAsync.hexists(pair.getKey(), pair.getValue()).thenCompose(has -> {
      if (has) {
        return redisAsync.hget(pair.getKey(), pair.getValue()).thenApply(codec::decode);
      }
      throw new NoSuchElementException("[subscriptions] snapshot not found for:" + key);
    }).toCompletableFuture();
  }

  private Map.Entry<String, String> parseKey(String key) {
    int pos = key.indexOf(':');
    if (pos < 0) {
      return new AbstractMap.SimpleEntry<>(key, key);
    } else {
      return new AbstractMap.SimpleEntry<>(key.substring(0, pos), key.substring(pos + 1));
    }
  }

  @Override
  public <T> CompletableFuture<Long> offer(String key, Codec<T> codec, T... data) {
    byte[][] bytes = Stream.of(data).map(codec::encode).toArray(byte[][]::new);
    return redisAsync.rpush(key, bytes).toCompletableFuture();
  }

  @Override
  public <T> CompletableFuture<Void> offer(Map<String, T> data, Codec<T> codec){

    final Map<String, Map<String, byte[]>> groups = Maps.newHashMapWithExpectedSize(data.size());
    data.forEach((key, val) -> {
      final Map.Entry<String, String> parsed = parseKey(key);
      if(!groups.containsKey(parsed.getKey())){
        groups.put(parsed.getKey(), Maps.newHashMapWithExpectedSize(data.size()));
      }
      groups.get(parsed.getKey()).put(parsed.getValue(), codec.encode(val));
    });

    final List<CompletableFuture<String>> futures = groups.entrySet().stream().map(entry -> redisAsync.hmset(entry.getKey(), entry.getValue())).map(CompletionStage::toCompletableFuture).collect(Collectors.toList());
    return CompletableFuture.allOf(futures.toArray(new CompletableFuture[futures.size()]));
  }

  @Override
  public <T> CompletableFuture<List<T>> take(String key, Codec<T> codec, int length) {
    return redisAsync.lrange(key, 0, length - 1).thenCompose(bytesList ->
        redisAsync.ltrim(key, 0, bytesList.size() - 1).thenApply(trim -> bytesList)
    ).thenApply(bytesList ->
        bytesList.stream().map(codec::decode).collect(Collectors.toList())
    ).toCompletableFuture();
  }

}
