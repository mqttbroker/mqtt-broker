package io.mqute.persistence.redis;

import com.lambdaworks.redis.codec.RedisCodec;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;


public class ByteArrayCodec implements RedisCodec<String, byte []> {

  @Override
  public String decodeKey(ByteBuffer bytes) {
    byte [] key;
    if (bytes.hasArray()) {
      key = bytes.array();
    } else {
      key = new byte[bytes.remaining()];
      bytes.get(key);
    }
    return new String(key, Charset.forName("UTF-8"));
  }

  @Override
  public byte[] decodeValue(ByteBuffer bytes) {
    byte [] value;
    if (bytes.hasArray()) {
      value = bytes.array();
    } else {
      value = new byte[bytes.remaining()];
      bytes.get(value);
    }
    return value;
  }

  @Override
  public ByteBuffer encodeKey(String key) {
    return ByteBuffer.wrap(key.getBytes(Charset.forName("UTF-8")));
  }

  @Override
  public ByteBuffer encodeValue(byte[] value) {
    return ByteBuffer.wrap(value);
  }
}
