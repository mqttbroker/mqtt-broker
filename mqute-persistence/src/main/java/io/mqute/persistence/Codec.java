package io.mqute.persistence;


public interface Codec<T> {

  byte[] encode(T data);

  T decode(byte[] bytes);
}
