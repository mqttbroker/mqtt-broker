package io.mqute.persistence;


import java.util.concurrent.CompletableFuture;

/**
 * An {@code IAsyncDao} represents a Data Access Object that provides asynchronous CRUD operations.
 */
public interface IAsyncDao<T> {

  /**
   * Create or update the data.
   *
   * @param key  unique key of the data
   * @param data data
   * @return {@literal true} if the data is added {@literal false} if the data is updated
   */
  CompletableFuture<Boolean> update(String key, T data);

  /**
   * Remove the data for a given key.
   *
   * @param key unique key of the data to be removed
   * @return {@literal true} if the data is removed {@literal false} if the key doesn't exist
   */
  CompletableFuture<Boolean> remove(String key);

  /**
   * Get the data for a given key.
   *
   * @param key unique key of the data to be fetched
   * @return data associated with the key
   */
  CompletableFuture<T> get(String key);

}
