package io.mqute.persistence;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;


public interface PersistenceService {

  /**
   * Persist the data for a given key.
   *
   * @param key   String key of the data
   * @param codec encode data to byte array
   * @param data  data to be persisted
   * @param <T>   type of the data
   * @return {@literal true} if the data is added {@literal false} if the data is updated
   */
  <T> CompletableFuture<Boolean> update(String key, Codec<T> codec, T data);

  /**
   * Remove the data for a given key.
   *
   * @param key String key of the data
   * @return {@literal true} if the data is removed {@literal false} if the key doesn't exist
   */
  CompletableFuture<Boolean> remove(String key);

  /**
   * Get the data for a given key.
   *
   * @param key   String key of the data
   * @param codec decode byte array to data
   * @param <T>   type of the data
   * @return data associated with the key
   */
  <T> CompletableFuture<T> get(String key, Codec<T> codec);

  /**
   * Put a sequence of data to the queue associated with given key.
   *
   * @param key   String key of the queue
   * @param codec encode data to byte array
   * @param data  a sequence of data to be put in the queue
   * @param <T>   type of the data
   * @return length of the queue after the operation
   */
  <T> CompletableFuture<Long> offer(String key, Codec<T> codec, T... data);

  /**
   * bulk create
   *
   * @param data
   * @param codec
   * @param <T>
   * @return
   */
  <T> CompletableFuture<Void> offer(Map<String, T> data, Codec<T> codec);

  /**
   * Get a sequence of data from beginning of the queue.
   *
   * @param key    String key of the queue
   * @param codec  decode byte array to data
   * @param length length of data to be loaded
   * @param <T>    type of the data
   * @return a list of data loaded from the queue
   */
  <T> CompletableFuture<List<T>> take(String key, Codec<T> codec, int length);
}
